package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.EVENTS_NUM
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.TASKS_NUM
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.data.daos.AgendaDao

import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test


class TestEvents {

    /** Login user , and maintain cookie */
    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            LoginUtils.login()
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            LoginUtils.logout()
        }
    }

    @Test
    fun testEvents() {

        /** Create car accident */
        var carAccident = TestUtils.addBeanItem( AgendaUtils.createCarAccident() , ::AgendaDao)

        /** Create the tasks under the car accident  */
        val hTasks = carAccident
                ?.mId
                ?.let { id -> AgendaUtils.createChildren(TASKS_NUM, id, AgendaUtils::createRepairingCarTask ) }
                ?.map { bean -> TestUtils.addBeanItem( bean , ::AgendaDao )!! }
                ?.toList()
                ?: ArrayList()

        Assert.assertEquals("Error adding $TASKS_NUM tasks in car accident", hTasks?.size , TASKS_NUM)
        /** Get tasks from database  */
        val hTasksFromDb =  carAccident
                ?.mId
                ?.let { id -> AgendaUtils.getChildren( id ) }


        Assert.assertTrue("There are no $TASKS_NUM in car accident", hTasksFromDb?.size == TASKS_NUM)
        Assert.assertTrue("There was an error adding tasks", AgendaUtils.areEquals( hTasks, hTasksFromDb))

        /** Add events in tasks */
       val hEventsMap = hTasks
                .map { agendaBean -> agendaBean.mId!!  }
                .map { id -> id to AgendaUtils.createChildren(EVENTS_NUM, id, AgendaUtils::createEvent ) }
                .toMap()


        val hAddEventsList = hEventsMap
                .values
                .flatten()
                .map { bean -> TestUtils.addBeanItem( bean, ::AgendaDao )!! }
                .toList()

        val hEventsListFromDb = hTasks
                .map{ task -> task.mId!! }
                .map { taskId -> AgendaUtils.getChildren(taskId) }
                .flatten()
                .toList()

        Assert.assertTrue("There was an error adding tasks", AgendaUtils.areEquals(hAddEventsList, hEventsListFromDb))


        /** Remove the car accident  */
        carAccident = TestUtils.removeBeanItem( carAccident!! , ::AgendaDao )
        Assert.assertNotNull("Error removing accident" , carAccident )

        /** Verify the tasks has been removed  */
        hTasks
                .map { agenda -> agenda.mId!! }
                .map { id -> TestUtils.assertNotBeanItemInDataBase( id , ::AgendaDao  ) }

        /** Verify that the car accident has been removed  */
        TestUtils.assertNotBeanItemInDataBase( carAccident!!.mId!! , ::AgendaDao )
    }

}
