package client.manager.accident.car.uoc.es.activity.view.multifab



import android.view.View
import android.view.View.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import client.manager.accident.car.uoc.es.activity.R.id.contract_fab_button
import client.manager.accident.car.uoc.es.activity.R.id.expand_fab_button
import client.manager.accident.car.uoc.es.activity.view.multifab.interfaces.IMultiFabButtonModel
import client.manager.accident.car.uoc.es.application.utils.DeviceHelper
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MultiFabButtonView(var mParentView: View, var mModel : IMultiFabButtonModel) {

    private var mExpandedFabButton : FloatingActionButton? = null

    private var mContractFabButton : FloatingActionButton? = null

    fun createView() {

        mModel.getFabButtonsId()
                .map { setDimensions(mParentView.findViewById<View>( it ) )
                        ?.setOnClickListener{ mModel.onFabButtonSelected( it.id ) }}

        mExpandedFabButton = mParentView.findViewById(expand_fab_button)

        mContractFabButton = mParentView.findViewById(contract_fab_button)

        mExpandedFabButton?.setOnClickListener{ setVisible( VISIBLE ) }

        mContractFabButton?.setOnClickListener{ setVisible( INVISIBLE ) }

        setVisible( INVISIBLE )
    }

    private fun setVisible( visibility : Int ) {
        mModel.getFabButtonsId()
                .map { mParentView.findViewById<View>(it )?.visibility = visibility }

        ( mExpandedFabButton as? View?)?.visibility = if (visibility == VISIBLE ) INVISIBLE else VISIBLE

        ( mContractFabButton as? View?)?.visibility  = if (visibility == VISIBLE ) VISIBLE else INVISIBLE
    }

    private fun setDimensions( fabButton : View ) : View {

       if( DeviceHelper.isMobileLandScape( fabButton.context ) == false ){
           return fabButton
       }

       val bottomMargin = (fabButton.layoutParams as? CoordinatorLayout.LayoutParams)?.bottomMargin
       val rightMargin = (fabButton.layoutParams as? CoordinatorLayout.LayoutParams)?.rightMargin

        (fabButton.layoutParams as? CoordinatorLayout.LayoutParams)?.bottomMargin = rightMargin
        (fabButton.layoutParams as? CoordinatorLayout.LayoutParams)?.rightMargin = bottomMargin

       return fabButton
    }
}