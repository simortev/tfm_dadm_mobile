package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.AGENDA_EVENT_COUNTRY_LABEL
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.MODE_MOCK
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils.verifyHasAuthorization
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils.verifyNoAuthorization
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role.*

import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.util.*


class TestAuthCarAccident {

    /** The agenda bean added  */
    private val m_carAccident: AgendaBean? = null


    /** Login user , and maintain cookie */
    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            if(TestConstants.MODE_MOCK) {
                return
            }
            LoginUtils.loginUsers()
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            if(TestConstants.MODE_MOCK) {
                return
            }
            LoginUtils.logoutUsers()
        }
    }

    @Test
    fun testCarAccident() {

        if( MODE_MOCK ) {
            return
        }

        addCarAccidentBy(CUSTOMER)
        addCarAccidentBy(INSURANCE_STAFF)
        addCarAccidentFailure(CAR_MECHANIC)

        updateCarAccidentWhenCreatedBy(CUSTOMER)
        updateCarAccidentWhenCreatedBy(INSURANCE_STAFF)
    }

    /**
     * Add correctly an accident car by a role
     * @param role
     */
    fun addCarAccidentBy(role: Role) {

        /** Role setContentModel car accident  */
        LoginUtils.activeUser(role)

        var carAccident1 = TestUtils.addBeanItem(  AgendaUtils.createCarAccident() , ::AgendaDao)
        TestUtils.assertBeanItemMatchesDataBaseItem( carAccident1 , " add accident on auth " , ::AgendaDao )

        /**
         * Verify customer and insurance staff has grants, the role car mechanic doesn't have permissions
         */
        verifyHasAuthorization(carAccident1, CUSTOMER)
        verifyHasAuthorization(carAccident1, INSURANCE_STAFF)
        verifyNoAuthorization(carAccident1, CAR_MECHANIC)
        verifyNoAuthorization(carAccident1, CAR_DRIVER)

        /** Role remove car accident  */
        LoginUtils.activeUser(INSURANCE_STAFF)
        TestUtils.removeBeanItem(carAccident1!!, ::AgendaDao )
        TestUtils.assertNotBeanItemInDataBase( carAccident1!!.mId!! , ::AgendaDao )
    }

    /**
     * Add correctly an accident car by a role
     * @param role
     */
    fun updateCarAccidentWhenCreatedBy(role: Role) {

        /** Role setContentModel car accident  */
        LoginUtils.activeUser(role)

        val accident = TestUtils.addBeanItem( AgendaUtils.createCarAccident() , ::AgendaDao)
        TestUtils.assertBeanItemMatchesDataBaseItem( accident!! , role.name + " add accident " , ::AgendaDao)

        /**
         * Verify customer and insurance staff has grants, the role car mechanic doesn't have permissions
         */
        verifyHasAuthorization(accident, CUSTOMER)
        verifyHasAuthorization(accident, INSURANCE_STAFF)
        verifyNoAuthorization(accident, CAR_MECHANIC)
        verifyNoAuthorization(accident, CAR_DRIVER)

        /** update car accident by customer  */
        LoginUtils.activeUser(CUSTOMER)
        accident?.mCountry = Properties[AGENDA_EVENT_COUNTRY_LABEL]
        TestUtils.updateBeanItem(accident!!, ::AgendaDao )
        TestUtils.assertBeanItemMatchesDataBaseItem( accident , role.name + " update accident " , ::AgendaDao)

        verifyHasAuthorization(accident, CUSTOMER)
        verifyHasAuthorization(accident, INSURANCE_STAFF)
        verifyNoAuthorization(accident, CAR_MECHANIC)
        verifyNoAuthorization(accident, CAR_DRIVER)

        /** update car accident by insurance staff  */
        LoginUtils.activeUser(INSURANCE_STAFF)
        accident?.mEndDate = Date()
        TestUtils.updateBeanItem(accident!!, ::AgendaDao )
        TestUtils.assertBeanItemMatchesDataBaseItem( accident , role.name + " update accident " , ::AgendaDao)

        verifyHasAuthorization(accident, CUSTOMER)
        verifyHasAuthorization(accident, INSURANCE_STAFF)
        verifyNoAuthorization(accident, CAR_MECHANIC)
        verifyNoAuthorization(accident, CAR_DRIVER)

        /** failure update car accident by professional staff  */
        LoginUtils.activeUser(CAR_MECHANIC)
        accident?.mLongDescription = CAR_MECHANIC.name
        var agendaBean = TestUtils.updateBeanItem(accident!!, ::AgendaDao )
        Assert.assertNull(role.name + ", update accident ", agendaBean )


        /** Car accident hasn't been updagted  */
        accident?.mLongDescription = null
        verifyHasAuthorization(accident, CUSTOMER)
        verifyHasAuthorization(accident, INSURANCE_STAFF)
        verifyNoAuthorization(accident, CAR_MECHANIC)
        verifyNoAuthorization(accident, CAR_DRIVER)

        /** Role remove car accident  */
        LoginUtils.activeUser(INSURANCE_STAFF)
        TestUtils.removeBeanItem(accident!!, ::AgendaDao)
        TestUtils.assertNotBeanItemInDataBase( accident!!.mId!! , ::AgendaDao )
    }


    /**
     * Verify user doesn't have authorization to add a car accident
     * @param role
     */
    fun addCarAccidentFailure(role: Role) {
        LoginUtils.activeUser(role)
        val accident = TestUtils.addBeanItem( AgendaUtils.createCarAccident() , ::AgendaDao)
        Assert.assertNull(role.name + ", update accident ", accident )
    }


}
