package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.mock.services.AgendaService

class CarAccidentsAction(  ) : IObjectAction<List<CarAccidentBean>>{

    private var mList : List<CarAccidentBean> ? = null

    override fun execute() {
        mList = AgendaService.getCarAccidents()
    }

    override fun getResponse(): List<CarAccidentBean>? {
        return  mList
    }
}