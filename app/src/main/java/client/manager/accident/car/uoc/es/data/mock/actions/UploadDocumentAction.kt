package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.mock.services.DocumentService
import java.io.InputStream

class UploadDocumentAction(
        private var mDocument : DocumentBean,
        private var mInput : InputStream) : IObjectAction<DocumentBean>{

    private var mResponse : DocumentBean? = null

    override fun execute() {
        mResponse = DocumentService.uploadDocument( mDocument , mInput )
    }


    override fun getResponse(): DocumentBean? {
        return mResponse
    }
}
