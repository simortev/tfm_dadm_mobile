package client.manager.accident.car.uoc.es.activity.java.constants

object TestConstants {



    /**************************** PROPERTIES CONSTANTS  */
    /** The properties file  */
    val PROPERTIES_FILE = "colors.xml"

    /**the url property label  */
    val URL_PROPERTY_LABEL = "url"

    /** The bad user property label  */
    val BAD_USER_PROPERTY_LABEL = "bad_user"

    /** The bad password property label  */
    val BAD_PASSWORD_PROPERTY_LABEL = "bad_password"

    /** Agenda event to get short description */
    val AGENDA_EVENT_SHORT_DESCRIPTION_LABEL = "agenda.car.accident.short_description"

    /** Agenda event label to get mCountry label  */
    val AGENDA_EVENT_COUNTRY_LABEL = "agenda.car.accident.country"

    /** Agenda event label to get mCity label */
    val AGENDA_EVENT_CITY_LABEL = "agenda.car.accident.city"

    val AGENDA_POLICY_ID_LABEL = "agenda.car.accident.policy.id"

    /** The damage description 1 label  */
    val DAMAGES_DESCRIPTION1_LABEL = "agenda.car.accident.damages.description1"

    /** The damage description 2 label  */
    val DAMAGES_DESCRIPTION2_LABEL = "agenda.car.accident.damages.description2"

    /** The customer user login  */
    val CUSTOMER_USER = "customer_user"

    /** The customer user password  */
    val CUSTOMER_PASSWORD = "customer_password"

    /** The insurance staff user login  */
    val INSURANCE_STAFF_USER = "insurance_staff_user"

    /** The insurance staff user password  */
    val INSURANCE_STAFF_PASSWORD = "insurance_staff_password"

    /** The professional staff user login 1 */
    val PROFESSIONAL_STAFF_USER_1 = "professional_staff_user_1"

    /** The professional staff passwrod 1 */
    val PROFESSIONAL_STAFF_PASSWORD_1 = "professional_staff_password_1"

    /** The professional staff user login 2 */
    val PROFESSIONAL_STAFF_USER_2 = "professional_staff_user_2"

    /** The professional staff passwrod 2 */
    val PROFESSIONAL_STAFF_PASSWORD_2 = "professional_staff_password_2"

    /** */
    val TASKS_NUM = 4

    val EVENTS_NUM = 4

    val DOCUMENTS_NUM = 1

    val MODE_MOCK = true
}
/**
 * A class with static member, never can be created
 */
