package client.manager.accident.car.uoc.es.data.daos


import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_SEPARATOR
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation
import client.manager.accident.car.uoc.es.data.remote.rest.actions.*
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.*
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.GetObjectAction
import client.manager.accident.car.uoc.es.data.mock.actions.ObjectAction
import client.manager.accident.car.uoc.es.data.mock.services.MockService
import client.manager.accident.car.uoc.es.data.remote.rest.RestTaskDao

/**
 * Object used to execute basic operation on beans, it is extended by other Daos
 *
 * ADD, UPDATE, REMOVE, GET OBJECT , GET OBJECT BY ID or GET OBJECT LIST
 *
 * when a operation function is invoked a ITaskDao is received
 *
 *  A handler should be established, the response in ASYN mode is notified in a different thread form
 *  the invocation one
 *
 *  ADD, UPDATE, REMOVE, GET OBJECT return a bean or null if there was an error
 *
 *  GET OBJECT LIST, return a list, empty in case of error
 *
 * Dao()
        .updateObject( bean )
        .onResponse( ResponseBean<BeanType>::setResponse , response )
        .execute()
 */

open class ObjectDao<BeanType : IObjectBean>(
        private var mUrlService: String,
        private var mObjectClass: Class<BeanType>,
        private var mListClass : Class<Array<BeanType>>
)
{
    /** Add object */
    fun addObject(  obj : BeanType? ): ITaskDao<BeanType> {
        return when( mMockMode ) {
            true -> MockTaskDao( ObjectAction<BeanType>( obj!! , ADD ) )
            false -> RestTaskDao(RestObjectAction(ADD, mUrlService, null, obj, mObjectClass))
        }
    }

    /** Update object */
    fun updateObject( obj : BeanType? ): ITaskDao<BeanType> {
        return when( mMockMode ) {
            true -> MockTaskDao( ObjectAction<BeanType>( obj!! , UPDATE ) )
            false -> RestTaskDao(RestObjectAction(UPDATE, mUrlService, null, obj, mObjectClass))
        }
    }

    /** Remove object */
    fun removeObject( obj : BeanType? ): ITaskDao<BeanType> {
        return when( mMockMode ) {
            true -> MockTaskDao( ObjectAction<BeanType>( obj!! , REMOVE ) )
            false -> RestTaskDao(RestObjectAction(REMOVE,
                    mUrlService + URL_SEPARATOR + obj?.getId(), null, null, mObjectClass))
        }
    }

    /** Get object by id , only for test propouses */
    fun getObject( Id : Long ): ITaskDao<BeanType> {

        return when(mMockMode) {
            true -> MockTaskDao( GetObjectAction<BeanType>( Id , mObjectClass ) )
            false ->
                RestTaskDao(RestObjectAction(OBJECT, mUrlService + URL_SEPARATOR + Id, null, null, mObjectClass))
        }

    }

    /*** These functions are only used by Rest tasks */

    /** Get object in this url, used in login or logout operations */
    fun getObject(operation: Operation, urlRelative : String, parameters : Map<String,String>? = null ): ITaskDao<BeanType> {
        return RestTaskDao(RestObjectAction(operation, urlRelative, parameters, null, mObjectClass))
    }

    /** Get object list */
    fun getObjectList(  urlRelative : String ): ITaskDao<List<BeanType>> {
        return RestTaskDao(RestListAction(urlRelative, mListClass))
    }
}