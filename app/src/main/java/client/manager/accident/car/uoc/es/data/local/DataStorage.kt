package client.manager.accident.car.uoc.es.data.local

import android.content.Context

import java.io.Serializable
import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import client.manager.accident.car.uoc.es.data.local.DataStorageScope.ACTIVITY
import client.manager.accident.car.uoc.es.data.local.DataStorageScope.APPLICATION
import com.google.gson.Gson


class DataStorage(
        private var mContext : Context) : IDataStorage{

    companion object {
        val APPLICATION_DATA = "APPLICATION_DATA"
        val ACTIVITY_DATA = "ACTIVITY_DATA"
    }

    private var mContextData = HashMap<String,Serializable>()

    override fun addData(variable: DataVariable, obj: Serializable) {
        when( variable.mScope){
            ACTIVITY -> addDataInActivity( variable.mName, obj)
            APPLICATION -> addDataInApplication( variable.mName, obj )
        }
    }

    override  fun removeData( variable : DataVariable ) {
         when( variable.mScope){
            ACTIVITY -> removeDataFromActivity( variable.mName)
            APPLICATION -> removeDataFromApplication( variable.mName )
        }
    }

    override fun getData( variable: DataVariable ) : Serializable ? {
        return when( variable.mScope){
            ACTIVITY -> getDataFromActivity( variable.mName)
            APPLICATION -> getDataFromApplication( variable.mName )
        }
    }

    override fun <T> getAppObject( variable: DataVariable , cl : Class<T>  ) : T?{
        return getData( variable )?.let { Gson().fromJson( it as String , cl ) }
    }

    private fun addDataInActivity( variableName: String , obj: Serializable ) {
        mContextData[variableName] = obj
    }

    private fun addDataInApplication( variableName: String , obj: Any ) {
       if(obj is String) {
            mContext.getSharedPreferences(APPLICATION_DATA, MODE_PRIVATE)
                    .edit().putString( variableName , obj ).apply()
        }
        else {
           addDataInApplication( variableName , Gson().toJson( obj ))
        }
    }

    private fun removeDataFromActivity( name : String  ) : Serializable ? {
        return mContextData.remove( name)
    }

    private fun removeDataFromApplication( name : String  )  {
          mContext.getSharedPreferences(APPLICATION_DATA, MODE_PRIVATE)
                .edit().remove( name ).apply()
    }

    private fun getDataFromActivity( name : String  ) : Serializable?  {
        return mContextData[name]
    }

    private fun getDataFromApplication( name : String  ) : String ? {
      return  mContext.getSharedPreferences(APPLICATION_DATA, MODE_PRIVATE).getString( name , null )
    }


    fun save( mBundle : Bundle ?  ) {
       mBundle?.putSerializable( ACTIVITY_DATA , mContextData )
    }

    fun restore( mBundle : Bundle ? ) {
        mContextData = mBundle?.getSerializable( ACTIVITY_DATA ) as? HashMap<String, Serializable>?
                          ?: HashMap()
    }
}