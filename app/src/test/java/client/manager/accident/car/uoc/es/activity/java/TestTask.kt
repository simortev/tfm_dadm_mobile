package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.TASKS_NUM
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import kotlin.collections.ArrayList


class TestTask {

    /** Login user , and maintain cookie */
    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            LoginUtils.login()
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            LoginUtils.logout()
        }
    }

    @Test
    fun testTask() {
        /** Create car accident */
        var carAccident = TestUtils.addBeanItem( AgendaUtils.createCarAccident() , ::AgendaDao )

        /** Create the tasks under the car accident  */
        val hTasks = carAccident
                ?.mId
                ?.let { id -> AgendaUtils.createChildren(TASKS_NUM, id, AgendaUtils::createRepairingCarTask ) }
                ?.map { id -> TestUtils.addBeanItem( id , ::AgendaDao )!! }
                ?.toList()
                ?: ArrayList()

        Assert.assertTrue("Error adding $TASKS_NUM tasks in car accident", hTasks.size == TASKS_NUM)

        /** Get tasks from database  */
        val hTasksFromDb =  carAccident
                ?.mId
                ?.let { id -> AgendaUtils.getChildren( id ) }
                ?: ArrayList()

        Assert.assertTrue("There are no $TASKS_NUM in car accident", hTasksFromDb.size == TASKS_NUM)
        Assert.assertTrue("There was an error adding tasks", AgendaUtils.areEquals( hTasks, hTasksFromDb))

        /** Remove the car accident  */
        carAccident = TestUtils.removeBeanItem( carAccident!! , ::AgendaDao )
        Assert.assertNotNull("Error removing accident" , carAccident )

        /** Verify the tasks has been removed  */
        hTasks
                .map { agenda -> agenda.mId!! }
                .map { id -> TestUtils.assertNotBeanItemInDataBase( id , ::AgendaDao  ) }

        /** Verify that the car accident has been removed  */
        TestUtils.assertNotBeanItemInDataBase( carAccident!!.mId!! , ::AgendaDao )
    }
}
