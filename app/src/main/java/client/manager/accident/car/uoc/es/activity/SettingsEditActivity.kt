package client.manager.accident.car.uoc.es.activity

import client.manager.accident.car.uoc.es.activity.R.string.menu_settings
import client.manager.accident.car.uoc.es.activity.fragments.agenda.edition.SettingsFragment

/**
 * This activity show the settings for the application
 */
class SettingsEditActivity : EditBaseActivity() {


    // Save settings
    override fun onClickSaveData() {

        // Update agenda info
        ((mFragment as? SettingsFragment?)?.saveSettings())

        finish()
    }

    // Get settings
    override fun getSubTitle() : String? {
        return resources.getString(menu_settings)
    }
}