package client.manager.accident.car.uoc.es.data.mock.services

import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.CAR_ACCIDENT
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mAgendaTable
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.AGENDA_CHILDREN_BY_PARENT_ID_INDEX

import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.AGENDA_CHILDREN_BY_TYPE

object AgendaService {

    /**
     * Get list of accident
     */
    fun getCarAccidents( ) : List<CarAccidentBean> {
        return mAgendaTable.getList(AGENDA_CHILDREN_BY_TYPE, CAR_ACCIDENT.ordinal)
                .map { CarAccidentBean( ).apply {
                    mCarAccident = it
                    mUser = MockService.mCarInfoBean?.mUser
                    mPolicy = MockService.mCarInfoBean?.mPolicy
                    }
                }

    }

    /**
     * Get children of a parent
     */
    fun getChildren( parentId : Long ) : List<AgendaBean> {
        return ArrayList( mAgendaTable.getList( AGENDA_CHILDREN_BY_PARENT_ID_INDEX, parentId ) )
    }
}