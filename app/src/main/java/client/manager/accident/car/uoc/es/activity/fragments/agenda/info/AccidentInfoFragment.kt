package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.view.multiblock.MultiBlock
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager


class AccidentInfoFragment : AgendaBlockInfoFragment<CarAccidentBean>() {

    override fun getMultiBlock() : MultiBlock {
        return AgendaDataBindingManager.getMultiBlockDataBindingForAccident( mAgenda as CarAccidentBean )
    }
}