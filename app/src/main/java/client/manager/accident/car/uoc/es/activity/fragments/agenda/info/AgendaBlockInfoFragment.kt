package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.R.layout.detail_content
import client.manager.accident.car.uoc.es.activity.view.multiblock.MultiBlock
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.MultiblockViewHolder
import client.manager.accident.car.uoc.es.data.beans.IObjectBean

abstract class AgendaBlockInfoFragment<T : IObjectBean> : InfoFragment<T>(detail_content) {

    private var mMultiBlock : MultiblockViewHolder? = null

    abstract fun getMultiBlock() : MultiBlock

    override fun createContent(  ){
        mMultiBlock = MultiblockViewHolder( getMultiBlock() ).apply { createView( mRootView!! , layoutInflater ) }
    }
}