package client.manager.accident.car.uoc.es.activity.view.multipane

import android.os.Bundle
import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean


class PaneInfo(
        private var mAgenda : IAgendaBean?,
        var mMenuTextId : Int,
        var mIconId : Int,
        private var mFragmentFactory : () -> Fragment,
        var mPaneId : Int ) {

     var mFragment : Fragment? = null

     fun createFragment() : Fragment {
         mFragment = mFragmentFactory.invoke()

         mAgenda?.let { mFragment?.arguments = Bundle().apply { putSerializable(AGENDA_OBJECT, it ) } }

         return mFragment!!
     }
}
