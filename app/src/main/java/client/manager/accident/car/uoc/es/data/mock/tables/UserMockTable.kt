package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.UserBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.USER_BY_NAME_INDEX

class UserMockTable : MockTable<UserBean>(){

    init {
        addSimpleIndex(USER_BY_NAME_INDEX, UserBean::mLogin )
    }
}
