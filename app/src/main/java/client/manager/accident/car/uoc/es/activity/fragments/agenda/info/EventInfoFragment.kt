package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.view.multiblock.Block
import client.manager.accident.car.uoc.es.activity.view.multiblock.MultiBlock
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager


class EventInfoFragment : AgendaBlockInfoFragment<AgendaBean>() {

    override fun getMultiBlock() : MultiBlock {
        val multiBlock = MultiBlock()

        multiBlock.addBlock( AgendaDataBindingManager.getBlockDataBindingForEvent( mAgenda?.getAgenda()!!,true,
                Block::createBlockText) )

        return multiBlock
    }
}