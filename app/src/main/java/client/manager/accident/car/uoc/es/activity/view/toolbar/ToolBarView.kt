package client.manager.accident.car.uoc.es.activity.view.toolbar


import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import client.manager.accident.car.uoc.es.activity.R



class  ToolBarView(private var mToolBarInfo: ToolBarInfo, private var mActivity: AppCompatActivity){

    private var mToolBar : Toolbar? = null

    init {
        mToolBar = mActivity.findViewById( R.id.activity_toolbar )

        mActivity.setSupportActionBar(mToolBar)

        mActivity.supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }
    }

    fun onCreateOptionsMenu(menu : Menu){
        mToolBar?.setNavigationIcon(mToolBarInfo.mBackIconId)
        mActivity.menuInflater.inflate(mToolBarInfo.mMenuId, menu)
    }
}