package client.manager.accident.car.uoc.es.activity.view.list.interfaces

import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.activity.view.list.GenericViewHolder

interface IListHolderSelectedListener<T: IObjectBean>{
    fun onHolderSelected( holder : GenericViewHolder<T>)
}