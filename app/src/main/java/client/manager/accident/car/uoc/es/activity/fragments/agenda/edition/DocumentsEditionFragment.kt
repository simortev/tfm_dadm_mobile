package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R.layout.edition_list
import client.manager.accident.car.uoc.es.model.agenda.edition.DocumentUploadListModel


class DocumentsEditionFragment( ) : EditionFragment(edition_list) {
    override fun createContent() {
        DocumentUploadListModel(mRootView!!, activity as AgendaBaseActivity, mAgenda!!).createList( )
    }
}