package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.*
import client.manager.accident.car.uoc.es.data.mock.services.MockService

class ObjectAction<ResponseBeanType: IObjectBean> (
        private var mObj : ResponseBeanType ,
        private var operation: Operation) :IObjectAction<ResponseBeanType> {

    var mResponse : ResponseBeanType? = null

    override fun execute() {
       mResponse = when( operation ){
            ADD -> MockService.addObject( mObj )
            UPDATE -> MockService.addObject(mObj )
            REMOVE -> MockService.removeObject( mObj )
            else -> null
        }
    }

    override fun getResponse(): ResponseBeanType? {
        return mResponse
    }
}