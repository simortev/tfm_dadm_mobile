package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R.layout.detail_list
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.model.agenda.info.list.DamagesListInfoModel

class DamagesInfoFragment : InfoFragment<CarAccidentBean>(detail_list) {

        override fun createContent() {
            DamagesListInfoModel( mRootView!!, activity as AgendaBaseActivity ).createList( )
        }
}