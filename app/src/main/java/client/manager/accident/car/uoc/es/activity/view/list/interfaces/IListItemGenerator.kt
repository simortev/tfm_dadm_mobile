package client.manager.accident.car.uoc.es.activity.view.list.interfaces

import android.view.View

interface IListItemGenerator<T>{

    fun getCardLeftIconId(obj : T) : Int ?

    fun getCardRightIconId(obj : T) : Int ?

    fun getCardLeftIconGravity(obj : T) : Int ?

    fun getCardRightIconGravity(obj : T) : Int ?

    fun getCardContent( obj : T ) : View

    fun getCardBackgroundColor( selected: Boolean ) : Int

    fun isCardSelected( obj : T ) : Boolean
}