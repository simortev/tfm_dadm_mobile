package client.manager.accident.car.uoc.es.application

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import client.manager.accident.car.uoc.es.application.utils.LocaleHelper


class CarAccidentApplication : Application()
