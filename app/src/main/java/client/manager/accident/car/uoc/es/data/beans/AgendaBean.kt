package client.manager.accident.car.uoc.es.data.beans


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.Date

/**
 * This represents an agenda event, car accident, tasks or events
 */
class AgendaBean : IAgendaBean {

    /** ID of the event in agenda table  */
    @SerializedName("id")
    var mId: Long? = null

    /** Short description of the event in agenda  */
    @SerializedName("short_description")
    var mShortDescription: String? = null

    /** long description of the event in agenda  */
    @SerializedName("long_description")
    var mLongDescription: String? = null

    /** mStreet of the event in agenda  */
    @SerializedName("street")
    var mStreet: String? = null

    /** City of the event in agenda  */
    @SerializedName("city")
    var mCity: String? = null

    /** Country of the event in agenda   */
    @SerializedName("country")
    var mCountry: String? = null

    /** Latitude of the event in agenda   */
    @SerializedName("latitude")
    var mLatitude: Double? = null

    /** Longitude of the event in agenda   */
    @SerializedName("longitude")
    var mLongitude: Double? = null

    /** Type of the event in agenda Car acciden : 0 , task: 1 and event 2 */
    @SerializedName("agenda_type")
    var mType: Int? = null

    /** Subtype of the event in agenda , only for tasks and events  */
    @SerializedName("agenda_subtype")
    var mSubtype: Int? = null

    /** Status of the event in agenda   */
    @SerializedName("status")
    var mStatus: Int? = null

    /** Id of the parent of the event in agenda   */
    @SerializedName("parent_id")
    var mParentId: Long? = null

    /** Start date of the parent of the event in agenda   */
    @SerializedName("start_date")
    var mStartDate: Date? = null

    /** Update date of the parent of the event in agenda , internal parameter , no serialized  */
    @Expose(serialize = false)
    var mUpdateDate: Date? = null

    /** end date of the parent of the event in agenda   */
    @SerializedName("end_date")
    var mEndDate: Date? = null

    /** The mId policy of the customer who setContentModel the car accident incident  */
    @SerializedName("policy_id")
    var mPolicyId: Long? = null

    /**
     * Enum used to fill the iType element
     */
    enum class AgendaItemType {
        CAR_ACCIDENT,
        TASK,
        EVENT;
    }

    /**
     * This subtype is for tasks and events
     * The id is included because in the future new subtypes can be added and use ordinal
     * can be a problem
     */
    enum class AgendaItemSubType ( var mId : Int ) {
        CAR_DRIVING_TASK(0),
        TOW_TRUCK_DRIVING_TASK(1),
        DAMAGE_ASSESSMENT_TASK(2),
        CAR_REPAIRING_TASK(3),
        REHABILITATION_TASK(4),
        MEDICAL_TASK(5),
        LEGAL_TASK(6),
        CAR_DRIVING_EVENT(100),
        TOW_TRUCK_EVENT(101),
        DAMAGE_ASSESSMENT_EVENT(102),
        CAR_REPAIRING_EVENT(103),
        REHABILITATION_EVENT(104),
        MEDICAL_EVENT(105),
        LEGAL_EVENT(106);
    }



    override fun getId(): Long? {
        return mId
    }

    override fun setId( objId : Long ){
        mId = objId
    }

    override fun getAgenda(): AgendaBean? {
        return this
    }


    /**
     * Compare if this agenda bean is equal to the agenda bean parameter
     * @param agendaBean the agenda bean to be compared
     * @return true or false depending on the item is equal or not
     */
    fun areEquals(agendaBean: AgendaBean?): Boolean {
        return agendaBean.toString().equals( toString() , true  )
    }

    override fun toString(): String {
        val builder = StringBuilder()

        mId?.let{ id -> builder.append("ID: ").append(id).append("\n") }
        mParentId?.let { parentId -> builder.append("PARENT_ID: ").append(parentId).append("\n") }
        mShortDescription?.let { description -> builder.append("SHORT DESCRIPTION: ").append(description).append("\n") }
        mLongDescription?.let{ description -> builder.append("LONG DESCRIPTION: ").append(description).append("\n") }
        mType?.let{ type -> builder.append("TYPE: ").append(AgendaItemType.values()[type].name).append("\n") }
        mSubtype?.let { subtype -> builder.append("SUBTYPE: ").append(subtype).append("\n") }
        mStatus?.let { status -> builder.append("STATUS: ").append(status).append("\n") }
        mLongitude?.let { longitude -> builder.append("LONGITUDE: ").append(longitude).append("\n") }
        mLatitude?.let { latitude -> builder.append("LATITUDE: ").append(latitude).append("\n") }
        mCity?.let{ city -> builder.append("CITY: ").append(city).append("\n") }
        mStreet?.let{ street -> builder.append("STREET: ").append(street).append("\n") }
        mCountry?.let{ country -> builder.append("COUNTRY: ").append(country).append("\n") }
        mStartDate?.let { startDate -> builder.append("START DATE: ").append(startDate).append("\n") }
        mUpdateDate?.let{ updateDate -> builder.append("UPDATE DATE: ").append(updateDate).append("\n") }
        mEndDate?.let{ endDate -> builder.append("START DATE: ").append(endDate).append("\n") }
        mPolicyId?.let { policyId -> builder.append("POLICY ID: ").append(policyId).append("\n") }

        return builder.toString()
    }
}
