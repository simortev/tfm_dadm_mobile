package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R.layout.detail_list
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.model.agenda.info.list.DocumentDownloadListModel


class DocumentsInfoFragment( ) : InfoFragment<AgendaBean>(detail_list) {

    override fun createContent() {
        DocumentDownloadListModel( mRootView!!, activity as AgendaBaseActivity).createList( )
    }
}