package client.manager.accident.car.uoc.es.activity.view


import androidx.test.espresso.Espresso
import org.junit.Test
import androidx.test.filters.SdkSuppress



import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import client.manager.accident.car.uoc.es.activity.AuthenticationActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.drawable.ic_menu
import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.activity.R.string.menu_logout
import client.manager.accident.car.uoc.es.activity.view.list.GenericViewHolder
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import kotlinx.android.synthetic.main.fragment_multipane.*
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
class TestSession {

    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            GlobalConstants.mModeTestView = true
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {

        }
    }

    @get:Rule
    val mActivityScenarioRule = ActivityTestRule<AuthenticationActivity>(AuthenticationActivity::class.java)

    @Test
    fun testLogin() {

        /** Login */
        onView(withId(login_text)).perform(replaceText("mock"))
        onView(withId(password_text)).perform(replaceText("mock"))
        onView(withId(login_button))
                .perform(closeSoftKeyboard())
                .perform(click())

        onView(withId(activity_list)).check(matches(isDisplayed()))
    }
}

