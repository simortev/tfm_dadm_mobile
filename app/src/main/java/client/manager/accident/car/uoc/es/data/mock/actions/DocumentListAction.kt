package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.DocumentBean

class DocumentListAction(parentId : Long  ) : GetListAction<DocumentBean>( parentId , DocumentBean::class.java )