package client.manager.accident.car.uoc.es.activity.view.list


import android.view.View
import android.view.View.GONE



import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams
import androidx.recyclerview.widget.RecyclerView
import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListHolderSelectedListener


class GenericViewHolder<T: IObjectBean> (
        private var mCardView: View?,
        private var mListModel: ListModel<T>,
        private var mCardSelectedListener: IListHolderSelectedListener<T>
        ) : RecyclerView.ViewHolder(mCardView!!) {

    private var mLeftIconView : ImageView? = null

    private var mRighIconView : ImageView? = null

    private var mContent : LinearLayout ? = null


    init {
        mLeftIconView = mCardView?.findViewById(card_left_icon)

        mRighIconView = mCardView?.findViewById(card_right_icon)

        mContent = mCardView?.findViewById(card_content)
    }

    fun bindData(obj : T ) {

        mListModel.getCardLeftIconId( obj )
                ?.let { mLeftIconView?.setImageResource( it ) }


        if( mListModel.getCardLeftIconId( obj ) != null ) {
            mLeftIconView?.setImageResource( mListModel.getCardLeftIconId( obj)!! )
            (mLeftIconView?.layoutParams as LayoutParams).gravity = mListModel.getCardLeftIconGravity( obj )!!
            mLeftIconView?.setOnClickListener { _ -> mListModel.onLeftIconClick( obj ) }
        }else{
            mLeftIconView?.visibility = GONE
        }

        if( mListModel.getCardRightIconId( obj ) != null ) {
            mRighIconView?.setImageResource( mListModel.getCardRightIconId( obj )!! )
            (mRighIconView?.layoutParams as LayoutParams).gravity = mListModel.getCardRightIconGravity( obj )!!
            mRighIconView?.setOnClickListener { _ -> mListModel.onRightIconClick( obj ) }
        }else{
            mRighIconView?.visibility = GONE
        }

        mContent?.addView( mListModel.getCardContent( obj ) )

        mCardView?.setOnClickListener { onSelected( obj ) }

        if(  mListModel.isCardSelected( obj ) ){ onSelected( obj ) }
    }

    private fun  onSelected( obj : T) {
        mListModel.onContentClick( obj )
        mCardSelectedListener.onHolderSelected( this )

        mCardView?.requestFocus()
    }

    fun setBackgroundColor(selected : Boolean ) {
        mCardView?.setBackgroundResource( mListModel.getCardBackgroundColor( selected ) )
    }
}