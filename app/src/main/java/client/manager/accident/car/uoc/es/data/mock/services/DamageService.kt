package client.manager.accident.car.uoc.es.data.mock.services

import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mDamagesTable
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mPolicyMockTable
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.DAMAGES_BY_AGENDA_ID_INDEX
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.POLICY_BY_NUMBER_INDEX

object DamageService {

    /**
     * Get damages of an accident
     */
    fun getAccidentDamages( accidentId : Long  ) : List<CarDamageBean> {
        val hDamages = ArrayList(mDamagesTable.getList( DAMAGES_BY_AGENDA_ID_INDEX,accidentId))

        return hDamages.map { createDamage( it ) }
    }

    /**
     * Create damages, get user and policy
     */
    private fun createDamage( damage : DamageBean ) : CarDamageBean {
        val cardDamage = CarDamageBean()

        cardDamage.mDamages = damage
        cardDamage.mPolicy = PolicyService.getPolicyById( damage.mPolicyId!! )
        cardDamage.mUser = UserService.getUserById( cardDamage.mPolicy?.mUserId!! )

        return cardDamage
    }

    /**
     * Add damages of a policy number
     */
    fun addDamageByPolicyNumber( damage : DamageBean? , policyNumber : String ? ): DamageBean? {

        val policy = mPolicyMockTable.getObject(POLICY_BY_NUMBER_INDEX,policyNumber!!)
        if(policy == null ){
            return null
        }

        damage?.mPolicyId = policy.mId

        mDamagesTable.addObject( damage!! )

        return damage
    }
}