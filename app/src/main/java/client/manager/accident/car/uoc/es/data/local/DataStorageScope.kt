package client.manager.accident.car.uoc.es.data.local

enum class DataStorageScope {
    APPLICATION,
    ACTIVITY
}
