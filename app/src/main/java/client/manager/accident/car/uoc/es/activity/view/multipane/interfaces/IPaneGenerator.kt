package client.manager.accident.car.uoc.es.activity.view.multipane.interfaces


interface IPaneGenerator{
    fun getPaneBackgroundColor( selected: Boolean ) : Int

    fun isPanelSelected( panel : Int ) : Boolean
}