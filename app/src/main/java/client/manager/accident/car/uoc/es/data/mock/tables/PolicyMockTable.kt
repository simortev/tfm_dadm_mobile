package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.PolicyBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.POLICY_BY_NUMBER_INDEX
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.POLICY_BY_USER_ID_INDEX

class PolicyMockTable: MockTable<PolicyBean>(){

    init {
        addSimpleIndex(POLICY_BY_NUMBER_INDEX, PolicyBean::mPolicyNumber )
        addSimpleIndex(POLICY_BY_USER_ID_INDEX,PolicyBean::mUserId)
    }
}