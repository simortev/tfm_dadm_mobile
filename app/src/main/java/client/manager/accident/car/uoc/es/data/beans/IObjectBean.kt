package client.manager.accident.car.uoc.es.data.beans

import java.io.Serializable

/**
 * All the beans extend this class
 */
interface IObjectBean : Serializable {
    /**
     * Return the object id
     */
    fun getId() : Long?

    /**
     * Set object id
     */
    fun setId( objId : Long )
}
