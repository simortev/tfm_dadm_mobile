package client.manager.accident.car.uoc.es.activity.view


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import androidx.test.rule.ActivityTestRule
import client.manager.accident.car.uoc.es.activity.AuthenticationActivity
import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.activity.view.actions.PaneClickAction
import client.manager.accident.car.uoc.es.application.GlobalConstants
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
class TestAgendaEdition {

    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            GlobalConstants.mModeTestView = true
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {

        }
    }

    @get:Rule
    val mActivityScenarioRule = ActivityTestRule<AuthenticationActivity>(AuthenticationActivity::class.java)

    @Test
    fun testAgendaEdtion() {

        /** Login */
        onView(withId(login_text)).perform(replaceText("mock"))
        onView(withId(password_text)).perform(replaceText("mock"))
        onView(withId(login_button))
                .perform(closeSoftKeyboard())
                .perform(click())

        onView(withId(list_fab_button)).perform(click())

        onView( withId(fragment_multipane_menu) )
                .perform(closeSoftKeyboard())
                .perform( PaneClickAction( "LOCATION" ))

    }
}

