package client.manager.accident.car.uoc.es.data.local

import java.io.Serializable

interface IDataStorage {
    fun addData( variable: DataVariable , obj : Serializable )

    fun removeData( variable : DataVariable )

    fun getData( variable: DataVariable ) : Serializable?

    fun <T> getAppObject( variable: DataVariable, cl : Class<T>  ) : T?
}