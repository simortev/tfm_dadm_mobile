package client.manager.accident.car.uoc.es.application.utils.biometry

import android.os.AsyncTask
import androidx.biometric.BiometricPrompt.AuthenticationCallback
import client.manager.accident.car.uoc.es.application.utils.biometry.Biometry.BiometryCallBack
import java.util.concurrent.Executor

class BiometryExecutor(private var mListener: BiometryListener,
                       private var mCallBack : BiometryCallBack)
    : Executor, AuthenticationCallback() {

    /**
     * The main function of executor, create an async task
     */
    override fun execute(command: Runnable) {
        BiometryAsyncTask( command , mListener , mCallBack).execute()
    }

    /**
     * Async task , request and notification in different threads
     * Used by mobile application to avoid mb_block the graphical thread
     */
    private class BiometryAsyncTask(
            private var mRunnable: Runnable,
            private var mListener: BiometryListener,
            private var mCallBack : BiometryCallBack)
        : AsyncTask<Void, Void, Boolean>() {


        /** Execute biometry authentication */
        override fun doInBackground(vararg params: Void): Boolean {
            mRunnable.run()
            return true
        }

        /** Return result in view thread*/
        override fun onPostExecute(taskDao: Boolean ) {
            mListener.onBiometryAuthentication( mCallBack.mAuthenticate )
        }
    }


}