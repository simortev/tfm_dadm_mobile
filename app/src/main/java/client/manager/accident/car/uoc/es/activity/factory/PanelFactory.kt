package client.manager.accident.car.uoc.es.activity.factory


import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.activity.view.multipane.PaneInfo
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean

object PanelFactory{

     fun createPane(fragmentFactory: () -> Fragment, obj: IAgendaBean?, panel : PanelConstants )
            : PaneInfo {

         return PaneInfo( obj , panel.mTitleId , panel.mIconId , fragmentFactory , panel.ordinal )
    }
}