package client.manager.accident.car.uoc.es.application.utils


import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import client.manager.accident.car.uoc.es.data.local.DataStorage
import client.manager.accident.car.uoc.es.data.local.DataVariable.LANGUAGE

import java.util.Locale
import android.os.LocaleList
import androidx.annotation.RequiresApi


/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 *
 *
 * You can also change the locale of your application on the fly by using the setLocale method.
 *
 *
 * Created by gunhansancar on 07/10/15.
 */
object LocaleHelper {

    fun onAttach(context: Context): Context {
           return setLocale(context, getLanguage(context))
    }

    fun getLanguage(context: Context): String {
        return DataStorage(context).getData( LANGUAGE  ) as? String? ?: Locale.getDefault().language
    }


    @Suppress("DEPRECATION")
    fun setLocale(context: Context, language: String): Context {

        DataStorage(context).addData( LANGUAGE , language )

        var locale = Locale(language)
        Locale.setDefault(locale)

        context.resources.configuration.setLocale( Locale( language ))
        context.resources.updateConfiguration(context.resources.configuration, context.resources.displayMetrics)

        context.applicationContext?.resources?.configuration?.setLocale( Locale(language ))
        context.applicationContext?.resources?.updateConfiguration(
                context.applicationContext.resources.configuration, context.applicationContext.resources.displayMetrics)

        return  context
    }
}