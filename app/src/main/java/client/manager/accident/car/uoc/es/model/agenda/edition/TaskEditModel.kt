package client.manager.accident.car.uoc.es.model.agenda.edition

import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.TaskEditionFragment
import client.manager.accident.car.uoc.es.data.beans.AgendaBean


class TaskEditModel(agenda: AgendaBean) : AgendaEditModel<AgendaBean>( agenda, ::TaskEditionFragment)