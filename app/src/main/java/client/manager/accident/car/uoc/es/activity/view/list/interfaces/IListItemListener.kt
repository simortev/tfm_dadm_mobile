package client.manager.accident.car.uoc.es.activity.view.list.interfaces

interface IListItemListener<T> {

     fun onRightIconClick(obj: T )

     fun onLeftIconClick(obj: T )

     fun onContentClick(obj: T )

     fun onAddButtonClick()
}