package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.*
import client.manager.accident.car.uoc.es.data.mock.services.MockService

class GetObjectAction<ResponseBeanType: IObjectBean>(
        private var mId : Long,
        private var cl : Class<ResponseBeanType>): IObjectAction<ResponseBeanType>{

    var mResponse : ResponseBeanType? = null

    override fun execute() {
       mResponse = MockService.getObject<ResponseBeanType>(cl,mId)
    }

    override fun getResponse(): ResponseBeanType? {
        return mResponse
    }
}