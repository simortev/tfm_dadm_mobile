package client.manager.accident.car.uoc.es.data.remote.rest.actions

import java.io.IOException
import java.io.Writer

class LogWriter(private val m_writer: Writer) : Writer() {

    @Throws(IOException::class)
    override fun write(cbuf: CharArray, off: Int, len: Int) {

        print(String(cbuf).substring(off, len - off))

        m_writer.write(cbuf, off, len)
    }

    @Throws(IOException::class)
    override fun flush() {
        m_writer.flush()
    }

    @Throws(IOException::class)
    override fun close() {
        m_writer.close()
    }
}
