package client.manager.accident.car.uoc.es.data.beans.extended

import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.beans.PolicyBean
import client.manager.accident.car.uoc.es.data.beans.UserBean
import com.google.gson.annotations.SerializedName

/**
 * Car info bean contains information about user and policies
 */
open class CarInfoBean: IObjectBean {
    /** the policy  */
    @SerializedName( "policy")
    var mPolicy: PolicyBean? = null

    /** the user  */
    @SerializedName("user")
    var mUser: UserBean? = null

    override fun getId(): Long? {
        return mUser?.mId
    }

    override fun setId( objId : Long ){
        mUser?.mId = objId
    }

    override fun toString(): String {
        val builder = StringBuilder()

        mUser?.let{ user -> builder.append(user).append("\n") }
        mPolicy?.let{ policy -> builder.append(policy).append("\n") }

        return builder.toString()
    }
}
