package client.manager.accident.car.uoc.es.data.mock.services

import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.mock.data.MockData.MOCK_FILE_CONTENT
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mDocumentsTable
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.DOCUMENTS_BY_AGENDA_ID_INDEX
import java.io.InputStream
import java.io.OutputStream

object DocumentService{

    /** Upload a document */
    fun uploadDocument(document : DocumentBean, input : InputStream) : DocumentBean {
       val array = input.readBytes()
       input.close()

        mDocumentsTable.addObject( document!!)

        return document
    }

    /** Download a document */
    fun downloadDocument(document : DocumentBean, output : OutputStream) : DocumentBean {

        output.write(MOCK_FILE_CONTENT.toByteArray( ) )

        return document
    }

    /** Get documents of a specific agenda item */
    fun getDocuments( parentId : Long  ) : List<DocumentBean> {
        return ArrayList( mDocumentsTable.getList(DOCUMENTS_BY_AGENDA_ID_INDEX,parentId) )
    }
}