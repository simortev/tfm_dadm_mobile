package client.manager.accident.car.uoc.es.application.utils.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.os.AsyncTask
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import client.manager.accident.car.uoc.es.application.utils.LocaleHelper
import java.util.*
import kotlin.collections.ArrayList
import android.os.Bundle
import android.location.LocationListener
import android.location.Criteria
import android.os.Looper


class LocationExecutor(
        private var mContext : Context,
        private var mListener: ILocationListener)
    :AsyncTask<Void, Void, Address?>(){

    /** Execute biometry authentication */
    @SuppressLint("MissingPermission")
    override fun doInBackground(vararg params: Void): Address? {

        if( Looper.myLooper() == null )  Looper.prepare()

        val locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as? LocationManager?

        if( locationManager?.isProviderEnabled( GPS_PROVIDER ) == false ){
            return null
        }

        val locListener = object : LocationListener {

            override fun onLocationChanged( location : Location) {}

            override fun onProviderEnabled( provider : String) {}

            override fun onProviderDisabled( provider : String) {}

            override fun onStatusChanged( provider: String, status : Int, extras: Bundle) {}
        }

        val bestProvider = locationManager?.getBestProvider(Criteria(), false)

        locationManager
                ?.requestLocationUpdates(bestProvider, 0, 0F, locListener );

        val location = locationManager?.getLastKnownLocation(bestProvider!!)

        val geocoder = Geocoder(mContext, Locale( LocaleHelper.getLanguage(mContext) ) )

        try {
            val list = geocoder.getFromLocation(location?.latitude!!, location?.longitude!!, 1)
            return list?.takeIf { it.isEmpty() == false }?.get( 0 )
        } catch (e: Exception) {

        }

        return null
    }

    /** Return result in view thread*/
    override fun onPostExecute(address: Address? ) {
        mListener.onLocation(address)
    }

}