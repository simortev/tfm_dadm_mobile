package client.manager.accident.car.uoc.es.data.mock.data

import client.manager.accident.car.uoc.es.data.beans.*
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemSubType.TOW_TRUCK_DRIVING_TASK
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemSubType.TOW_TRUCK_EVENT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.EVENT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.TASK
import client.manager.accident.car.uoc.es.data.mock.services.MockService

object MockData{

    val MOCK_FILE_CONTENT = "fichero de pruebas"

    fun start() {

        startUsers()

        startPolicy()

        startAccident()

        startDamages()

        startDocuments()
    }

    private fun startUsers(){
        UserBean().apply {
            mId = 0
            mLogin = "mock"
            mPassword = "mock"
            mName = "Luis Antonio Gimaraes Smith"
            mPhone = "645678345"
            mMail = "luis@dominio.com"
            mNif = "223455345T"
            mRole = UserBean.Role.INSURANCE_STAFF.ordinal
            mDriverLicenseNumber = "2635452773636"
        }.let { MockService.addObject(it) }

        UserBean().apply {
            mId = 1
            mLogin = "juan"
            mPassword = "juan"
            mName = "Juan Peréz López"
            mPhone = "756453434"
            mMail = "juan@dominio.com"
            mNif = "36252424255T"
            mRole = UserBean.Role.CUSTOMER.ordinal
            mDriverLicenseNumber = "12463763636"
        }.let { MockService.addObject(it) }

        UserBean().apply {
            mId = 2
            mLogin = "ana"
            mPassword = "ana"
            mName = "Ana Casas Blanco"
            mPhone = "76356373828"
            mMail = "ana@dominio.com"
            mNif = "84827379283H"
            mRole = UserBean.Role.CUSTOMER.ordinal
            mDriverLicenseNumber = "243277363636"
        }.let { MockService.addObject(it) }
    }

    private fun startPolicy(){

        PolicyBean().apply {
            mId = 0
            mCarAge = 8
            mCarModel = "Citroen Xara"
            mCarPlateNumber = "5426DER"
            mCompany = "Compañia Seguros 1"
            mUserId = 0
            mPolicyNumber = "1111"
        }.let { MockService.addObject(it) }

        PolicyBean().apply {
            mId = 1
            mUserId = 1
            mCarAge =3
            mCarModel = "Toyota Auris"
            mCarPlateNumber = "7868KWB"
            mCompany = "Compañia seguros 2"
            mPolicyNumber = "2222"
        }.let { MockService.addObject(it) }

        PolicyBean().apply {
            mId = 2
            mUserId = 2
            mCarAge = 15
            mCarModel = "Fiat Punto"
            mCarPlateNumber = "1111ABC"
            mCompany = "Compañia Seguros 1"
            mPolicyNumber = "3333"
        }.let { MockService.addObject(it) }
    }

    private fun startAccident() {

        AgendaBean().apply {
            mId = 0
            mType = AgendaBean.AgendaItemType.CAR_ACCIDENT.ordinal
            mShortDescription = "El accidente se produjo cuando un coche se salto el ceda el paso colisionando lateralmente con otro"
            mLongDescription = mShortDescription
            mParentId = null
            mPolicyId = 0
            mStreet = "Avenida Gómez Laguna"
            mCity = "Zaragoza"
            mCountry = "España"
            mLongitude = -0.92
            mLatitude = 41.637
        }.let { MockService.addObject(it) }

        AgendaBean().apply {
            mId = 1
            mParentId = 0
            mType = TASK.ordinal
            mSubtype = TOW_TRUCK_DRIVING_TASK.mId
            mShortDescription = "Tarea para dejar el coche en un taller"
            mLongDescription = mShortDescription
            mStreet = "Avenida Gómez Laguna"
            mCity = "Zaragoza"
            mCountry = "España"
            mLongitude = -0.92
            mLatitude = 41.637
        }.let { MockService.addObject(it) }

        AgendaBean().apply {
            mId = 2
            mParentId = 1
            mType = EVENT.ordinal
            mSubtype = TOW_TRUCK_EVENT.mId
            mShortDescription = "Evento para llevar el coche a un taller"
            mLongDescription = mShortDescription
            mStreet = "Avenida Gómez Laguna"
            mCity = "Zaragoza"
            mCountry = "España"
            mLongitude = -0.92
            mLatitude = 41.637
        }.let { MockService.addObject(it) }


    }

    private fun startDamages() {
        DamageBean().apply {
            mId = 0
            mPolicyId = 0
            mAgendaId = 0
            mDamages = "Las puertas laterales estaban dañadas, las ruedas torcidas, cristales rotos"
        }.let { MockService.addObject(it) }

        DamageBean().apply {
            mId = 1
            mPolicyId = 1
            mAgendaId = 0
            mDamages = "Choque frontal, parachoques roto, radiador y motor destrozado. Salto airbag"
        }.let { MockService.addObject(it) }

        DamageBean().apply {
            mId = 2
            mPolicyId = 2
            mAgendaId = 0
            mDamages = "Choque trasero, maletero emportrado, salto airbag frontal"
        }.let { MockService.addObject(it) }
    }

    private fun startDocuments() {
        DocumentBean().apply{
            mId = 0
            mAgendaId = 0
            mFileName = "imagen1.jgp"
            mShortDescription = "Imagen del coche accidentado Citroen Xara"
        }.let { MockService.addObject(it) }

        DocumentBean().apply{
            mId = 1
            mAgendaId = 0
            mFileName = "imagen2.jgp"
            mShortDescription = "Imagen del coche accidentado Toyota Auris"
        }.let { MockService.addObject(it) }

        DocumentBean().apply{
            mId = 2
            mAgendaId = 0
            mFileName = "imagen3.jgp"
            mShortDescription = "Imagen del coche accidentado Fiat Punto"
        }.let { MockService.addObject(it) }

    }
}