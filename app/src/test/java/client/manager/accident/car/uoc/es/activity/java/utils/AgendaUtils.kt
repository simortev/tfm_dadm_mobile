package client.manager.accident.car.uoc.es.activity.java.utils


import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.AGENDA_EVENT_CITY_LABEL
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.AGENDA_EVENT_SHORT_DESCRIPTION_LABEL
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.AGENDA_POLICY_ID_LABEL
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils.ResponseBean
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.daos.AccidentDao
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemSubType.CAR_REPAIRING_TASK
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemSubType.MEDICAL_EVENT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.*
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean


object AgendaUtils {

    fun createCarAccident(): AgendaBean {
        /** Create agenda event  */
        val agendaBean = AgendaBean()
        agendaBean.mType = CAR_ACCIDENT.ordinal
        agendaBean.mCity = Properties[AGENDA_EVENT_CITY_LABEL]
        agendaBean.mShortDescription = Properties[AGENDA_EVENT_SHORT_DESCRIPTION_LABEL]
        agendaBean.mPolicyId = Properties[AGENDA_POLICY_ID_LABEL].toLong()

        return agendaBean
    }


    fun createRepairingCarTask(parentId: Long?): AgendaBean {
        val agendaBean = AgendaBean()

        agendaBean.mParentId = parentId
        agendaBean.mType = TASK.ordinal
        agendaBean.mCity = Properties[AGENDA_EVENT_CITY_LABEL]
        agendaBean.mShortDescription = Properties[AGENDA_EVENT_SHORT_DESCRIPTION_LABEL]
        agendaBean.mSubtype = CAR_REPAIRING_TASK.mId

        return agendaBean
    }

    fun createEvent(parentId: Long?): AgendaBean {
        val agendaBean = AgendaBean()

        agendaBean.mType = EVENT.ordinal
        agendaBean.mSubtype = MEDICAL_EVENT.mId

        agendaBean.mCity = Properties[AGENDA_EVENT_CITY_LABEL]
        agendaBean.mShortDescription = Properties[AGENDA_EVENT_SHORT_DESCRIPTION_LABEL]
        agendaBean.mParentId = parentId

        return agendaBean
    }

    fun createChildren(number: Int, lParentId: Long, constructor: (Long) -> AgendaBean ): List<AgendaBean> {
        return (1..number.toLong())
                .map { _ -> constructor.invoke( lParentId) }
                .toList()
    }

    fun getAccidents()  : List<CarAccidentBean> {

        val response = ResponseBean<List<CarAccidentBean>>()

        AccidentDao()
                .getAccidents()
                .onResponse( ResponseBean<List<CarAccidentBean>>::setResponse , response )
                .execute()

        return response.mBean ?: ArrayList()
    }

    fun getChildren(parentId: Long ): List<AgendaBean>  {
        val response = ResponseBean<List<AgendaBean>>()

        AgendaDao()
                .getChildren( parentId )
                .onResponse( ResponseBean<List<AgendaBean>>::setResponse , response )
                .execute()

        return response.mBean ?: ArrayList()
    }


    fun areEquals(hAgendaItems1: List<AgendaBean>?, hAgendaItems2: List<AgendaBean>?): Boolean {

        if ( hAgendaItems1 == null || hAgendaItems2 == null || hAgendaItems1.size != hAgendaItems2.size) {
            return false
        }

        val hAgendas2  = hAgendaItems2
                .map{ agendaBean -> agendaBean.mId to agendaBean }
                .toMap()

        return hAgendaItems1.all { agendaBean -> agendaBean.areEquals( hAgendas2[agendaBean.mId] ) }
    }


    fun verifyHasAuthorization(agendaId: Long, role: Role) {
        /** User setContentModel car accident  */
        LoginUtils.activeUser(role)
        TestUtils.assertBeanItemInDataBase( agendaId , ::AgendaDao )
    }


    fun verifyHasAuthorization(agendaBean: AgendaBean ?, role: Role) {
        verifyHasAuthorization( agendaBean?.mId!! , role )
    }


    fun verifyNoAuthorization(lAgendaId: Long, role: Role) {
        LoginUtils.activeUser(role)
        TestUtils.assertNotBeanItemInDataBase( lAgendaId , ::AgendaDao )
    }


    fun verifyNoAuthorization(agendaBean: AgendaBean?, role: Role) {
       verifyNoAuthorization( agendaBean?.mId!! , role )
    }

}
