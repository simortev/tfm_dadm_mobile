package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.fragments.agenda.edition.AgendaBlockEditionFragment
import client.manager.accident.car.uoc.es.activity.view.multiblock.Block
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager

class EventEditionFragment : AgendaBlockEditionFragment() {

    override fun getBlock() : Block {
        return AgendaDataBindingManager.getBlockDataBindingForEvent( mAgenda!! , false ,
                Block::createEditText )
    }
}