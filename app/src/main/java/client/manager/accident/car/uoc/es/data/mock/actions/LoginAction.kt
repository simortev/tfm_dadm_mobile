package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.mock.services.UserService

class LoginAction(
        private var mName : String ,
        private var mPassword : String )
    :IObjectAction<CarInfoBean>{

    var mResponse : CarInfoBean? = null

    override fun execute(){
        mResponse = UserService.login( mName, mPassword )
    }

    override fun getResponse(): CarInfoBean? {
        return mResponse
    }
}