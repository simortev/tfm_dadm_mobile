package client.manager.accident.car.uoc.es.data.daos


import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_AGENDA
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_AGENDA_CHILDREN
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.AgendaListAction

/**
 * Object used to execute basic operation on agenda beans, it extends ObjectDao
 *
 * ADD, UPDATE, REMOVE, GET OBJECT , GET OBJECT BY ID or GET  AgendaBean objects
 *
 * when a operation function is invoked a ITaskDao is received
 *
 *  A handler should be established, the response in ASYN mode is notified in a different thread form
 *  the invocation one
 *
 *  ADD, UPDATE, REMOVE, GET OBJECT return a bean or null if there was an error
 *
 *  GET OBJECT LIST, return a list, empty in case of error
 *
 *  It contains a specific functin ot get agenda items of a parents to get taks of a an accident
 *  or events of a tasks
 *
     AgendaDao()
        .updateObject( bean )
        .onResponse( ResponseBean<BeanType>::setResponse , response )
        .execute()
 */

class AgendaDao
    : ObjectDao<AgendaBean>( URL_AGENDA, AgendaBean::class.java, Array<AgendaBean>::class.java){

    /** Get tasks of an accident or the events of a task */
    fun getChildren( parentId : Long  ) : ITaskDao<List<AgendaBean>> {
        return when( mMockMode ) {
            true -> MockTaskDao( AgendaListAction( parentId ) )
            false -> getObjectList( String.format(URL_AGENDA_CHILDREN,parentId ) )
        }
    }
}