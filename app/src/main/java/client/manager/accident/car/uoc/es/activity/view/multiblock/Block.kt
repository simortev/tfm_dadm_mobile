package client.manager.accident.car.uoc.es.activity.view.multiblock

import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.IFabButtonHolder

/**
 * It is a block of content with several piece of content
 *
 * It contains a label and an icon and after these a list of block items
 *
 * Each block item is
 *
 *  BlockText -> information
 *  BlockEditText -> Edtion
 *  BlockArray -> Selecton of servereral options ( a spinner )
 */
open class Block{

    // The icon id
    var mIconId : Int = 0

    // The title ide, the value depend on the locale selected
    var mTitleId : Int = 0

    // Fab button holder
    var mFabButtonHolder: IFabButtonHolder? = null

    // Block of items
    var mBlockItems = mutableListOf<BlockItem>()

    var mDisableScroll = false

    // Create a block text for information
    fun createBlockText(labelId : Int, value : String? )  {
        BlockText( labelId, value ?: "").apply { mBlockItems.add(this ) }
    }

    // Create a block for edtion
    fun createEditText(labelId : Int, value : String? )  {
        BlockEditText( labelId, value ?: "" ).apply { mBlockItems.add(this ) }
    }

    // Create a block for selection
    fun createBlockArray(labelId : Int, arrayId : Int , position : Int )  {
        BlockArray( labelId , arrayId , position ).apply { mBlockItems.add(this ) }
    }
}