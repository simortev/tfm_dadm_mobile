package client.manager.accident.car.uoc.es.model.agenda.edition



import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.*
import client.manager.accident.car.uoc.es.activity.factory.PanelConstants.*
import client.manager.accident.car.uoc.es.activity.factory.PanelFactory
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.local.DataStorage

import client.manager.accident.car.uoc.es.data.local.DataVariable.PANEL_EDIT
import client.manager.accident.car.uoc.es.data.local.DataVariable.PANEL_INFO

import client.manager.accident.car.uoc.es.activity.view.multipane.MultiPaneModel
import client.manager.accident.car.uoc.es.activity.view.multipane.MultiPaneViewHolder
import client.manager.accident.car.uoc.es.activity.view.multipane.PaneInfo

abstract class AgendaEditModel<T : IAgendaBean>(
        private var mObj : T,
        private var mFragmentAgendaFactory : () -> Fragment)
    : MultiPaneModel(){

    private var mPaneId : Int = -1

    private var mStorage : DataStorage? = null

    override fun createContent( storage : DataStorage) : Fragment {

        val multiPane = MultiPaneViewHolder()

        multiPane.setContentModel( this )

        mStorage = storage

        mPaneId = mStorage?.getData(PANEL_EDIT) as? Int? ?: getPaneIdFromInfoPaneId()

        multiPane.addPane(PanelFactory.createPane(mFragmentAgendaFactory, mObj , AGENDA_ITEM_EDIT) )

        multiPane.addPane(PanelFactory.createPane(::LocationEditionFragment, mObj, AGENDA_LOCATION_EDIT))

        if( mObj.getId() == null ) {
            return multiPane
        }

        mObj.getAgenda()?.mType
                .takeIf { it == AgendaBean.AgendaItemType.CAR_ACCIDENT.ordinal }
                ?.let {  multiPane.addPane(PanelFactory.createPane(::DamagesEditionFragment, mObj, AGENDA_DAMAGES_EDIT)) }


        multiPane.addPane(PanelFactory.createPane(::DocumentsEditionFragment, mObj, AGENDA_DOCUMENTS_EDIT))

        return multiPane
    }

    private fun getPaneIdFromInfoPaneId() : Int {
        return when( mStorage?.getData(PANEL_INFO) as? Int? ){
            AGENDA_DOCUMENTS_INFO.ordinal -> AGENDA_DOCUMENTS_EDIT.ordinal
            AGENDA_LOCATION_INFO.ordinal -> AGENDA_LOCATION_EDIT.ordinal
            AGENDA_DAMAGES_INFO.ordinal -> AGENDA_DAMAGES_EDIT.ordinal
            else -> AGENDA_ITEM_EDIT.ordinal
        }
    }

    override fun getPaneBackgroundColor(selected: Boolean): Int {
        return when{
            selected -> R.color.cardBackgroundSelected
            else -> R.color.cardBackgroundNoSelected
        }
    }

    override fun onPaneSelection(pane: PaneInfo) {
        mPaneId = pane.mPaneId
        mStorage?.addData( PANEL_EDIT , mPaneId )
    }

    override fun isPanelSelected(panel: Int): Boolean {
        return mPaneId == panel
    }
}