package client.manager.accident.car.uoc.es.model.autentication

import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.AuthenticationCallback
import client.manager.accident.car.uoc.es.activity.LocaleActivity

import client.manager.accident.car.uoc.es.activity.R.array.languages
import client.manager.accident.car.uoc.es.activity.R.drawable.im_insurance_en
import client.manager.accident.car.uoc.es.activity.R.drawable.im_insurance_es
import client.manager.accident.car.uoc.es.activity.R.layout.spinner_item
import client.manager.accident.car.uoc.es.application.GlobalConstants.mLocale
import client.manager.accident.car.uoc.es.application.GlobalConstants.mModeTestView
import client.manager.accident.car.uoc.es.application.utils.biometry.Biometry
import client.manager.accident.car.uoc.es.application.utils.LocaleHelper
import client.manager.accident.car.uoc.es.application.utils.biometry.BiometryListener
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.daos.UserDao
import client.manager.accident.car.uoc.es.data.local.DataVariable.BIOMETRY
import client.manager.accident.car.uoc.es.data.local.DataVariable.POLICY_INFO


/**
 * The model used by authentication activity in order to load data of languages, images or
 * send request to server
 */
class AuthenticationModel( private val mActivity : LocaleActivity ) :
        OnItemSelectedListener, BiometryListener {

    // Images to show depending on the locale
    private val mImage = arrayOf(im_insurance_en, im_insurance_es)

    // Save password in memory, server not return it
    private var mPassword : String? = null

    // The language id ( 0 for english and 1 for spanish )
    var mLanguageId : Int = mLocale.indexOf( LocaleHelper.getLanguage( mActivity ) )

    // Selected item
    var mSelectedItem : Int = -1

    // The listener used when login function is invoed to return the response
    // it is implemented by authentication activity
    private var mListener: IAuthenticationListener? = null

    /**
     * Function invoked by authentication activity in order to login user
     * This function invoke the UserDao in a async way, the response is
     * processed by onLoginResponse function
     */
    fun login( login : String, password: String , listener : IAuthenticationListener ) {
        mListener = listener

        mPassword = password

        UserDao()
                .login( login , password )
                .onResponse( AuthenticationModel::onLoginResponse , this )
                .execute()
    }

    // WHen login is made the response is return using the listener
    private fun onLoginResponse( carInfoBean : CarInfoBean? ){

        carInfoBean?.let {
            it.mUser?.mPassword = mPassword
            mActivity.getStorage().addData(POLICY_INFO, it )
        }

       mListener?.onLoginResponse( carInfoBean )
    }

    /** Function to authenticate */
    fun authenticate(  listener : IAuthenticationListener ) {
        mListener = listener

        /** Get user info if it exists and fill the login text */
        val carInfo = mActivity.getStorage().getAppObject(POLICY_INFO, CarInfoBean::class.java)

        if(      mModeTestView == true ||
                mActivity.getStorage().getData(BIOMETRY) as? Boolean == false ||
                carInfo == null || carInfo.mUser?.mPassword == null ||
                Biometry.authenticate( mActivity  , this  ) == false ){
            authenticateByCredentials()
        }
        else{
            mListener?.onActiveFingerPrint()
        }
    }

    /**
     * Authentication by credentials, if no fingerprint or user doesn't authenticate
     */
    private fun authenticateByCredentials() {
        val carInfo = mActivity.getStorage().getAppObject(POLICY_INFO, CarInfoBean::class.java)
        carInfo?.mUser?.mLogin?.let { mListener?.onUserValue( it ) }

        mActivity.getStorage().addData( BIOMETRY, false )

        mListener?.onDeactiveFingerPrint()
    }

    /**
     * When biometry has been ok
     */
    fun authenticateByBiometry() {


        /** Get user info if it exists and fill the login text */
        val carInfo = mActivity.getStorage().getAppObject(POLICY_INFO, CarInfoBean::class.java)

        login( carInfo?.mUser?.mLogin!!, carInfo.mUser?.mPassword!!, mListener!! )
    }

    /** Get array languages, the array of strings depends on the current locale */
    private fun getLanguages() : Array<String>{
        return mActivity.resources.getStringArray(languages)
    }

    // Get language adapter to create the list box with the languages supported by the application
    fun getLanguageAdapter(   ) : ArrayAdapter<String> {
        // Initializing an ArrayAdapter
        val adapter = ArrayAdapter<String>(mActivity, spinner_item, getLanguages() )
        adapter.setDropDownViewResource(spinner_item)
        return adapter
    }

    // Get the image to show depending on locale
    fun getImageId() : Int {
        return mImage[mLanguageId]
    }

    // Listener when a language is selected, basically the locale in changed and activity is recreated
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if( mLanguageId == position ){
            return
        }

        mLanguageId = position
        mSelectedItem = position

        LocaleHelper.setLocale(mActivity,mLocale[position]);

        (mActivity as AppCompatActivity).recreate()
    }

    // The callback when nothing is selected in the language list box
    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    /**
     * When there was an erro with authentication
     */
    override fun onBiometryAuthentication( bAuthenticate : Boolean ) {

        when( bAuthenticate ){
            true -> authenticateByBiometry()
            false -> authenticateByCredentials()
        }
    }
}