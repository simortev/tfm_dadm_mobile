package client.manager.accident.car.uoc.es.activity.view.multifab.interfaces

interface IMultiFabButtonGenerator {
    fun getFabButtonsId(): List<Int>
}