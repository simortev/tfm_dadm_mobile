package client.manager.accident.car.uoc.es.activity.fragments.agenda.info
import android.os.Bundle


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.local.DataVariable.AGENDA_ITEM

abstract class EditionFragment(private val mLayoutId : Int) : Fragment() {

    protected var mAgenda : AgendaBean ? = null

    protected var mRootView : ViewGroup ? = null

    protected abstract fun createContent(  )

    override fun  onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? {

        mRootView = inflater.inflate( mLayoutId , container, false) as ViewGroup

        mAgenda =
                ( ( activity as? AgendaBaseActivity?)?.getStorage()?.getData(AGENDA_ITEM ) ?:
                  activity?.intent?.getSerializableExtra(AGENDA_OBJECT) ) as? AgendaBean?

        createContent( )

        return mRootView
    }



}

