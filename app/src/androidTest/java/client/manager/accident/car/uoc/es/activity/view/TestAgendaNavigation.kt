package client.manager.accident.car.uoc.es.activity.view


import android.app.Activity
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage.RESUMED
import client.manager.accident.car.uoc.es.activity.AuthenticationActivity
import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.activity.view.actions.PaneClickAction
import client.manager.accident.car.uoc.es.activity.view.list.GenericViewHolder
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
class TestAgendaNavigation {

    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            GlobalConstants.mModeTestView = true
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {

        }
    }

    @get:Rule
    val mActivityScenarioRule = ActivityTestRule<AuthenticationActivity>(AuthenticationActivity::class.java)

    @Test
    fun testAgendaNavigation() {

        /** Login */
        onView(withId(login_text)).perform(replaceText("mock"))
        onView(withId(password_text)).perform(replaceText("mock"))
        onView(withId(login_button))
                .perform(closeSoftKeyboard())
                .perform(click())

        onView(withId(activity_list)).check(matches(isDisplayed()))
        navigateMultiPane( true );

        /** Go to first task */
        onView(withId(activity_list))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<GenericViewHolder<AgendaBean>>(0, ClickItemAction(card_right_icon)))

        onView(withId(activity_list)).check(matches(isDisplayed()))
        navigateMultiPane( false );
      /*  onView(withId(activity_list))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<GenericViewHolder<AgendaBean>>
                        (0, CheckTextAction( "tarea", card_content)));*/

        /** Go to first event */
        onView(withId(activity_list))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<GenericViewHolder<AgendaBean>>(0, ClickItemAction(card_right_icon)))

        onView(withId(activity_list)).check(matches(isDisplayed()))
        navigateMultiPane( false );
       /* onView(withId(activity_list))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition<GenericViewHolder<AgendaBean>>
                        (0, CheckTextAction( "evento", card_content)));*/
    }

    private fun navigateMultiPane( bAccident : Boolean ){

        onView( withId(fragment_multipane_menu) )
                .perform(closeSoftKeyboard())
                .perform( PaneClickAction( "LOCATION" ))

        verifyPaneContentIsMultiBlock();

        if(bAccident){
            onView( withId(fragment_multipane_menu) )
                    .perform( PaneClickAction( "DAMAGES" ))

            verifyPaneContentIsList()
        }

        onView( withId(fragment_multipane_menu) )
                .perform( PaneClickAction( "DOCUMENTS" ))
        verifyPaneContentIsList()

        onView( withId(fragment_multipane_menu) )
                .perform( PaneClickAction( "INFO" ))

        verifyPaneContentIsMultiBlock()
    }

    private class ClickItemAction(private var mId: Int) : ViewAction {

        override fun getConstraints(): org.hamcrest.Matcher<View> {
            return isEnabled()
        }

        override fun getDescription(): String {
            return "Click on a child view with specified id."
        }

        override fun perform(uiController: UiController, view: View) {
            val v = view.findViewById<View>(mId)
            v.performClick()
        }
    }

    private fun verifyPaneContentIsList() {
        val view = getActivityInstance()?.findViewById<RecyclerView>( activity_list)

        checkNotNull( view )
    }

    private fun verifyPaneContentIsMultiBlock() {
        val view =  getActivityInstance()?.findViewById<LinearLayout>( multiblock_content)

       checkNotNull( view )
    }

    fun getActivityInstance(): Activity ? {
        var currentActivity: Activity? = null

        getInstrumentation().runOnMainSync {
            val resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(RESUMED)
            if (resumedActivities.iterator().hasNext()) {
                currentActivity = resumedActivities.iterator().next()
            }
        }

        return currentActivity
    }
}

