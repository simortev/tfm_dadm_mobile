package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import client.manager.accident.car.uoc.es.activity.R.id.multiblock_content
import client.manager.accident.car.uoc.es.activity.R.layout.mb_content
import client.manager.accident.car.uoc.es.activity.view.multiblock.MultiBlock

/**
 * If represents several blocks of information or edition
 *
 * It contains block and each block contains block items
 *
 * There are different block items , for information, edition or selection ( spinner )
 */
class MultiblockViewHolder( multiBlock : MultiBlock ){

    // Lis of block holders
    private var mBlockHolders = mutableListOf<BlockViewHolder>()

    // Create block view holders from block data
    init { multiBlock.mBlocks.map { BlockViewHolder( it ) }.map { mBlockHolders.add( it ) } }

    // Create view, the layout is a linear layout inside an scroll view
    fun createView( parentView: ViewGroup , layoutInflater: LayoutInflater) {
        val multiBlockView : View =
                layoutInflater.inflate(mb_content, parentView, false)

        parentView.addView( multiBlockView )

        mBlockHolders.map { it.createView( multiBlockView.findViewById(multiblock_content) , layoutInflater )}
    }
}