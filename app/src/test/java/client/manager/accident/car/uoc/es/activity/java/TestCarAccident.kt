package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.AGENDA_EVENT_COUNTRY_LABEL
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test


class TestCarAccident {

    /** The agenda bean added  */
    private var mCarAccident: AgendaBean? = null

    /** Login user , and maintain cookie */
    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            LoginUtils.login()
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            LoginUtils.logout()
        }
    }

    /**
     * Test car accident, basically, add a agendabean, update it, get it and finally remove it
     *
     * We make it in only one test function because we need the functions be executed in order
     */
    @Test
    fun testCarAccident() {
        testAddCarAccident()
        testUpdateCarAccident()
        testGetCarAccidents()
        testRemoveCarAccident()
    }

    /**
     * Test add car accident
     */
    private fun testAddCarAccident() {
        mCarAccident = AgendaUtils.createCarAccident()
        mCarAccident = TestUtils.addBeanItem( mCarAccident!! , ::AgendaDao)

        TestUtils.assertBeanItemMatchesDataBaseItem( mCarAccident , " add accident " , ::AgendaDao)
    }

    /**
     * Test update car accident
     */
    private fun testUpdateCarAccident() {
        mCarAccident?.mCountry = Properties[AGENDA_EVENT_COUNTRY_LABEL]
        mCarAccident?.mStartDate = null
        TestUtils.updateBeanItem( mCarAccident!! , ::AgendaDao)

        TestUtils.assertBeanItemMatchesDataBaseItem( mCarAccident , " update accident " , ::AgendaDao)
    }

    /**
     * Test get car accidents
     */
    private fun testGetCarAccidents() {
        val carAccident = AgendaUtils.getAccidents()

        val accident = carAccident
                .filter { carAccidentBean -> carAccidentBean.getId() == mCarAccident!!.getId() }
                .first()

        TestUtils.assertBeanItemMatches( mCarAccident!! , accident.mCarAccident ," ger car accidents " )
    }

    /**
     * Test remove car accident
     */
    private fun testRemoveCarAccident() {
        TestUtils.removeBeanItem( mCarAccident!! , ::AgendaDao)

        TestUtils.assertNotBeanItemInDataBase( mCarAccident!!.mId!! , ::AgendaDao )
    }
}
