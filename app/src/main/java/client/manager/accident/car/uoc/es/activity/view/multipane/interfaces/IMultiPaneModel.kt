package client.manager.accident.car.uoc.es.activity.view.multipane.interfaces

import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.data.local.DataStorage

interface IMultiPaneModel   {
    fun createContent( storage : DataStorage ) : Fragment
}