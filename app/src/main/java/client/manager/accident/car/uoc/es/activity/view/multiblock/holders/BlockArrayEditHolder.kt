package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import client.manager.accident.car.uoc.es.activity.R.id.edit_spinner
import client.manager.accident.car.uoc.es.activity.R.layout.*
import client.manager.accident.car.uoc.es.activity.view.multiblock.BlockArray


/**
 * This is a list box ( spinner ) to select one option, it contains a label and the list box
 */
class BlockArrayEditHolder(private var mBlockIem : BlockArray ):
        BlockItemViewHolder( mb_spinner_edit , mBlockIem.mLabelId ), OnItemSelectedListener {

    // The spinner
    private var mSpinner : Spinner? = null

    var mListener : ISpinnerListener? = null

    // Create the view
    override fun createView(parentView: ViewGroup, layoutInflater: LayoutInflater) : ViewGroup ? {

        val blockView = super.createView( parentView , layoutInflater)

        mSpinner = blockView?.findViewById(edit_spinner)

        val array = blockView?.context?.resources?.getStringArray( mBlockIem.mArrayId )

        // Initializing an ArrayAdapter
        mSpinner?.adapter =  ArrayAdapter<String>(mSpinner?.context!!, spinner_item, array!! )
                .apply { setDropDownViewResource(spinner_item) }

        mSpinner?.setSelection( mBlockIem.mPosition )

        mSpinner?.onItemSelectedListener = this

        return blockView
    }

    // Get the position of item selected
    fun getPosition() : Int? {
        return mSpinner?.selectedItemPosition
    }

    // Listener when a language is selected, basically the locale in changed and activity is recreated
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        mListener?.onSelectedIem( position )
    }

    // The callback when nothing is selected in the language list box
    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}