package client.manager.accident.car.uoc.es.application.utils.location

import android.location.Address

interface ILocationListener{
    fun onLocation( address : Address? )
}