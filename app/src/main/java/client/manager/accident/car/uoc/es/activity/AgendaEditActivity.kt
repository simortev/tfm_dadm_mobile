package client.manager.accident.car.uoc.es.activity

import client.manager.accident.car.uoc.es.activity.R.string.*
import client.manager.accident.car.uoc.es.activity.fragments.agenda.edition.AgendaBlockEditionFragment
import client.manager.accident.car.uoc.es.activity.view.multipane.MultiPaneViewHolder
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.CAR_ACCIDENT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.TASK
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.local.DataVariable.AGENDA_ITEM

/**
 * This activity show the views to create or update accidents, tasks or events
 *
 * As in other activities the model is created and content is loaded as a fragment
 *
 * The model depends on the associated bean
 *
 * Accident -> AccidentEditModel
 * Task -> TaskEditModel
 * Event -> EventEditMdodel
 */
class AgendaEditActivity : EditBaseActivity() {



    // Save agenda in repository
     override fun onClickSaveData() {

        // Update agenda info
        ((mFragment as? MultiPaneViewHolder?)
                ?.getSelectedFragment() as? AgendaBlockEditionFragment?)
                ?.updateAgendaBlock()

        val agenda = mStorage?.getData(AGENDA_ITEM) as? AgendaBean?

        // If agenda bean id is null is a new bean ( then create it ) in other case update
        when( agenda?.getId() == null ){
            true ->    AgendaDao().addObject( agenda )
            else -> AgendaDao().updateObject( agenda )
        }.onResponse( AgendaEditActivity::onAddAgenda  , this )
                .execute()
    }

    private fun onAddAgenda( agenda: AgendaBean? ) {
        finish()
    }

    // Get subtitle
    override fun getSubTitleId( type: Int ) : Int? {
        return when( type ){
            CAR_ACCIDENT.ordinal -> accident_edition_title
            TASK.ordinal  -> task_edition_title
            else -> event_edition_title
        }
    }
}