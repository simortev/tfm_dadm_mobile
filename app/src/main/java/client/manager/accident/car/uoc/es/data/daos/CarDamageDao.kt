package client.manager.accident.car.uoc.es.data.daos

import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_AGENDA_DAMAGES
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_DAMAGES
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.CarAccidentsAction
import client.manager.accident.car.uoc.es.data.mock.actions.DamagesListAction
import client.manager.accident.car.uoc.es.data.mock.services.DamageService

/**
 * Object used to get a list of damages in an accident , all the vehicles with users and policies
 *
*/

class CarDamageDao : ObjectDao<CarDamageBean>(  URL_DAMAGES, CarDamageBean::class.java, Array<CarDamageBean>::class.java){

    fun getAccidentDamages( accidentId : Long  ) : ITaskDao<List<CarDamageBean>> {

        return when(mMockMode){
            true -> MockTaskDao( DamagesListAction( accidentId ) )
            false -> getObjectList( String.format(URL_AGENDA_DAMAGES, accidentId ) )

        }
    }
}