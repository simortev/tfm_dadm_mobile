package client.manager.accident.car.uoc.es.data.mock.services

import client.manager.accident.car.uoc.es.data.beans.PolicyBean
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mPolicyMockTable

object PolicyService {

    fun getPolicyById( policyId : Long ) : PolicyBean? {
        return mPolicyMockTable.getObjectById( policyId)
    }
}