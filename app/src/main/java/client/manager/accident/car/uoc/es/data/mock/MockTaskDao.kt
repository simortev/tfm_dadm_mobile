package client.manager.accident.car.uoc.es.data.mock

import android.os.AsyncTask
import client.manager.accident.car.uoc.es.application.GlobalConstants


import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.ASYNC
import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.SYNC
import client.manager.accident.car.uoc.es.data.mock.actions.IObjectAction
import client.manager.accident.car.uoc.es.data.remote.rest.RestTaskDao

import client.manager.accident.car.uoc.es.data.tasks.ITaskDao


/**
  * This is a class representing a task to do a rest connection witch response is a bean (user,agenda,damages,document)
 *
 *  The base class is ITaskDao, really is the interface see by the code, the code never see this RestTaskDao
 *
 *  This class implements the interface
 *
 *  onResponse -> set an object and the handler with the response. The handler is a function to manage the
 *  response, it has 2 parameters, the object set and the response.  This is useful because this object
 *  can be the 'this' parameter and the handler a class function
 *
 *  cancel -> cancel notification.
 *
 *  execute -> execute connection
 *
 *  Cancel, execute and onResponse are invoked in the same thread, but notifyResponse in a different one
      this functions are synchronized
 *
 *  In an ASYNC mode, the connection is executed in a different thread from the thread invoking the request
 *
 *  cancel only avoid notification
 *
 *  Internally there are 2 different task classes
 *
 *      Sync task , request and notification in the same thread used for testing purposes
 *      Async task , where connection and notification are executed in different thread, used in mobile application
 */

class MockTaskDao<ResponseBeanType>(private var mAction : IObjectAction<ResponseBeanType>)
    : ITaskDao<ResponseBeanType> {

    /** See the explanation of this object in the definition of the interface */
    private var mNotifier : INotifier<ResponseBeanType>? = null

    /** Create notification, object and handler ( function ) */
    @Synchronized
    override fun <ResponseAttachedObject> onResponse(
            responseHandle: (ResponseAttachedObject, ResponseBeanType?) -> Unit ,
            obj: ResponseAttachedObject ): ITaskDao<ResponseBeanType> {

        mNotifier = Notifier(obj, responseHandle)

        return this
    }

    /** Execute the tasks */
    override fun execute(): ITaskDao<ResponseBeanType> {
        mAction.execute()

        notifyResponse()

        return this
    }

    /** Avoid notification, similar behavior as live data of Google */
    @Synchronized
    override fun cancel() {
        mNotifier = null
    }

    /** Notify response, finally the handler will be notified */
    @Synchronized
    private fun notifyResponse(  ){
        mNotifier?.notifyResponse( mAction.getResponse() )
    }


    /** Notifier is an internal interface used to contain the response handler and attached object
     *
     * It is used because if MockTaskDao holds the handler and object directly, the generic type of the object
     * should be set when the MockTaskDao is created and no when onResponse is invoked
     * */
    private interface INotifier<ResponseBeanType> {
        fun notifyResponse( response : ResponseBeanType? )
    }

    /** Implementation of INotifier
     *
     * A class with only 1 generic type is transformed in a class with 2 generic types
     *
     * See the previous function to know the explanation
     **/
    private class Notifier<ResponseAttachObject,ResponseBeanType>(
            var mObj: ResponseAttachObject,
            var mResponseHandle: ( (ResponseAttachObject,ResponseBeanType?) -> Unit ) )
        : INotifier<ResponseBeanType> {

        override fun notifyResponse( response : ResponseBeanType?) {
            mResponseHandle.invoke( mObj , response )
        }
    }
}
