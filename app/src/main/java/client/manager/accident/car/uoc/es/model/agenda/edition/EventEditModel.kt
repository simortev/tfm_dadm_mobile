package client.manager.accident.car.uoc.es.model.agenda.edition

import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.EventEditionFragment
import client.manager.accident.car.uoc.es.data.beans.AgendaBean


class EventEditModel( agenda : AgendaBean) : AgendaEditModel<AgendaBean>(agenda, ::EventEditionFragment)