package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.LocaleActivity
import client.manager.accident.car.uoc.es.activity.fragments.agenda.edition.AgendaBlockEditionFragment
import client.manager.accident.car.uoc.es.activity.view.multiblock.Block
import client.manager.accident.car.uoc.es.application.utils.LocaleHelper
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager
import client.manager.accident.car.uoc.es.model.agenda.edition.LocationEditModel

class LocationEditionFragment( ) : AgendaBlockEditionFragment() {

    override fun getBlock() : Block {
        return AgendaDataBindingManager.getBlockDataBindingForLocation(
                LocaleHelper.getLanguage(
                        activity as LocaleActivity),mAgenda!! ,
                Block::createEditText ,
                LocationEditModel( this ) )
    }

    override fun updateAgendaBlock() {
        AgendaDataBindingManager.updateAgendaFromLocationBlock(
                LocaleHelper.getLanguage(activity as LocaleActivity),mBlockViewHolder!!,mAgenda!!)
                .let { updateAgenda( it ) }
    }
}