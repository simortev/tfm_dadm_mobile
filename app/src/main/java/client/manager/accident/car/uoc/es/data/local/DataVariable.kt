package client.manager.accident.car.uoc.es.data.local

import client.manager.accident.car.uoc.es.data.local.DataStorageScope.ACTIVITY
import client.manager.accident.car.uoc.es.data.local.DataStorageScope.APPLICATION


enum class DataVariable(  var mName : String ,
                          var mScope: DataStorageScope ){
     SERVER_URL( "SERVER_URL", APPLICATION),
     POLICY_INFO( "POLICY_INFO", APPLICATION),
     //AGENDA_LIST("AGENDA_LIST", ACTIVITY),
     AGENDA_ITEM("AGENDA_ITEM", ACTIVITY),
     PANEL_INFO( "PANEL_INFO" , ACTIVITY ),
     AGENDA_ITEM_TYPE_RETURN("AGENDA_ITEM_TYPE_RETURN", APPLICATION),
     AGENDA_ITEM_RETURN("AGENDA_ITEM_RETURN", APPLICATION),
     PANEL_INFO_RETURN( "PANEL_INFO_RETURN" , APPLICATION ),
     PANEL_EDIT( "PANEL_EDIT" , ACTIVITY ),
     LANGUAGE( "LANGUAGE" , APPLICATION ),
     BIOMETRY("BIOMETRY",ACTIVITY)
}