package client.manager.accident.car.uoc.es.model.autentication

import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean

/**
 * This interface is implmented by authentication activity in order to receive the
 * login user information
 */
interface IAuthenticationListener{
    /** If carInfobean is null then user is not in session, if it contains a value, the user is in session
     * carInfoBean is user and insurance policy data*/
    fun onLoginResponse( carInfoBean : CarInfoBean?  )

    /**
     * If user is known for credentials authentication
     */
    fun onUserValue( login : String )

    /**
     * When finger print is active
     */
    fun onActiveFingerPrint()

    /**
     * When finer print is not active
     */
    fun onDeactiveFingerPrint()
}