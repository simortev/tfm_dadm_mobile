package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.DOCUMENTS_BY_AGENDA_ID_INDEX


class DocumentMockTable: MockTable<DocumentBean>(  ) {

    init {
        addMultipleIndex(DOCUMENTS_BY_AGENDA_ID_INDEX, DocumentBean::mAgendaId )
    }
}