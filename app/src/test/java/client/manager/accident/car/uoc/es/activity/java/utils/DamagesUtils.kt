package client.manager.accident.car.uoc.es.activity.java.utils

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.AGENDA_POLICY_ID_LABEL
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.DAMAGES_DESCRIPTION1_LABEL
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils.ResponseBean
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.daos.CarDamageDao


/**
 * Class used to setContentModel damage bean
 */
object DamagesUtils {


    fun createDamages(carAccidentId: Long?): DamageBean {

        val damageBean = DamageBean()

        damageBean.mPolicyId = Properties[AGENDA_POLICY_ID_LABEL].toLong()
        damageBean.mAgendaId = carAccidentId
        damageBean.mDamages = Properties[DAMAGES_DESCRIPTION1_LABEL]

        return damageBean
    }


    fun getAccidentDamages(accidentId: Long ): List<CarDamageBean>  {
        val response = ResponseBean<List<CarDamageBean>>()

        CarDamageDao()
                .getAccidentDamages( accidentId )
                .onResponse( ResponseBean<List<CarDamageBean>>::setResponse , response )
                .execute()

        return response.mBean ?: ArrayList()
    }
}
