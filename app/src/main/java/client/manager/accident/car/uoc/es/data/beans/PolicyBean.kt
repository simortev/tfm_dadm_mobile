package client.manager.accident.car.uoc.es.data.beans


import com.google.gson.annotations.SerializedName

/**
 * The policy data, a user can have more than one policy, one policy a car
 */
class PolicyBean : IObjectBean {

    /** The policy mId */
    @SerializedName("id")
    var mId: Long? = null

    /** Policy number */
    @SerializedName( "policy_number")
    var mPolicyNumber: String? = null

    /** User Id  */
    @SerializedName("user_id")
    var mUserId: Long? = null

    /** The car model  */
    @SerializedName("car_model")
    var mCarModel: String? = null

    /** The car plate number  */
    @SerializedName("car_plate_number")
    var mCarPlateNumber: String? = null

    /** The car age , number of years */
    @SerializedName("car_age")
    var mCarAge: Int? = null

    /** The policy company */
    @SerializedName("company")
    var mCompany: String? = null

    override fun getId(): Long? {
        return mId
    }

    override fun setId( objId : Long ){
        mId = objId
    }

    override fun toString(): String {
        val builder = StringBuilder()

        mId?.let { id -> builder.append("ID: ").append(id).append("\n") }
        mUserId?.let{ userId -> builder.append("USER: ").append(userId).append("\n") }
        mCompany?.let{ company -> builder.append("COMPANY: ").append(company).append("\n") }
        mPolicyNumber?.let{ policyNumber -> builder.append("POLICY NUMBER: ").append(policyNumber).append("\n") }
        mCarModel?.let{ carModel -> builder.append("CAR MODEL: ").append(carModel).append("\n") }
        mCarPlateNumber?.let{ carPlateNumber -> builder.append("CAR PLATE NUMBER: ").append(carPlateNumber).append("\n") }
        mCarAge?.let{ carAge -> builder.append("CAR AGE: ").append(carAge).append("\n") }

        return builder.toString()
    }
}
