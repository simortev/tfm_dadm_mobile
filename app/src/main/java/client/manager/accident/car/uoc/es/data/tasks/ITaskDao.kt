package client.manager.accident.car.uoc.es.data.tasks

/**
 * This class is an interface that represents the operation to do , at the end this implemented
 * by RestTaskDao
 *
 * Used as a pipe line, exept in cancel function
 *
 * Use example, every call return a ITaskDao
 *
 * onReponse is used to set the handler and pass a object that will be invoked when the response
 * is received
 *
 * The handler contains 2 parameters, the object passed in onReponse and the Rest response
 *
 *    The object attached is the type ResponseAttachObject ( is a generic )
 *    The response is the type ResponseBeanType ( is a generic )
 *
 *    UserDao()
 *        .logout()
 *        .onResponse( LoginVerifier::verifyLogout , LoginVerifier())
 *        .execute()
 *
 *  All of this functions are executed in the same thread but the handler of the response
 *  can be invoked in a different one.  In the implementation class these functions are shynchronized
 */
interface ITaskDao<ResponseBeanType>{

    fun <ResponseAttachObject> onResponse(
            responseHandle : (ResponseAttachObject,ResponseBeanType?) -> Unit ,
            obj: ResponseAttachObject ):  ITaskDao<ResponseBeanType>

    fun execute(): ITaskDao<ResponseBeanType>

    fun cancel()
}