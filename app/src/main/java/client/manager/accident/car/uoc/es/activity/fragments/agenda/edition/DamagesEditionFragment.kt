package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.model.agenda.edition.DamagesListEditModel

class DamagesEditionFragment( ) : EditionFragment(R.layout.edition_list) {
    override fun createContent() {
        DamagesListEditModel(mRootView!!, activity as AgendaBaseActivity, mAgenda!!).createList( )
    }
}