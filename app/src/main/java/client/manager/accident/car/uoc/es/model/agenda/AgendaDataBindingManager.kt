package client.manager.accident.car.uoc.es.model.agenda

import android.content.Context
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R.array.events
import client.manager.accident.car.uoc.es.activity.R.array.tasks
import client.manager.accident.car.uoc.es.activity.R.drawable.*
import client.manager.accident.car.uoc.es.activity.R.string.*
import client.manager.accident.car.uoc.es.activity.view.multiblock.Block
import client.manager.accident.car.uoc.es.activity.view.multiblock.MultiBlock
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.BlockViewHolder
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.IFabButtonHolder
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.DATE_TIME_FORMAT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemSubType.*
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.*
import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.beans.PolicyBean
import client.manager.accident.car.uoc.es.data.beans.UserBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.local.DataVariable.SERVER_URL
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*





/**
 * Singleton used for binding between views and beans and viceversa, beans and views
 * in case of content or edition screens
 */
object AgendaDataBindingManager {

    /**
     * Get multiblock view content from accident
     */
    fun getMultiBlockDataBindingForAccident(accident : CarAccidentBean) : MultiBlock {

        val multiBlock = MultiBlock()

        // Show accident info as an agenda bean
        accident.mCarAccident?.let { multiBlock.addBlock( getAgendaBlock( it, true , Block::createBlockText )) }

        // Show policy info
        accident.mPolicy?.let { multiBlock.addBlock( getPolicyBlock( it )) }

        // Show policy owner info
        accident.mUser?.let { multiBlock.addBlock( getUserBlock( it  ) ) }

        return multiBlock
    }

    /**
     * Get multiblock for accident edition
     */
    fun getBlockDataBindingForEditAccident(accident : AgendaBean ) : Block {

        // Show task as an agenda bean
       return getAgendaBlock( accident , false , Block::createEditText )
    }

    /**
     * Get multiblock for tasks ( info or edition, depending of factory )
     */
    fun getBlockDataBindingForTask(
            task : AgendaBean,
            isInfo : Boolean,
            factory : (Block,Int,String?) -> Unit ) : Block {
        // Show task as an agenda bean
        return getAgendaBlock( task ,isInfo , factory )
    }

    /**
     * Get multiblock for an event ( info or edition, depending of factory )
     */
    fun getBlockDataBindingForEvent(
            event : AgendaBean,
            isInfo : Boolean,
            factory : (Block,Int,String?) -> Unit
    ) : Block {
       return getAgendaBlock( event , isInfo, factory )
    }

    /**
     * Get multiblock for location from an agenda bean ( accident, task or event ) ( info or edition, depending of factory )
     */
    fun getBlockDataBindingForLocation(
            language: String,
            event: AgendaBean,
            factory : (Block,Int,String?) -> Unit,
            fabButtonHolder: IFabButtonHolder ? ) : Block {
      return getLocationBlock( language, event , factory , fabButtonHolder)

    }

    /**
     * Get block for show damages from a car damage bean
     */
    fun getBlockForInfoCarDamage(damage : CarDamageBean) : Block {
        val block = Block()

        block.mTitleId = damages
        block.mIconId = ic_car

        // Add policy block
        addPolicyBlock( damage.mPolicy!! , block )

        // Add policy owner block
        addUserBlock( damage.mUser!! , block  )

        // Add damages description
        Block::createBlockText.invoke( block , agenda_long_description, damage.mDamages?.mDamages )

        return block
    }


    /**
     * Get accident, task or event from the view block
     * factory crete a BlockText or a BlockEditText depeind if the activity is a content or a
     * edition activity
     */
    private fun getAgendaBlock(
            agenda : AgendaBean,
            isInfo : Boolean,
            factory : (Block,Int,String?) -> Unit
            ) : Block {
        val block = Block()

        block.mTitleId = when( agenda.mType ) {
            CAR_ACCIDENT.ordinal -> agenda_accident_title
            TASK.ordinal -> agenda_task_title
            else -> agenda_event_title
        }

        block.mIconId = when( agenda.mType ) {
            CAR_ACCIDENT.ordinal -> ic_car
            TASK.ordinal -> getTaskIconId( agenda )
            else -> getEventIConId( agenda )
        }

        when( isInfo ) {
            true -> createAgendaBlockForInfo( agenda , block )
            false -> createAgendaBlockForEdition( agenda , block )
        }

        factory.invoke( block, agenda_long_description, agenda.mLongDescription )

        return block
    }

    /**
     * Create agenda block info for information , including dates
     */
    private fun createAgendaBlockForInfo(agenda : AgendaBean, block: Block )  {
        agenda.mStartDate?.let { block.createBlockText( agenda_start_date,
                  SimpleDateFormat(DATE_TIME_FORMAT).format( it ) ) }

        agenda.mEndDate?.let { block.createBlockText(  agenda_end_date,
                SimpleDateFormat(DATE_TIME_FORMAT).format( it ) ) }
    }

    /**
     * Create agenda block for edition , including short description and in creation
     * the task or type type spinner
     */
    private fun createAgendaBlockForEdition(agenda : AgendaBean, block: Block)  {

        if( agenda.mId == null ) {
            when {
                agenda.mType == TASK.ordinal ->
                    block.createBlockArray(agenda_task_type, tasks, getTaskItemPosition(agenda.mSubtype?:0))
                agenda.mType == EVENT.ordinal ->
                    block.createBlockArray(agenda_event_type, events, getEventItemPosition(agenda.mSubtype?:0))
            }
        }

        block.createEditText( agenda_short_description, agenda.mShortDescription )
    }


    /**
     * Get user block
     */
    private fun getUserBlock(user : UserBean ) : Block {
        val block = Block()

        block.mTitleId = user_title
        block.mIconId = ic_person

        return addUserBlock( user , block )
    }

    /**
     * Add a user block 
     */
    private fun addUserBlock(user : UserBean , block : Block) : Block {

        block.createBlockText(   user_name, user.mName )
        block.createBlockText(   user_nif, user.mNif )
        block.createBlockText(  user_driver_license, user.mDriverLicenseNumber)
        block.createBlockText(  user_phone, user.mPhone )
        block.createBlockText(   user_mail, user.mMail )

        return block
    }

    /**
     * Get policy block
     */
    private fun getPolicyBlock(policy : PolicyBean ) : Block {
        val block = Block()

        block.mTitleId = policty_title
        block.mIconId = ic_car

        return addPolicyBlock(policy, block )
    }

    /**
     * Add policy block
     */
    private fun addPolicyBlock(policy : PolicyBean, block : Block) : Block {

        block.createBlockText( policy_car_plate_number, policy.mCarPlateNumber )
        block.createBlockText( policy_car_model, policy.mCarModel )
        block.createBlockText( policy_company, policy.mCompany )
        block.createBlockText( policy_number, policy.mPolicyNumber)
        block.createBlockText( policy_car_age, policy.mCarAge.toString() )

        return block
    }

    /**
     * Get location block
     */
    private fun getLocationBlock(
            language: String,
            agenda : AgendaBean,
            factory : (Block,Int,String?) -> Unit,
            fabButtonHolder: IFabButtonHolder ? ): Block {
        val block = Block()

        block.mIconId = ic_location
        block.mTitleId = location
        block.mFabButtonHolder = fabButtonHolder

        if( fabButtonHolder == null ) {
            block.mDisableScroll = true
        }

        factory.invoke( block, agenda_country, agenda.mCountry )
        factory.invoke( block, agenda_city, agenda.mCity )
        factory.invoke( block, agenda_street, agenda.mStreet )

        val format = NumberFormat.getInstance(Locale(language) )

        factory.invoke( block, agenda_longitude, format.format( agenda.mLongitude ?: 0F ) )
        factory.invoke( block, agenda_latitude,  format.format( agenda.mLatitude ?: 0F ) )

        return block
    }

    /**
     * Get task icon from task agenda bean
     */
    fun getTaskIconId(obj: AgendaBean): Int {
        return obj.mSubtype?.let { getTaskIconId( it ) } ?: ic_location
    }

    /**
     * Get task icon id from the agenda subtype
     */
    private fun getTaskIconId( subType : Int ): Int {
        return when ( subType ) {
            CAR_DRIVING_TASK.mId -> ic_car
            CAR_REPAIRING_TASK.mId -> ic_repairing
            TOW_TRUCK_DRIVING_TASK.mId -> ic_location
            LEGAL_TASK.mId -> ic_justice
            MEDICAL_TASK.mId -> ic_health
            DAMAGE_ASSESSMENT_TASK.mId -> ic_info
            REHABILITATION_TASK.mId -> ic_rehabilitation
            else -> ic_location
        }
    }

    /**
     * Get icon of task from position in the spinner of edtion
     */
    fun getTaskIconIdFromPosition( position : Int ): Int {
        return getTaskIconId( getTaskTypeFromPosition( position ) )
    }

    /**
     *
     * Ge event icon id from event agenda bean
     */
    fun getEventIConId(obj: AgendaBean): Int {
        return obj.mSubtype?.let { getEventIConId( it ) } ?: ic_location
    }

    /**
     * Get event icon id from the agenda event subtype
     */
    private fun getEventIConId( subType: Int ) : Int {
        return when ( subType ) {
            MEDICAL_EVENT.mId -> ic_health
            CAR_DRIVING_EVENT.mId -> ic_car
            TOW_TRUCK_EVENT.mId -> ic_location
            DAMAGE_ASSESSMENT_EVENT.mId -> ic_info
            CAR_REPAIRING_EVENT.mId -> ic_repairing
            REHABILITATION_EVENT.mId -> ic_rehabilitation
            LEGAL_EVENT.mId -> ic_justice
            else -> ic_location
        }
    }

    /**
     * Get icon of task from position in the spinner of edtion
     */
    fun getEventIconIdFromPosition( position : Int ): Int {
        return getEventIConId( getEventTypeFromPosition( position ) )
    }

    /**
     * Get task description
     */
    fun getTaskDescription( context: Context , subtype : Int ): String {

        val descriptions = context.resources.getStringArray( tasks )
        val position = getTaskItemPosition( subtype )

        return descriptions[position]
    }

    /**
     * Get task item position in spiffer from the task type
     */
    private fun getTaskItemPosition( subtype : Int ): Int {

        return when ( subtype ) {
            MEDICAL_TASK.mId -> 0
            LEGAL_TASK.mId -> 1
            CAR_DRIVING_TASK.mId -> 2
            CAR_REPAIRING_TASK.mId -> 3
            TOW_TRUCK_DRIVING_TASK.mId -> 4
            DAMAGE_ASSESSMENT_TASK.mId -> 5
            REHABILITATION_TASK.mId -> 6
            else -> 0
        }
    }

    /**
     * Get task type fromo the position selected in spinner
     */
    private fun getTaskTypeFromPosition( position : Int  ) : Int {
        return when ( position ) {
            0 -> MEDICAL_TASK.mId
            1 -> LEGAL_TASK.mId
            2 -> CAR_DRIVING_TASK.mId
            3 -> CAR_REPAIRING_TASK.mId
            4 -> TOW_TRUCK_DRIVING_TASK.mId
            5 -> DAMAGE_ASSESSMENT_TASK.mId
            6 -> REHABILITATION_TASK.mId
            else -> MEDICAL_TASK.mId
        }
    }

    /**
     * Get event description
     */
    fun getEventDescription( context: Context , subtype : Int ): String {
        val descriptions = context.resources.getStringArray(events)
        val position = getEventItemPosition( subtype )

        return descriptions[position]
    }

    /**
     * Get event item position
     */
    private fun getEventItemPosition( subtype : Int ): Int {
        return when ( subtype ) {
            MEDICAL_EVENT.mId -> 0
            LEGAL_EVENT.mId -> 1
            CAR_DRIVING_EVENT.mId -> 2
            CAR_REPAIRING_EVENT.mId -> 3
            TOW_TRUCK_EVENT.mId -> 4
            DAMAGE_ASSESSMENT_EVENT.mId -> 5
            REHABILITATION_EVENT.mId -> 6
            else -> 0
        }
    }

    /**
     * Get event type from the position selected in spinner
     */
    private fun getEventTypeFromPosition( position : Int  ) : Int {
        return when ( position ) {
            0 -> MEDICAL_EVENT.mId
            1 -> LEGAL_EVENT.mId
            2 -> CAR_DRIVING_EVENT.mId
            3 -> CAR_REPAIRING_EVENT.mId
            4 -> TOW_TRUCK_EVENT.mId
            5 -> DAMAGE_ASSESSMENT_EVENT.mId
            6 -> REHABILITATION_EVENT.mId
            else -> MEDICAL_EVENT.mId
        }
    }

    /**
     * Get block for edit damages
     */
    fun getBlockForCarDamageInEditActivity(damage : CarDamageBean ) : Block {
        val block = Block()

        block.mTitleId = damages
        block.mIconId = ic_car

        block.mDisableScroll = true

        block.createBlockText( policy_car_plate_number, damage.mPolicy?.mCarPlateNumber )
        block.createBlockText( agenda_long_description, damage.mDamages?.mDamages )

        return block
    }

    /**
     * Get block for a new car damage
     */
    fun getBlockForNewCarDamage(damage : CarDamageBean ) : Block {
        val block = Block()

        block.mTitleId = damages
        block.mIconId = ic_car

        block.mDisableScroll = true

        block.createEditText( policy_number, damage.mPolicy?.mPolicyNumber )
        block.createEditText( agenda_long_description, damage.mDamages?.mDamages )

        return block
    }


    /**
     * Get block for update a car damage
     */
    fun getBlockForUpdateCarDamage(damage : CarDamageBean ) : Block {
        val block = Block()

        block.mTitleId = damages
        block.mIconId = ic_car

        block.mDisableScroll = true

        block.createBlockText( policy_car_plate_number, damage.mPolicy?.mCarPlateNumber)
        block.createEditText( agenda_long_description, damage.mDamages?.mDamages )

        return block
    }

    /**
     * Get block for update a car damage
     */
    fun getBlockForSettings( activity : AgendaBaseActivity ) : Block {
        val block = Block()

        val url = activity.getStorage().getData(SERVER_URL) as? String?

        block.mTitleId = menu_settings
        block.mIconId = ic_settings

        block.createEditText( settings_url, url )

        return block
    }

    /**
     * Get damage bean from the view block
     */
    fun getNewDamageFromBlock( blockHolder : BlockViewHolder ) : CarDamageBean {

       val carDamageBean = CarDamageBean()
        carDamageBean.mDamages = DamageBean()
        carDamageBean.mPolicy = PolicyBean()

        carDamageBean.mDamages?.mDamages = blockHolder.getValue( agenda_long_description )
        carDamageBean.mPolicy?.mPolicyNumber = blockHolder.getValue( policy_number )

        return carDamageBean
    }

    /**
     * Set agenda info from agenda view holder
     */
    fun updateAgendaFromAgendaBlock(blockHolder : BlockViewHolder, agenda: AgendaBean ) : AgendaBean {

        agenda.mLongDescription = blockHolder.getValue(agenda_long_description)
        agenda.mShortDescription = blockHolder.getValue(agenda_short_description)

        if( agenda.mId != null ) {
            return agenda
        }

        agenda.mSubtype = when (agenda.mType) {
            TASK.ordinal -> getTaskTypeFromPosition(blockHolder.getPosition(agenda_task_type))
            EVENT.ordinal -> getEventTypeFromPosition(blockHolder.getPosition(agenda_event_type))
            else -> 0
            }

        return agenda
    }

    /**
     * Set agenda item location info from location view holder
     */
    fun updateAgendaFromLocationBlock(
            language: String,
            blockHolder : BlockViewHolder ,
            agenda: AgendaBean ) : AgendaBean {

        val formater = NumberFormat.getInstance(Locale( language ))

        agenda.mCountry = blockHolder.getValue(agenda_country)
        agenda.mCity = blockHolder.getValue(agenda_city)
        agenda.mStreet = blockHolder.getValue(agenda_street)
        agenda.mLongitude =  formater.parse( blockHolder.getValue(agenda_longitude) ).toDouble()
        agenda.mLatitude = formater.parse( blockHolder.getValue(agenda_latitude) ).toDouble()

        return agenda
    }
}