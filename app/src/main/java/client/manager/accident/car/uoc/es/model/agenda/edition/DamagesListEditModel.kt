package client.manager.accident.car.uoc.es.model.agenda.edition


import android.view.Gravity
import android.view.View
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R.color.cardBackgroundNoSelected
import client.manager.accident.car.uoc.es.activity.R.drawable.ic_accept
import client.manager.accident.car.uoc.es.activity.R.drawable.ic_cancel
import client.manager.accident.car.uoc.es.activity.view.list.GenericRecyclerViewAdapter
import client.manager.accident.car.uoc.es.activity.view.list.ListModel
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.BlockViewHolder
import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.daos.CarDamageDao
import client.manager.accident.car.uoc.es.data.daos.DamageDao
import client.manager.accident.car.uoc.es.data.local.DataVariable.AGENDA_ITEM
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager.getNewDamageFromBlock


/**
 * Create the list of damages bean in edition
 */
class DamagesListEditModel(
        rootView : View,
        activity: AgendaBaseActivity,
        private var mAgenda : IAgendaBean)
    : ListModel<CarDamageBean>( activity , rootView ) {

    // The list of damages of the accident
    private var mhList : MutableList<CarDamageBean>? = null

    // When a new damage is going to be created these are the new bean and new damage view
    private var mNewDamageBean : CarDamageBean ? = null
    private var mNewBlockViewHolder : BlockViewHolder ? = null

    // When a damage is updated
    private var mUpdateDamageBean : CarDamageBean ? = null
    private var mUpdateBlockViewHolder : BlockViewHolder ? = null

    // Create list
    override fun createList( )  {
        onRefreshList()
    }

    // Update list
    private fun updateList( )  {
        onRefreshList()
    }

    // Get damages list always the data is upated or user request it
    override fun onRefreshList() {
        CarDamageDao()
            .getAccidentDamages( mAgenda.getAgenda()?.mId!!)
            .onResponse( DamagesListEditModel::createListView, this)
            .execute()
    }

    // On response of data create the list
    private fun createListView(hList : List<CarDamageBean> ? ) {

        finishRefresh()

        mhList = hList?.toMutableList() ?: mutableListOf()

        mhList?.let {
            mRecyclerView?.adapter = GenericRecyclerViewAdapter(this, it  )
        }
    }

    /**
     * Only when a new damage is created the righ icon click is managed
     * When a new damage is created this is the accept button and damage will be created
     * or updated
     */
    override fun onRightIconClick(obj: CarDamageBean) {
        when( obj.mUser == null ){
            true -> createDamage()
            false -> updateDamage( obj )
        }
    }

    /**
     * Create damage
     */
    private fun createDamage(){
        // Get new damage bean from view
        val carDamage = mNewBlockViewHolder?.let { getNewDamageFromBlock( it) }
        if( carDamage == null ) {
            cancelAddDamage( )
            createListView( mhList )
            return
        }

        // Get the accident
        val agenda = mActivity.getStorage().getData( AGENDA_ITEM ) as? IAgendaBean?

        carDamage.mDamages?.mAgendaId = agenda?.getAgenda()?.mId

        // Add the damage with the policy
        DamageDao()
            .addDamageByPolicyNumber( carDamage.mDamages , carDamage.mPolicy?.mPolicyNumber )
            .onResponse( DamagesListEditModel::onUpdateList , this )
            .execute()
    }

    /**
     * Update damage
     */
    private fun updateDamage(damage: CarDamageBean){
        // Get new damage bean from view
        val carDamage = mNewBlockViewHolder?.let { getNewDamageFromBlock( it) }
        if( carDamage == null ) {
            cancelUpdateDamage( )
            createListView( mhList )
            return
        }

       damage.mDamages?.mDamages = carDamage.mDamages?.mDamages

        // Add the damage with the policy
        DamageDao()
            .updateObject( damage.mDamages )
            .onResponse( DamagesListEditModel::onUpdateList , this )
            .execute()
    }

    /**
     * Remove damage
     */
    private fun removeDamage() {

        DamageDao()
            .removeObject( mUpdateDamageBean?.mDamages )
            .onResponse( DamagesListEditModel::onUpdateList , this )
            .execute()
    }

    // Left button in case a new damage is for cancel operation
    override fun onLeftIconClick(obj: CarDamageBean) {

        cancelAddDamage()

        cancelUpdateDamage()

        createListView( mhList)
    }

    // On contenct click is update damage
    override fun onContentClick(obj: CarDamageBean) {
        if( obj == mUpdateDamageBean || obj == mNewDamageBean ) {
            return
        }

        executeUpdateDamage( obj )
    }

    // If click on add button then add a new damage
    override fun onAddButtonClick() {
        if( mNewDamageBean != null ){
            return
        }

        when( mUpdateDamageBean == null ) {
            true -> executeAddDamage()
            false -> removeDamage()
        }


    }

    // Only in case a new damage there is a icon the accept icon
    override  fun getCardRightIconId(obj: CarDamageBean): Int ? {
        return when{
            obj == mNewDamageBean || obj == mUpdateDamageBean  -> ic_accept
            else -> null
        }
    }

    // Only in case a new damage there is a icon the cancel icon
    override  fun getCardLeftIconId(obj: CarDamageBean): Int ? {
        return when{
            obj == mNewDamageBean || obj == mUpdateDamageBean  -> ic_cancel
            else -> null
        }
    }

    override fun getCardRightIconGravity(obj: CarDamageBean): Int? {
        return Gravity.CENTER
    }

    override fun getCardLeftIconGravity(obj: CarDamageBean): Int? {
        return Gravity.CENTER
    }

    /**
     * The content depends on the item
     *
     * If and old item then show the damages as info
     * If a new item then show text field for edition and accept and cancel buttons
     */
    override fun getCardContent(obj: CarDamageBean): View {
        return when{
            obj == mNewDamageBean -> getNewCardContent( obj )
            obj == mUpdateDamageBean -> geUpdateCardContent( obj )
            else -> getDefaultCardContent( obj )
        }
    }

    /**
     * Get content from the agenda data binding manager for a new damage
     */
    private fun getNewCardContent(obj: CarDamageBean): View {
        return AgendaDataBindingManager.getBlockForNewCarDamage( obj )
                .let{ BlockViewHolder( it ) }
                .apply { createView(null,mActivity.layoutInflater) }
                .apply { mNewBlockViewHolder = this }
                .let { it.mCardView!! }
    }


    /**
     * Get content from the agenda data binding manager for a new damage
     */
    private fun geUpdateCardContent(obj: CarDamageBean): View {
        return AgendaDataBindingManager.getBlockForUpdateCarDamage( obj )
                .let{ BlockViewHolder( it ) }
                .apply { createView(null,mActivity.layoutInflater) }
                .apply { mNewBlockViewHolder = this }
                .let { it.mCardView!! }
    }

    /**
     * Get the content from the agenda data biding manager for an old damage
     */
    private fun getDefaultCardContent(obj: CarDamageBean): View {
        return AgendaDataBindingManager.getBlockForCarDamageInEditActivity( obj )
                .let { BlockViewHolder( it ) }
                .apply { createView(null,mActivity.layoutInflater) }
                .let { it.mCardView!! }
    }

    override fun getCardBackgroundColor(selected: Boolean): Int {
        return cardBackgroundNoSelected
    }

    override fun isCardSelected( obj : CarDamageBean ) : Boolean {
        return obj == mNewDamageBean
    }


    private fun executeAddDamage() {
        cancelUpdateDamage()

        mNewDamageBean = CarDamageBean()
        mNewBlockViewHolder = null
        mhList?.add( mNewDamageBean!! )
        createListView( mhList )
    }

    private fun executeUpdateDamage( damage : CarDamageBean) {
        cancelAddDamage()

        mUpdateDamageBean = damage
        mUpdateBlockViewHolder = null
        createListView( mhList )

        setFabButton( android.R.drawable.ic_menu_delete)
    }


    private fun cancelAddDamage() {
        mNewDamageBean?.let {  mhList?.remove( it ) }
        mNewDamageBean = null
        mNewBlockViewHolder = null
    }

    private fun cancelUpdateDamage() {
        mUpdateDamageBean = null
        mUpdateBlockViewHolder = null

        setFabButton( android.R.drawable.ic_input_add)
    }

    private  fun onUpdateList( obj : DamageBean? ) {
        cancelAddDamage()
        cancelUpdateDamage()
        updateList()
    }
}