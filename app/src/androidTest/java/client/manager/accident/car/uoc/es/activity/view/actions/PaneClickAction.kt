package client.manager.accident.car.uoc.es.activity.view.actions

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import client.manager.accident.car.uoc.es.activity.R

class PaneClickAction( private var mText : String ) : ViewAction {

    override fun getConstraints(): org.hamcrest.Matcher<View> {
        return ViewMatchers.isEnabled()
    }

    override fun getDescription(): String {
        return "Click on a child view with specified id."
    }

    override fun perform(uiController: UiController, view: View) {

        val view =  (0..(view as ViewGroup).childCount - 1)
                .map { view.getChildAt( it )  }
                .first{ it.findViewById<TextView>(R.id.menu_pane_text)?.text?.equals( mText ) ?: false }
                ?: null

        view?.performClick();

    }
}