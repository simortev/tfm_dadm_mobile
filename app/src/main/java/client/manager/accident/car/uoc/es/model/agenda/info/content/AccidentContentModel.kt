package client.manager.accident.car.uoc.es.model.agenda.info


import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.AccidentInfoFragment
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean

class AccidentContentModel( agenda : CarAccidentBean )
    : AgendaContentModel<CarAccidentBean>( agenda , ::AccidentInfoFragment )