package client.manager.accident.car.uoc.es.data.remote.rest.actions

import client.manager.accident.car.uoc.es.data.daos.OperationDao
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.InputStreamReader
import java.net.HttpURLConnection

/**
 * This class processes the response to a Rest request witch response is a JSON object
 *
 * Used by RestObjectAction and RestFileUploadAction
 */
abstract class RestObjectResponseAction<ResponseBeanType> constructor(
        mOperation: OperationDao.Operation,
        mRelativeUrl: String,
        params : Map<String,String>?,
        var mObjClass: Class<ResponseBeanType>? )
    : RestAction<ResponseBeanType>(mOperation, mRelativeUrl, params ){

    override fun receiveResponse( httpConnection : HttpURLConnection) {
        mResponseBean = Gson().fromJson<ResponseBeanType>( JsonReader(
                InputStreamReader( httpConnection.inputStream , Charsets.UTF_8.name() ) ) ,  mObjClass )
    }
}