package client.manager.accident.car.uoc.es.model.agenda

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


import client.manager.accident.car.uoc.es.activity.AgendaEditActivity
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.menu.toolbar_edit
import client.manager.accident.car.uoc.es.activity.R.menu.toolbar_info
import client.manager.accident.car.uoc.es.activity.R.string.app_name
import client.manager.accident.car.uoc.es.activity.SettingsEditActivity
import client.manager.accident.car.uoc.es.activity.fragments.agenda.edition.SettingsFragment

import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.application.GlobalConstants.NAVIGATION
import client.manager.accident.car.uoc.es.application.GlobalConstants.Navigation
import client.manager.accident.car.uoc.es.application.GlobalConstants.Navigation.AGENDA_EDITION
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.*
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.model.agenda.edition.AccidentEditModel
import client.manager.accident.car.uoc.es.model.agenda.edition.EventEditModel
import client.manager.accident.car.uoc.es.model.agenda.edition.TaskEditModel
import client.manager.accident.car.uoc.es.model.agenda.info.*
import client.manager.accident.car.uoc.es.model.agenda.info.list.AccidentListModel
import client.manager.accident.car.uoc.es.model.agenda.info.list.EventListModel
import client.manager.accident.car.uoc.es.model.agenda.info.list.TaskListModel
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IMultiPaneModel
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListModel
import client.manager.accident.car.uoc.es.activity.view.toolbar.ToolBarInfo

/**
 * This is a singleton class ( a kotlin object) used to get models in activities
 * depending in the bean tupe
 */
object AgendaModelManager {

    /**
     * Get model used to show a list with accidents, tasks or events
     * CarAccidentListModel, TasksListModel or EventListModel
     */
    fun getListModel(activity : AgendaBaseActivity) : IListModel {

        val agenda = activity.intent.getSerializableExtra(AGENDA_OBJECT) as? IAgendaBean?

        return when( agenda?.getAgenda()?.mType ?: EVENT.ordinal ){
            CAR_ACCIDENT.ordinal -> TaskListModel((agenda?.getAgenda())!!,activity)
            TASK.ordinal -> EventListModel((agenda?.getAgenda())!!,activity)
            else -> AccidentListModel(activity)
        }
    }

    /**
     * Get model to show the content of a bean
     * CarAccidentContentModel, TaskContentModel or EventContenModel depending on the bean
     * to show its data
     */
    fun getContentModel(agenda: IAgendaBean ) : IMultiPaneModel {

        return when(  agenda?.getAgenda()?.mType ?: CAR_ACCIDENT.ordinal ){
            TASK.ordinal -> TaskContentModel( (agenda.getAgenda() )!! )
            EVENT.ordinal -> EventContentModel( (agenda.getAgenda())!! )
            else -> AccidentContentModel( (agenda as? CarAccidentBean?)!! )
        }
    }

    /**
     * Get fragment to show edtion content
     *
     * If agenda ( accidents, tasks or events ) then TaskEditModel, EventEditMode or CarAccidentEdtiModel
     * If settings then the Settings model
     */
    fun  getEditionContent(activity: AgendaBaseActivity ) : Fragment {
        return when( activity.intent.getSerializableExtra(NAVIGATION) as? Navigation? ){
            AGENDA_EDITION -> getAgendaEditModel(activity )
            else -> SettingsFragment()
        }
    }

    /**
     * Get agenda edit model depending on the agenda object to create or update
     * AccidentEditMdel, TaskModel or EventEditmodel
     */
    private fun  getAgendaEditModel( activity: AgendaBaseActivity ) : Fragment {

        val agenda = activity.intent.getSerializableExtra(AGENDA_OBJECT) as AgendaBean

        val model = when ( agenda.getAgenda()?.mType ) {
            TASK.ordinal -> TaskEditModel( agenda  )
            EVENT.ordinal -> EventEditModel(agenda )
            else -> AccidentEditModel(agenda )
        }

        return model.createContent( activity.getStorage() )
    }

    // Create toolbar, the model is selected by agenda model mangger depending of the
    // type of activity
    //
    // Edition activities -> icon right -> cancel    icon left -> accept
    // Rest of activities -> icon right -> back         icon left -> settings
    fun getToolBarGenerator( activity: AppCompatActivity ) : ToolBarInfo {

        return when{
            activity is AgendaEditActivity  || activity is SettingsEditActivity
            -> ToolBarInfo( toolbar_edit , R.drawable.ic_cancel )
            else -> ToolBarInfo( toolbar_info , R.drawable.ic_back )
        }
    }
}