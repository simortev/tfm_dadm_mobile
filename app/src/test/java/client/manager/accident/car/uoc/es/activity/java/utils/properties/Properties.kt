package client.manager.accident.car.uoc.es.activity.java.utils.properties

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.MODE_MOCK


/**
 * Class with all the properties used in tests
 */
object Properties {


    /** Properties from properties file */
    var mProperties: Map<String,String> = mapOf(
        "url" to "http://localhost:8081/car_accident_manager-1.0-SNAPSHOT",
        "bad_user" to "juan",
        "bad_password" to "failure",
        "agenda.car.accident.short_description" to "Short description",
        "agenda.car.accident.city" to "London",
        "agenda.car.accident.country" to "England",
        "agenda.car.accident.policy.id" to "1",
        "agenda.car.accident.damages.description1" to "The right side window has been broken 1",
        "agenda.car.accident.damages.description2" to "The right side window has been broken 2",
        "customer_user" to "luis",
        "customer_password" to "luis",
        "insurance_staff_user" to "vicente",
        "insurance_staff_password" to "vicente",
        "professional_staff_user_1" to "juan",
        "professional_staff_password_1" to "juan",
        "professional_staff_user_2" to "pedro",
        "professional_staff_password_2" to "pedro")

    /** Properties from properties file */
    var mMockProperties: Map<String,String> = mapOf(
            "url" to "http://localhost:8081/car_accident_manager-1.0-SNAPSHOT",
            "bad_user" to "juan",
            "bad_password" to "failure",
            "agenda.car.accident.short_description" to "Short description",
            "agenda.car.accident.city" to "London",
            "agenda.car.accident.country" to "England",
            "agenda.car.accident.policy.id" to "1",
            "agenda.car.accident.damages.description1" to "The right side window has been broken 1",
            "agenda.car.accident.damages.description2" to "The right side window has been broken 2",
            "customer_user" to "mock",
            "customer_password" to "mock",
            "insurance_staff_user" to "mock",
            "insurance_staff_password" to "mock",
            "professional_staff_user_1" to "mock",
            "professional_staff_password_1" to "mock",
            "professional_staff_user_2" to "mock",
            "professional_staff_password_2" to "mock")

    /**
     * Get property value
     * @param propertyName the property name
     * @return tge property
     */
    operator fun get(propertyName: String ) : String {

        return when( MODE_MOCK ){
            true -> mMockProperties[propertyName] ?: ""
            false -> mProperties[propertyName] ?: ""
        }
    }
}
/**
 * Constructor , this calls never can be instantiated
 */
