package client.manager.accident.car.uoc.es.data.remote.rest.actions

import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation
import java.net.HttpURLConnection


/**
 * This class represents a Rest connection , it contains the logic to send the HTTP request
 * and process the HTTP response
 *
 * There are 4 different types of RestAction, this is an interface with a bean type as Generic
 *
 *     + It also contains the operation ( see OperationDao object )
 *     + It will contain the bean response ( always a bean or a list the beans )
 *     + The function to send the body of the RestFul request if it is necessary to do it
 *     + The function to process the RestFul response
 *
 * RestObjectAction:  Get, Add, Update or Remove a bean,  always receives an object
 * RestListAction: Get a list of object, always receives a list
 * RestFileUploadAction, upload a document and receives a DocumentBean object
 * RestFileDownloadAction, download a document and although only receives the content file from
 *                         backend, it returns the DocumentBean object passed when invoke the request
 */

abstract class RestAction<ResponseBeanType> constructor(
        val mOperation: Operation,
        val mRelativeUrl: String,
        val mParams: Map<String,String> ? ) {

     var mResponseBean : ResponseBeanType? = null

     open fun sendRequest(httpConnection : HttpURLConnection){
     }

     open fun receiveResponse( httpConnection : HttpURLConnection ) {

     }
}