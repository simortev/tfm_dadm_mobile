package client.manager.accident.car.uoc.es.activity.java
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.DOCUMENTS_NUM
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.MODE_MOCK
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.PROPERTIES_FILE
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.DocumentUtils
import client.manager.accident.car.uoc.es.activity.java.utils.DocumentUtils.RandomInputStream
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.daos.DocumentDao
import client.manager.accident.car.uoc.es.data.mock.data.MockData.MOCK_FILE_CONTENT
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test

import java.util.Arrays

import java.io.*


class TestDocument {

    /** Login user , and maintain cookie */
    companion object {

        /** The agenda bean added  */
        var mCarAccident: AgendaBean? = null

        @BeforeClass
        @JvmStatic
        fun login() {
            LoginUtils.login()

            mCarAccident = AgendaUtils.createCarAccident()
            mCarAccident = TestUtils.addBeanItem( mCarAccident!! , ::AgendaDao)

            TestUtils.assertBeanItemMatchesDataBaseItem( mCarAccident , " add accident " , ::AgendaDao)
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            TestUtils.removeBeanItem( mCarAccident!! , ::AgendaDao)
            TestUtils.assertNotBeanItemInDataBase( mCarAccident!!.mId!! , ::AgendaDao )

            LoginUtils.logout()
        }
    }


    /**
     * + Add an new agenda event  ( get it and verify the operation was well performed )
     * + Update agenda event ( get it and verify the operation was well performed )
     * + Remove agenda event ( get it and verify no event has been received )
     */
    @Test
    fun testAddDocument() {


        /**************** GET DOCUMENT CONTENT  */
        var original_document : ByteArray ? =null
        try {
            val fileInputStream = RandomInputStream()
            original_document = ByteArray(fileInputStream.available())
            fileInputStream.read(original_document)
            fileInputStream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var input = RandomInputStream()

        DocumentUtils.createDocuments( DOCUMENTS_NUM, mCarAccident!!.getId()!!, PROPERTIES_FILE )
                .map { document -> DocumentUtils.uploadDocument( document , input ) }
                .toList()

        /** Get agenda bean from RestFul service and verify it  */
        var hDocuments = DocumentUtils.getDocuments( mCarAccident!!.mId!!)

        Assert.assertNotNull("There was an error get documents ", hDocuments)
        Assert.assertTrue("There was an error get documents , bad number",
                hDocuments?.size == DOCUMENTS_NUM)



        for (document in hDocuments) {
            var out = ByteArrayOutputStream()

            val doc1 = DocumentUtils.downloadDocument( document , out )
            TestUtils.assertBeanItemMatches( document , doc1 , " get documents ")

            if(MODE_MOCK == false ){
                Assert.assertTrue("Files are not equal", Arrays.equals(original_document, out.toByteArray()))
            }
            else {
                Assert.assertTrue("Files are not equal", Arrays.equals(MOCK_FILE_CONTENT.toByteArray(), out.toByteArray()))
            }


            var document = TestUtils.removeBeanItem( document , ::DocumentDao )
            TestUtils.assertNotBeanItemInDataBase( document!!.mId!! , ::DocumentDao )
        }
    }
}

