package client.manager.accident.car.uoc.es.model.agenda.info.list

import android.view.View
import android.view.View.GONE
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R

import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.model.agenda.info.TaskContentModel
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IMultiPaneModel
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager

import java.util.*

/**
 * Create the list for task object
 */
class TaskListModel(parent : AgendaBean ? ,activity: AgendaBaseActivity  ) :
        AgendaListModel<AgendaBean>( parent, activity ) {

    /**
     * Get tasks of an accident from the repository
     */
     override fun getData() : ITaskDao<List<AgendaBean>>{
        return AgendaDao().getChildren( mParent?.getId()!! )
    }

    // Get righ icon -> an arrow to navigate
    override  fun getCardRightIconId(obj: AgendaBean): Int ? {
        return R.drawable.ic_right_arrow
    }

    // Get left icon of the task depending on the task type
    override  fun getCardLeftIconId(obj: AgendaBean): Int ? {
       return AgendaDataBindingManager.getTaskIconId( obj)
    }

    // Get car content on the task
    override fun getCardContent(obj: AgendaBean ): View {
        val view = super.getCardContent(obj)

        view.findViewById<TextView>(R.id.card_body)?.text =
                AgendaDataBindingManager.getTaskDescription( mActivity , obj.mSubtype!! )

        return view
    }

    // Get the content model for landscape tablets to show a specific task
    override fun getContentModel(obj: AgendaBean): IMultiPaneModel {
        return TaskContentModel(obj)
    }
}