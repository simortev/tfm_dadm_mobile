package client.manager.accident.car.uoc.es.model.agenda.info.list


import android.view.Gravity
import android.view.View
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R.color.cardBackgroundNoSelected
import client.manager.accident.car.uoc.es.activity.R.layout.mb_info
import client.manager.accident.car.uoc.es.activity.view.list.GenericRecyclerViewAdapter
import client.manager.accident.car.uoc.es.activity.view.list.ListModel
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.BlockViewHolder
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.daos.CarDamageDao
import client.manager.accident.car.uoc.es.data.local.DataVariable.AGENDA_ITEM
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager


/**
 * Create a list showing information of damages
 * All the vehicles involved in accident with their damages
 * CarDamageBean = damages + car owner + policy info
 */
class DamagesListInfoModel(rootView : View, activity: AgendaBaseActivity)
    : ListModel<CarDamageBean>( activity , rootView )  {

    // No add button
    init {
        disableAddButton()
    }

    // Create thie list
    override fun createList( )  {
        onRefreshList()
    }

    // Get the accident selected, get damages for this accident
    // The other cars involved
    // CarDamageBean = damages + car owner + policy info
    override fun onRefreshList() {

        val agenda = mActivity.getStorage().getData( AGENDA_ITEM ) as? IAgendaBean?

        CarDamageDao()
            .getAccidentDamages( agenda?.getAgenda()?.mId!!)
            .onResponse( DamagesListInfoModel::createListView, this)
            .execute()
    }

    // Create list when the response is received
    private fun createListView(hList : List<CarDamageBean> ? ) {

        finishRefresh()

        hList?.let {
            mRecyclerView?.adapter = GenericRecyclerViewAdapter(this, hList )
        }
    }

    // No handlers on icons or contents
    override fun onRightIconClick(obj: CarDamageBean) {

    }

    override fun onLeftIconClick(obj: CarDamageBean) {

    }

    override fun onContentClick(obj: CarDamageBean) {

    }

    override fun onAddButtonClick() {

    }

    // No right icon
    override  fun getCardRightIconId(obj: CarDamageBean): Int ? {
        return null
    }

    // No left icon
    override  fun getCardLeftIconId(obj: CarDamageBean): Int ? {
        return null
    }

    override fun getCardRightIconGravity(obj: CarDamageBean): Int? {
        return Gravity.CENTER
    }

    override fun getCardLeftIconGravity(obj: CarDamageBean): Int? {
        return Gravity.TOP
    }

    // Only card conent as a block
    override fun getCardContent(obj: CarDamageBean): View {
        return AgendaDataBindingManager.getBlockForInfoCarDamage( obj )
                .let { BlockViewHolder(  it ) }
                .apply { createView(null,mActivity.layoutInflater) }
                .let { it.mCardView!! }
    }

    // Get backgroun color
    override fun getCardBackgroundColor(selected: Boolean): Int {
        return cardBackgroundNoSelected
    }

    override fun isCardSelected( obj : CarDamageBean ) : Boolean {
        return false
    }
}