package client.manager.accident.car.uoc.es.model.agenda.info

import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.EventInfoFragment
import client.manager.accident.car.uoc.es.data.beans.AgendaBean

class EventContentModel( agenda : AgendaBean )
    : AgendaContentModel<AgendaBean>( agenda , ::EventInfoFragment )
