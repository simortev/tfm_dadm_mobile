package client.manager.accident.car.uoc.es.application.utils.biometry

interface BiometryListener{
    fun onBiometryAuthentication( bAuthenticate : Boolean )
}