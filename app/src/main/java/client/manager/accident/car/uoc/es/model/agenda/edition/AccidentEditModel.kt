package client.manager.accident.car.uoc.es.model.agenda.edition

import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.AccidentEditionFragment
import client.manager.accident.car.uoc.es.data.beans.AgendaBean



class AccidentEditModel(agenda : AgendaBean ) : AgendaEditModel<AgendaBean>(agenda, ::AccidentEditionFragment )
