package client.manager.accident.car.uoc.es.model.agenda.edition

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.TextView.BufferType.EDITABLE
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.daos.DocumentDao

import client.manager.accident.car.uoc.es.activity.view.list.ListModel
import client.manager.accident.car.uoc.es.activity.view.list.GenericRecyclerViewAdapter
import client.manager.accident.car.uoc.es.application.GlobalConstants.ANY_FILE
import client.manager.accident.car.uoc.es.application.GlobalConstants.FILE_DIALOG_SERVICE
import client.manager.accident.car.uoc.es.application.utils.IActivityResultListener
import java.io.File


/**
 * Create a list of documents of an accident task or event in edition mode, upload files to the
 * repository
 *
 * It is possible to add or remove a file
 *
 * Each item has a left icon -> file a right icon -> remove file , and content with name and
 * description
 *
 * There is a floating button on the booton right to add a new file
 *
 * When a file is uploaded the list is updated and later on clicking on the content of the row
 * teh file pass to edit mode and name or description can be changed, in this case
 * the left icon turn into the cancel icon and the right icon turn into the accept icon
 */
class DocumentUploadListModel(rootView : View, activity: AgendaBaseActivity, private var mAgenda : IAgendaBean)
    : ListModel<DocumentBean>( activity, rootView ) , IActivityResultListener {

    // When click on the content of a card of a file, then the name or descrition can be changed
    // This are the document and view of this document
    var mEditDocument : DocumentBean ? = null
    var mEditViewCard : View ? = null

    // Create list
    override fun createList() {
        onRefreshList()
    }

    // Refresh list if a document has been added
    private fun onAddDocument(document : DocumentBean? ){
        onRefreshList()
    }

    // If the name or descrition of a document has been updated, refresh the list
    private fun onUpdateDocument( dcoument: DocumentBean?){
        mEditDocument = null
        mEditViewCard = null

        onRefreshList()
    }

    // if a document has been removed, refresh the list
    private fun onRemoveDocument( document: DocumentBean?){
        onRefreshList()
    }

    // Get document info form the respository
    override fun onRefreshList() {
        DocumentDao()
                .getDocuments( mAgenda.getId()!! )
                .onResponse( DocumentUploadListModel::createListView , this )
                .execute()
    }

    // Create list on response
    private fun createListView(hList : List<DocumentBean> ? ) {
        finishRefresh()

        hList?.let { mRecyclerView?.adapter = GenericRecyclerViewAdapter(this, hList ) }

        if( mEditDocument == null ){
            enableAddButton()
        }else{
            disableAddButton()
        }
    }

    /**
     * Left icon
     *  Default a file icon
     *  Edition mode -> cancel icon
     */
    override fun getCardLeftIconId(obj: DocumentBean): Int? {
        return when(obj.getId() == mEditDocument?.getId() ){
            true -> R.drawable.ic_cancel
            false -> R.drawable.ic_file
        }
    }

    /**
     * Right cion
     * Defaul t mode -> remove icon
     * Edition mode -> accept icon
     */
    override fun getCardRightIconId(obj: DocumentBean): Int? {
        return when(obj.getId() == mEditDocument?.getId() ){
            true -> R.drawable.ic_accept
            false -> R.drawable.ic_remove
        }
    }

    override fun getCardRightIconGravity(obj: DocumentBean): Int? {
        return Gravity.CENTER
    }

    override fun getCardLeftIconGravity(obj: DocumentBean): Int? {
        return Gravity.CENTER
    }

    override fun getCardBackgroundColor(selected: Boolean): Int {
        return when {
            selected -> R.color.cardBackgroundSelected
            else -> R.color.cardBackgroundNoSelected
        }
    }

    override fun isCardSelected(obj: DocumentBean): Boolean {
        return false
    }

    /**
     * Get card content
     * Default -> TextView with name and description ( info )
     * Edition -> TextEdit to change name or description ( edition )
     */
    override fun getCardContent(obj: DocumentBean): View {
        return when(obj.getId() == mEditDocument?.getId() ){
            true -> getEditCardContent(obj)
            false -> getInfoCardContent( obj )
        }
    }

    /**
     * Righ icon acction
     * Default -> remove file  ( remove icon )
     * Edition -> update file ( accept icon )
     */
    override fun onRightIconClick(obj: DocumentBean) {
        when(obj.getId() == mEditDocument?.getId() ){
            true -> updateDocument(obj)
            false -> removeDocument( obj )
        }
    }

    /**
     * left icon action
     * Default -> nothing , file icon ( info )
     * Edition -> cancel edition
     */
    override fun onLeftIconClick(obj: DocumentBean) {
        if( obj.getId() != mEditDocument?.getId() ) {
            return
        }

        mEditDocument = null
        mEditViewCard = null

        createListView( ( mRecyclerView?.adapter as? GenericRecyclerViewAdapter<DocumentBean>? )?.mList )
    }

    /**
     * On content click , go to edition mode for this document
     */
    override fun onContentClick(obj: DocumentBean) {
        mEditDocument = obj
        mEditViewCard = null
        createListView( ( mRecyclerView?.adapter as? GenericRecyclerViewAdapter<DocumentBean>? )?.mList )
    }

    /**
     * On add button an activity is launchend in order to request the file to upload
     * On response the file will be uploaded
     */
    override fun onAddButtonClick() {
       val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = ANY_FILE
        }

        mActivity.startActivityForResult( intent , FILE_DIALOG_SERVICE ,this )
    }

    /**
     * When the file is selected then upload it
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
    if (requestCode != FILE_DIALOG_SERVICE || resultCode != RESULT_OK) {
          return
        }

        resultData?.data?.let { addDocument( it ) }
    }

    /**
     * View for a document in info mode
     */
    private fun getInfoCardContent(obj: DocumentBean): View {

        val view = mActivity.layoutInflater.inflate(R.layout.card_info_document, null, false)
        view.findViewById<TextView>(R.id.file_name)?.text = obj.mFileName?:""
        view.findViewById<TextView>(R.id.file_description)?.text = obj.mShortDescription?:""

        return view
    }

    /**
     * View for a document in edit mode
     */
    private fun getEditCardContent(obj: DocumentBean): View {

        mEditViewCard = mActivity.layoutInflater.inflate(R.layout.card_edit_document, null, false)
        mEditViewCard?.findViewById<EditText>(R.id.file_name)?.setText( obj.mFileName?:"" , EDITABLE)
        mEditViewCard?.findViewById<EditText>(R.id.file_description)?.setText(obj.mShortDescription?:"", EDITABLE)

        return mEditViewCard!!
    }

    /**
     * Add a document in the remote repository
     */
    private fun addDocument(uri : Uri) {

        val input = mActivity.contentResolver.openInputStream(uri)

        val document = DocumentBean()
        document.mAgendaId = mAgenda.getId()
        document.mFileName = File( uri.path).name

        DocumentDao()
                .uploadDocument( document, input!! )
                .onResponse( DocumentUploadListModel::onAddDocument , this )
                .execute()
    }

    /**
     * Update information of a document in remote repository
     */
    private fun updateDocument(obj: DocumentBean) {

        val document = DocumentBean()
        document.mAgendaId = obj.mAgendaId
        document.mId = obj.mId
        document.mFileName =
                mEditViewCard?.findViewById<TextView>(R.id.file_name)?.text?.toString()
        document.mShortDescription =
                mEditViewCard?.findViewById<TextView>(R.id.file_description)?.text.toString()

        DocumentDao()
                .updateObject( document )
                .onResponse(DocumentUploadListModel::onUpdateDocument, this)
                .execute()
    }

    /**
     * Remove un document
     */
    private fun removeDocument(obj: DocumentBean) {
        DocumentDao()
                .removeObject(obj)
                .onResponse(DocumentUploadListModel::onRemoveDocument, this)
                .execute()
    }

}