package client.manager.accident.car.uoc.es.data.remote.rest

import android.os.AsyncTask
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.data.remote.rest.actions.*


import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.ASYNC
import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.SYNC

import client.manager.accident.car.uoc.es.application.GlobalConstants.mUrl
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_QUERY_PARAMETER_SEPARATOR
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_QUERY_PREFIX
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_QUERY_SEPARATOR
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.ACCEPT_HEADER
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.APPLICATION_JSON
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.APPLICATION_OCTET_STREAM
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.STATUS_OK
import java.net.HttpURLConnection
import java.net.URL


/**
  * This is a class representing a task to do a rest connection witch response is a bean (user,agenda,damages,document)
 *
 *  The base class is ITaskDao, really is the interface see by the code, the code never see this RestTaskDao
 *
 *  This class implements the interface
 *
 *  onResponse -> set an object and the handler with the response. The handler is a function to manage the
 *  response, it has 2 parameters, the object set and the response.  This is useful because this object
 *  can be the 'this' parameter and the handler a class function
 *
 *  cancel -> cancel notification.
 *
 *  execute -> execute connection
 *
 *  Cancel, execute and onResponse are invoked in the same thread, but notifyResponse in a different one
      this functions are synchronized
 *
 *  In an ASYNC mode, the connection is executed in a different thread from the thread invoking the request
 *
 *  cancel only avoid notification
 *
 *  Internally there are 2 different task classes
 *
 *      Sync task , request and notification in the same thread used for testing purposes
 *      Async task , where connection and notification are executed in different thread, used in mobile application
 */

class RestTaskDao<ResponseBeanType>(var mRestAction : RestAction<ResponseBeanType>) : ITaskDao<ResponseBeanType> {

    /** See the explanation of this object in the definition of the interface */
    private var mNotifier : IConnectionNotifier<ResponseBeanType>? = null

    /** Create notification, object and handler ( function ) */
    @Synchronized
    override fun <ResponseAttachedObject> onResponse(
            responseHandle: (ResponseAttachedObject, ResponseBeanType?) -> Unit ,
            obj: ResponseAttachedObject ): ITaskDao<ResponseBeanType> {

        mNotifier = RestConnectionNotifier(obj, responseHandle)
        return this
    }

    /** Execute the task in an async ( mobile app ) or sync ( testing ) way */
    override fun execute(): ITaskDao<ResponseBeanType> {
        when( GlobalConstants.mConnectionType) {
            SYNC -> RestConnectionSyncTask(this).execute()
            ASYNC -> RestConnectionAsyncTask(this).execute()
        }
        return this
    }

    /** Avoid notification, similar behavior as live data of Google */
    @Synchronized
    override fun cancel() {
        mNotifier = null
    }

    /** Notify response, finally the handler will be notified */
    @Synchronized
    private fun notifyResponse(  ){
        mNotifier?.notifyResponse( mRestAction.mResponseBean )
    }


    /** Create a HTTP Rest connection,
     *
     * RestAction contains the code to do the request
     *
     * There are 4 different types of RestAction, this is an interface with a bean type as Generic
     *
     *     + It will contain the bean response ( always a bean or a list the beans )
     *     + The function to send the body of the RestFul request if it is necessary to do it
     *     + The function to process the RestFul response
     *
     * RestObjectAction:  Get, Add, Update or Remove a bean,  always receives an object
     * RestListAction: Get a list of object, always receives a list
     * RestFileUploadAction, upload a document and receives a DocumentBean object
     * RestFileDownloadAction, download a document and although only receives the content file from
     *                         backend, it returns the DocumentBean object passed when invoke the request
     *
     *
     *
     * */
     fun createHttpConnection(restAction : RestAction<ResponseBeanType> ) {
        var con: HttpURLConnection? = null

        try {
            if( mUrl == null ){
                throw java.lang.Exception( "URL was no set up" )
            }

            /** Service global url more the relative url specific for this reqeust */
            var url = mUrl + restAction.mRelativeUrl

            /** The url query if there are parameters, in login there are parameters with user and password */
            restAction.mParams
                    ?.map { e -> e.key + URL_QUERY_PARAMETER_SEPARATOR + e.value }
                    ?.joinToString ( prefix = URL_QUERY_PREFIX , separator = URL_QUERY_SEPARATOR )
                    ?.let { query -> url += query }

            /** The connection */
            con = URL( url ).openConnection() as HttpURLConnection
            con.requestMethod = restAction.mOperation.mConnectionMethod.name
            con.setRequestProperty(ACCEPT_HEADER, APPLICATION_JSON+ ", "+APPLICATION_OCTET_STREAM)
            con.useCaches = false
            con.doInput = true

            /** Send body if it is necessary , this is specific of the rest action */
            restAction.sendRequest( con )

            if( con.responseCode != STATUS_OK) {
                throw java.lang.Exception("Error " + con.responseCode + " in request " + url )
            }

            /** Process response if it is necessary */
            restAction.receiveResponse( con )

        } catch (e: Exception) {
           e.printStackTrace()
        }
        finally {
            con?.let { con.disconnect() }
        }
    }

    /** IConnectionNotifier is an internal interface used to contain the response handler and attached object
     *
     * It is used because if RestTaskDao holds the handler and object directly, the generic type of the object
     * should be set when the RestTaskDao is created and no when onResponse is invoked
     *
     *   It is a way to delay the moment to know the attached object type
     *
     *   With delay ( RestTaskDao uses IConnectionNotifier )
     *
     *       UserDao().logout().onResponse(object,handler).execute()
     *
     *   without delay ( RestTaskDao contains handler and object directly )
     *
     *       UserDao(object.class).logout().onResponse(object,handler).execute()
     *
     * */
    private interface IConnectionNotifier<ResponseBeanType> {
        fun notifyResponse( response : ResponseBeanType? )
    }

    /** Implementation of IConnectionNotifier
     *
     * A class with only 1 generic type is transformed in a class with 2 generic types
     *
     * See the previous function to know the explanation
     **/
    private class RestConnectionNotifier<ResponseAttachObject,ResponseBeanType>(
            var mObj: ResponseAttachObject,
            var mResponseHandle: ( (ResponseAttachObject,ResponseBeanType?) -> Unit ) )
        : IConnectionNotifier<ResponseBeanType> {

        override fun notifyResponse( response : ResponseBeanType?) {
            mResponseHandle.invoke( mObj , response )
        }
    }

    /**
     * Sync task to execute request and notification in the same thread
     * Used for testing purposes
     */
    private class RestConnectionSyncTask<ResponseBeanType>( var mTaskDao: RestTaskDao<ResponseBeanType>){
        fun execute() {
            mTaskDao.createHttpConnection(mTaskDao.mRestAction)
            mTaskDao.notifyResponse()
        }
    }

    /**
     * Async task , request and notification in different threads
     * Used by mobile application to avoid mb_block the graphical thread
     */
    private class RestConnectionAsyncTask<ResponseBeanType>( var mTaskDao: RestTaskDao<ResponseBeanType>)
        : AsyncTask<Void, Void, RestTaskDao<ResponseBeanType>>() {

        override fun doInBackground(vararg params: Void): RestTaskDao<ResponseBeanType>? {
            mTaskDao.createHttpConnection(mTaskDao.mRestAction)
            return mTaskDao
        }

        override fun onPostExecute(taskDao: RestTaskDao<ResponseBeanType>) {
            super.onPostExecute(taskDao)
            mTaskDao.notifyResponse()
        }
    }

}
