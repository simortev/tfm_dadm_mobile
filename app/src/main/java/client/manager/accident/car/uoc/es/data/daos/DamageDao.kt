package client.manager.accident.car.uoc.es.data.daos

import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_DAMAGES
import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.ADD
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.DamagesAddAction
import client.manager.accident.car.uoc.es.data.remote.rest.RestTaskDao
import client.manager.accident.car.uoc.es.data.remote.rest.actions.RestObjectAction
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_DAMAGES_POLICY
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao

/**
 * Object used to execute basic operation on damages beans, it extends ObjectDao
 *
 * ADD, UPDATE, REMOVE, GET OBJECT , GET OBJECT BY ID or GET  DamageBean objects
 *
 * when a operation function is invoked a ITaskDao is received
 *
 *  A handler should be established, the response in ASYN mode is notified in a different thread form
 *  the invocation one
 *
 *  ADD, UPDATE, REMOVE, GET OBJECT return a bean or null if there was an error
 *
 *  GET OBJECT LIST, return a list, empty in case of error
 *
        DamagesDao()
        .updateObject( bean )
        .onResponse( ResponseBean<BeanType>::setResponse , response )
        .execute()
 */
class DamageDao : ObjectDao<DamageBean>(  URL_DAMAGES, DamageBean::class.java, Array<DamageBean>::class.java) {

        /** create damage bean by policy number */
        fun addDamageByPolicyNumber( damage : DamageBean? , policyNumber : String ? ): ITaskDao<DamageBean> {
               return when (mMockMode) {
                        true -> MockTaskDao( DamagesAddAction( damage , policyNumber ) )
                        false -> RestTaskDao(RestObjectAction(ADD, String.format(URL_DAMAGES_POLICY,policyNumber),
                        null, damage , DamageBean::class.java))
                }

        }

}