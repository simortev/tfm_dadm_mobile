package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.DAMAGES_DESCRIPTION2_LABEL
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.DamagesUtils
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.daos.DamageDao
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.DamageBean
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test


class TestDamages {

    /** The agenda bean added  */
    private var mCarAccident: AgendaBean? = null

    /** Add damage bean  */
    private var mDamageBean: DamageBean? = null

    /** Login user , and maintain cookie */
    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            LoginUtils.login()
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            LoginUtils.logout()
        }
    }
    /**
     * Test car accident, basically, add a agendabean, update it, get it and finally remove it
     *
     * We make it in only one test function because we need the functions be executed in order
     */
    @Test
    fun testCarAccident() {

        mCarAccident = TestUtils.addBeanItem( AgendaUtils.createCarAccident() , ::AgendaDao)

        testAddDamages()
        testGetDamagesByCarAccident()

        testUpdateDamages()
        testGetDamagesByCarAccident()

        testRemoveDamages()

        /** Remove the car accident  */
        mCarAccident = TestUtils.removeBeanItem( mCarAccident!! , ::AgendaDao )

        /** Verify that the car accident has been removed  */
        TestUtils.assertNotBeanItemInDataBase( mCarAccident!!.mId!! , ::AgendaDao )
    }

    /**
     * Test add car accident
     */
    private fun testAddDamages() {
        mDamageBean = DamagesUtils.createDamages(mCarAccident!!.getId())
        mDamageBean = TestUtils.addBeanItem( mDamageBean!! , ::DamageDao )

        TestUtils.assertBeanItemMatchesDataBaseItem( mDamageBean , " add damage " , ::DamageDao )
    }

    /**
     * Test get danages beans by car accident
     */
    private fun testGetDamagesByCarAccident() {

        val damageBean = mCarAccident
                ?.mId
                ?.let { id -> DamagesUtils.getAccidentDamages( id ) }
                ?.filter { damage -> damage.getId() == mDamageBean?.mId!! }
                ?.first()

        TestUtils.assertBeanItemMatches( mDamageBean!! , damageBean?.mDamages , " get damages of accident ")
    }

    private fun testUpdateDamages() {
        mDamageBean!!.mDamages = Properties[DAMAGES_DESCRIPTION2_LABEL]
        TestUtils.updateBeanItem( mDamageBean!! , ::DamageDao )

        TestUtils.assertBeanItemMatchesDataBaseItem( mDamageBean , " update damage " , ::DamageDao )
    }

    private fun testRemoveDamages() {
        TestUtils.removeBeanItem( mDamageBean!! , ::DamageDao)

        TestUtils.assertNotBeanItemInDataBase( mDamageBean!!.mId!! , ::DamageDao )
    }

}
