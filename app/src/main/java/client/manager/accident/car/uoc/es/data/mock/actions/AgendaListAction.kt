package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.AgendaBean

class AgendaListAction(parentId : Long  ) : GetListAction<AgendaBean>( parentId , AgendaBean::class.java )