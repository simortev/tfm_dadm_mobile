package client.manager.accident.car.uoc.es.data.beans.extended

import client.manager.accident.car.uoc.es.data.beans.DamageBean
import com.google.gson.annotations.SerializedName


/**
 * The damage bean with user and car policy
 */
class CarDamageBean : CarInfoBean() {

    /** The car damages  */
    @SerializedName("damages")
    var mDamages: DamageBean ? = null

    override fun getId(): Long? {
        return mDamages?.mId
    }

    override fun setId( objId : Long ){
        mDamages?.mId = objId
    }

    override fun toString(): String {
        val builder = StringBuilder()

        builder.append(super.toString()).append("\n")

        mDamages?.let{ damages -> builder.append(damages).append("\n") }

        return builder.toString()
    }

}
