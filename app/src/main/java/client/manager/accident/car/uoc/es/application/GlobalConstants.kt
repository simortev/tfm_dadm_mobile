package client.manager.accident.car.uoc.es.application

import android.Manifest.permission.*
import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.ASYNC
import client.manager.accident.car.uoc.es.application.utils.location.LocationExecutor


/**
  * This is a singleton used to request rest information
 */

object GlobalConstants {

    /** Tag used for log purposes*/
    var LOG_TAG = "CAR_ACCIDENT_MANAGER"

    /** Label used in intets to pass agenda bean objects between acivities */
    var AGENDA_OBJECT = "AGENDA_OBJECT"

    /** Label used to inform the next activit is */
    var NAVIGATION = "NAVIGATION"

    var FILE_DIALOG_SERVICE = 100

    /** Permission service */
    var PERMISSION_SERVICE = 200

    /** Max description size */
    val MAX_SHORT_DESCRIPTION_SIZE = 70

    /** The default URL */
    val DEFAULT_URL = "http://192.168.64.1:8081/car_accident_manager-1.0-SNAPSHOT"

    /** Filter used when a file is requested to be uploaded */
    var ANY_FILE = "*/*"

    /** The url of the rest service */
    var mUrl: String? = null

    /** The date time format */
    val DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm"

    /** Permission needed by the application */
    var mPermissions = arrayOf(
            WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE,
            ACCESS_FINE_LOCATION,
            ACCESS_COARSE_LOCATION)

    // Spanish locale
    var SPANISH_LOCALE = "es"

    // Enghlish locale
    var ENGLISH_LOCALE = "en"

    /** Locales supported by the appication */
    val mLocale = arrayOf( ENGLISH_LOCALE , SPANISH_LOCALE )

    /** Type of Rest connection, in this case ASYNC , in test is SYNC */
    var mConnectionType : RestConnectionType = ASYNC

    /** The user to pass to mode mock */
    val MOCK_USER = "mock"

    /** If it is in mode mock or not */
    var mMockMode = false

    /** When text view */
    var mModeTestView = false

    var mLocationExecutor : LocationExecutor? = null

    enum class RestConnectionType {
        SYNC,
        ASYNC
    }

    enum class Navigation {
        AGENDA_EDITION,
        SETTINGS_EDITION
    }
}
