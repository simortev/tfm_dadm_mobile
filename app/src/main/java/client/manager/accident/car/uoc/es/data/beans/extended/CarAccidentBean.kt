package client.manager.accident.car.uoc.es.data.beans.extended

import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import com.google.gson.annotations.SerializedName

/**
 * The car accident bean, used when the list is requested
 */
class CarAccidentBean : CarInfoBean(), IAgendaBean {

    @SerializedName("agenda")
    var mCarAccident: AgendaBean ? = null

    override fun getId(): Long? {
        return mCarAccident?.mId
    }

    override fun setId( objId : Long ){
        mCarAccident?.mId = objId
    }

    override fun getAgenda(): AgendaBean? {
       return mCarAccident
    }


    override fun toString(): String {
        val builder = StringBuilder()

        builder.append(super.toString()).append("\n")

        mCarAccident?.let { carAccident -> builder.append(carAccident).append("\n") }

        return builder.toString()
    }

}
