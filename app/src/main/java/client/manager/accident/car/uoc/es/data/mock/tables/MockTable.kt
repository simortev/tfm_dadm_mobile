package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.OBJECT_BY_ID_INDEX
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.PARENT_ID_FOR_ACCIDENTS
import java.util.*

open class MockTable<ResponseBeanType: IObjectBean>{

    private var mId = Date().time

    /** The object index for this table  */
    private var mObjectIndex = mutableMapOf<MockNames,IMockIndex<ResponseBeanType>>()

    /**
     * Constructor
     */
    init {
        addSimpleIndex(OBJECT_BY_ID_INDEX, IObjectBean::getId )
    }

    /**
     * add an index for this type of object
     */
    fun addSimpleIndex(operation: MockNames, function: (ResponseBeanType) -> Any?  ) {
        mObjectIndex.put( operation , MockIndex( function ) )
    }

    /**
     * add a multiple index for this type of object
     */
    fun addMultipleIndex(operation: MockNames, function: (ResponseBeanType) -> Any?  ) {
        mObjectIndex.put( operation , MockMultiIndex( function ) )
    }

    /**
     * Add object of type and index it by the value of function
     */
    fun addObject( obj : ResponseBeanType) : ResponseBeanType {

        if( obj.getId() == null ) { obj.setId( mId++ )}

        if( obj is AgendaBean ){
          if( obj.mStartDate == null )  obj.mStartDate = Date()
          if( obj.mParentId == null ) obj.mParentId = PARENT_ID_FOR_ACCIDENTS
        }

        mObjectIndex.values.map { it.addObject( obj ) }

        return obj
    }

    /**
     * Remove the object
     */
    open fun removeObject( obj : ResponseBeanType ) : ResponseBeanType {
        mObjectIndex.values.map { it.removeObject( obj ) }

        return obj
    }

    /**
     * Remove the object by object id
     */
    open fun removeObject( objectId : Long ) : ResponseBeanType? {
        return getObjectById(objectId)?.let { removeObject( it ) }
    }

    /**
     * Get object by id
     */
    fun getObjectById( objectId : Long ) : ResponseBeanType?  {
        return getObject( OBJECT_BY_ID_INDEX,objectId)
    }

    /**
     * Get object
     */
    fun getObject(name: MockNames, key : Any  ) : ResponseBeanType? {
       return ( mObjectIndex[name] as? MockIndex? )?.getObject( key )
    }

    /**
     * Get list
     */
    fun getList(name: MockNames, key : Any  ): MutableCollection<ResponseBeanType> {
        return ( mObjectIndex[name] as? MockMultiIndex? )?.getList( key )?: mutableSetOf()
    }
}