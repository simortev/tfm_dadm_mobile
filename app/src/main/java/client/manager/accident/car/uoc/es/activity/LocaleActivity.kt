package client.manager.accident.car.uoc.es.activity

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import client.manager.accident.car.uoc.es.activity.R.string.app_name
import client.manager.accident.car.uoc.es.data.local.DataStorage
import client.manager.accident.car.uoc.es.application.utils.LocaleHelper

/**
 * This class is the base activity class
 */
abstract class LocaleActivity( ) :  AppCompatActivity(){

    // This is the data storage for activities or shared preferences
    protected var mStorage : DataStorage? = null

    init {
        mStorage = DataStorage( this  )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mStorage?.restore( savedInstanceState )
        LocaleHelper.onAttach(this)
    }

    // Set locale in each activity
    override fun attachBaseContext(base: Context) {
       super.attachBaseContext(LocaleHelper.onAttach(base))
    }

    // Save the information from activity storage into bundle
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mStorage?.save( outState )
    }

    // Restore the information form bundle to storage
   /* override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        mStorage?.restore( savedInstanceState )
    }
*/
    // Recreate activity
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        recreate()
    }

    // Get storage
    fun getStorage() : DataStorage {
        return mStorage!!
    }

    open fun getSubTitle() : String? {
        return null
    }

    // Set content view
    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)

        var title = resources.getString(app_name)

        getSubTitle()?.let { title = title + " - " + it }

        setTitle( title )
    }
}