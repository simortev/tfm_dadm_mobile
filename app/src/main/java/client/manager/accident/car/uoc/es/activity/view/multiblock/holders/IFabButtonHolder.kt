package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

/**
 * Used to implement a fab button holder
 */
interface IFabButtonHolder{

    /** Get button id */
    fun getButtonId() : Int

    /** Get on click button listener  */
    fun onClickButton()
}