package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.R.id.block_value
import client.manager.accident.car.uoc.es.activity.R.layout.mb_info
import client.manager.accident.car.uoc.es.activity.view.multiblock.BlockText
import client.manager.accident.car.uoc.es.application.GlobalConstants.LOG_TAG


/**
 * This is a block of info, it contains a label and a text data
 */
class BlockTextViewHolder( private var mBlockText : BlockText )
    : BlockItemViewHolder( mb_info , mBlockText.mLabelId ) {

    // The text view to show the value of data
    private var mTextView: TextView? = null

    // Create view
    override fun createView(parentView: ViewGroup, layoutInflater: LayoutInflater): ViewGroup? {

        val blockView = super.createView( parentView , layoutInflater)

        mTextView = blockView?.findViewById(block_value)

        mTextView?.text = mBlockText.mValueText

        Log.d(LOG_TAG, parentView.resources.getString(mLabelId) + "=" + mBlockText.mValueText)

        return blockView
    }
}