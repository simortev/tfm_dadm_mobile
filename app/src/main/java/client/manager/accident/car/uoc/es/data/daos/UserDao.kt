package client.manager.accident.car.uoc.es.data.daos

import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.MOCK_USER
import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.UserBean
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_LOGIN
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_LOGIN_PARAMETER
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_LOGOUT
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_PASSWORD_PARAMETER
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.LOGIN
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.LOGOUT
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.LoginAction
import client.manager.accident.car.uoc.es.data.mock.actions.LogoutAction
import client.manager.accident.car.uoc.es.data.mock.services.MockService

/**
 * Object used to login and loogut users
 *
 * During login or logout a user bean is received if the request was ok and null if was no ok
 *
 *
 * when a operation function is invoked a ITaskDao is received
 *
 *  * A handler should be established, the response in ASYN mode is notified in a different thread form
 *  the invocation one
 *
 *    UserDao()
 *        .logout()
 *        .onResponse( LoginVerifier::verifyLogout , LoginVerifier())
 *        .execute()
 *
 */
class UserDao: ObjectDao<CarInfoBean>("", CarInfoBean::class.java, Array<CarInfoBean>::class.java) {


    /**
     * Function for login user
     */
    fun login( userName : String , userPassword : String ) : ITaskDao<CarInfoBean> {

    mMockMode = MOCK_USER.equals( userName )

    if( mMockMode == true ) {
        MockService.start()
    }

    return when( mMockMode ){
        true -> MockTaskDao( LoginAction( userName , userPassword ) )
        false -> getObject(LOGIN,URL_LOGIN , mapOf( URL_LOGIN_PARAMETER to userName  , URL_PASSWORD_PARAMETER to userPassword  )  )
        }

    }

    /**
     * Funciton for logout user
     */
    fun logout(): ITaskDao<CarInfoBean> {
        return when( mMockMode ){
            true -> MockTaskDao( LogoutAction() )
            false -> getObject( LOGOUT , URL_LOGOUT)
        }
    }
}