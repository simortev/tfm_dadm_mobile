package client.manager.accident.car.uoc.es.activity.view.list


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import client.manager.accident.car.uoc.es.activity.R.layout.list_card
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListHolderSelectedListener

class GenericRecyclerViewAdapter<T: IObjectBean>(
        private var mListModel: ListModel<T>,
        var mList : List<T> )

    : RecyclerView.Adapter<GenericViewHolder<T>>() , IListHolderSelectedListener<T> {

    private var mSelectedHolder : GenericViewHolder<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<T> {

       return GenericViewHolder( LayoutInflater.from(parent?.context)
               .inflate(list_card, parent, false) , mListModel , this )
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: GenericViewHolder<T>, position: Int) {
        holder?.bindData(mList[position]  )
    }

    override fun onHolderSelected(holder:GenericViewHolder<T> ) {
        mSelectedHolder?.setBackgroundColor( false )
        mSelectedHolder = holder
        mSelectedHolder?.setBackgroundColor( true  )
    }
}