package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView.BufferType.EDITABLE
import client.manager.accident.car.uoc.es.activity.R.id.block_value
import client.manager.accident.car.uoc.es.activity.R.layout.mb_text_edit
import client.manager.accident.car.uoc.es.activity.view.multiblock.BlockEditDateTime
import client.manager.accident.car.uoc.es.activity.view.multiblock.BlockEditText
import client.manager.accident.car.uoc.es.application.GlobalConstants.LOG_TAG

/**
 * This is a block for edition , it contains a lavel and a EditText to modify the data value
 */

class BlockDateTimeEditHolder(private var mBlockEditDate: BlockEditDateTime )
    : BlockItemViewHolder( mb_text_edit , mBlockEditDate.mLabelId ){

    // The edit text
    private var mEditText : EditText ? = null

    // Create view
    override fun createView(parentView: ViewGroup, layoutInflater: LayoutInflater) : ViewGroup ? {

        val blockView = super.createView( parentView , layoutInflater)

        mEditText = blockView?.findViewById(block_value)

  //      mEditText?.setText( mBlockEditText.mValueText , EDITABLE)

   //     Log.d( LOG_TAG,  parentView.resources.getString( mLabelId )  +"=" + mBlockEditText.mValueText )

        return blockView
    }

    // Get the value of text
    fun getValue() : String? {
        return mEditText?.text.toString()
    }
}