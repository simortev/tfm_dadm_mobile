package client.manager.accident.car.uoc.es.activity


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import client.manager.accident.car.uoc.es.activity.view.toolbar.ToolBarView
import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.application.GlobalConstants.NAVIGATION
import client.manager.accident.car.uoc.es.application.GlobalConstants.Navigation.SETTINGS_EDITION
import client.manager.accident.car.uoc.es.application.utils.IActivityResultListener
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.EVENT
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.daos.UserDao
import client.manager.accident.car.uoc.es.model.agenda.AgendaModelManager


/**
 * The agenda base activity, all the other activities extends this, it contains the common code
 * for all of them
 *
 *  Extends activities
 *
 *  AgendaEditActivity : used in edition
 *  AgendaInfoActivity: for content information when mobile or vertical tablet
 *  AgendaListActivity: for lists of accidents, tasks or events, in the case or landscape layout
 *  tablet it also contains the information of the selected item
 *
 *  Basically it manges the tool bar for all the
 */
abstract class AgendaBaseActivity(private var mLayout : Int ) :  LocaleActivity(){

    // The toolbar biew wrapper to facilitate the tool bar creation and management
    private var mToolBar : ToolBarView? = null

    // Listener used when a fragment or other code request a resource that
    // require a response
    // When a file from the file directories are requested for example
    // or camera or other resources
    private var mResultListener : IActivityResultListener? = null

    // On pause is invoked allways the activity goes out of the focus of user
    override fun onPause() {
        super.onPause()

      removeFragments()
    }

    /// Create the menu for toolbar , every
    override fun onCreateOptionsMenu(menu : Menu) : Boolean {
        super.onCreateOptionsMenu(menu)

        mToolBar?.onCreateOptionsMenu( menu )

        return true
    }

    // Create view
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(mLayout)

        // Create toolbar, the model is selected by agenda model mangger depending of the
        // type of activity
        //
        // Edition activities -> icon right -> cancel    icon left -> accept
        // Rest of activities -> icon right -> back         icon left -> settings

        mToolBar = ToolBarView( AgendaModelManager.getToolBarGenerator( this ) , this )
    }


    /**
     * Handel when a tool bar icon is pushed, this class is overriden by each activity in order
     * to manage the specific events
     *
     * When back button is pushed , the activity ends and go to previous activity
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.menu_settings -> editSettings()
            R.id.menu_logout -> logout()
            else -> super.onOptionsItemSelected( item )
        }

        return true
    }

    private fun editSettings( ) {
        startActivity( Intent( this , SettingsEditActivity::class.java ).apply {
            putExtra(NAVIGATION,SETTINGS_EDITION )
        })
    }

    private fun logout() {
        val intent = Intent( this , AuthenticationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        UserDao().logout().execute()
    }


    /** MODEL REQUEST ANY TYPE OF ACTION */

    /**
     * A model start an activity for a result
     */
    fun startActivityForResult(intent : Intent, code : Int,  listener : IActivityResultListener  ){
        startActivityForResult(intent,code)
        mResultListener = listener
    }

    /**
     * When the activity return the result to the model
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
            super.onActivityResult(requestCode, resultCode, resultData)
            mResultListener?.onActivityResult(requestCode,resultCode,resultData)
    }


    /**  DATA HANDLERS */

    /** This function is invoked by models when a object ( accident, task or event ) has been
     * deleted
     *
     * Each activity overrides it
     *
     *  List -> recreate activity
     *  Info -> finish
     *
     * */
    open fun onDeleteBean() {

    }

    /** This function is invoked by models when a object ( accident, task or event ) has been
     * udpated
     *
     * Usually the activity is recreated
     * */
    open fun onUpDateBean() {

    }

    /**
     * Remove fall the fragments
     */
    fun removeFragments() {
        // Unload fragments to avoid be loaded again if for instance the orientation of
        // tablet is changing from landscape to protrait, and no fragment should be loaded
        // exceptions happends if they are not removed previously
        for (fragment in supportFragmentManager.fragments) {
                supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
    }

    // Get subtitle id
    open fun getSubTitleId( type : Int ) : Int? {
        return null
    }

    // Get subtitle
    override fun getSubTitle() : String? {
        val agenda = intent.getSerializableExtra(AGENDA_OBJECT) as? IAgendaBean?

        val type = agenda?.getAgenda()?.mType ?: EVENT.ordinal

        return getSubTitleId( type )?.let { resources.getString( it ) }
    }

}