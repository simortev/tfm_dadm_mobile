package client.manager.accident.car.uoc.es.activity.view.multipane.interfaces

import client.manager.accident.car.uoc.es.activity.view.multipane.PaneInfo

interface IPaneListener{

    fun onPaneSelection( pane : PaneInfo)
}