package client.manager.accident.car.uoc.es.activity


import client.manager.accident.car.uoc.es.activity.R.layout.activity_list_double_pane
import client.manager.accident.car.uoc.es.activity.R.string.*
import client.manager.accident.car.uoc.es.application.utils.DeviceHelper
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.*
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.local.DataVariable
import client.manager.accident.car.uoc.es.model.agenda.AgendaModelManager

/**
 * This activity show a list of accidents, tasks or events, depending on the bean associated to it
 *
 *  When the bean is deleted or updated the list is recreated
 */
class AgendaListActivity : AgendaBaseActivity( activity_list_double_pane)  {

    // Create the list
    override fun onResume() {
        super.onResume()
        onUpDateBean()
    }

    // When a bean has been removed -> update the list
    override fun onDeleteBean() {
       onUpDateBean()
    }

    // When a bean has been update -> update the list
    override fun onUpDateBean() {
        // A model is created and the model create the list
        AgendaModelManager.getListModel(this).createList(  )
    }

    // Get subtitle
    override fun getSubTitleId( type: Int ) : Int? {
       return when( type ){
            CAR_ACCIDENT.ordinal -> tasks_list_title
            TASK.ordinal  -> events_list_title
            else -> accident_list_title
        }
    }
}
