package client.manager.accident.car.uoc.es.activity.view.multiblock

// It represent every piece of information or editio on a block,
// It contains a label
// If BlockText -> TextView
// If BlockEditText -> EditText
// If BlockArray -> A spinner
open class BlockItem(var mLabelId : Int  = 0 )