package client.manager.accident.car.uoc.es.model.agenda.info.list


import android.view.View
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.daos.AccidentDao
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.model.agenda.info.AccidentContentModel
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IMultiPaneModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * Model used to create the list of accidents and get list of accidents from the repository
 */
class AccidentListModel( activity: AgendaBaseActivity  )
    : AgendaListModel<CarAccidentBean>( null , activity ) {

        /** Get all the accidents from the repository */
    override fun getData() : ITaskDao<List<CarAccidentBean>>{
        return AccidentDao().getAccidents()
    }

    /** The arrow icon on the right to navigate */
    override  fun getCardRightIconId(obj: CarAccidentBean): Int ? {
        return R.drawable.ic_right_arrow
    }

    /** The left icon with a car */
    override  fun getCardLeftIconId(obj: CarAccidentBean): Int ? {
        return R.drawable.ic_car
    }

    /**
     * The content of each card in list, a layou for this card is used
     */
    override fun getCardContent(obj: CarAccidentBean ): View {
        val view = super.getCardContent(obj)

        val carModel = obj.mPolicy?.mCarModel
        val carPlateNumber = obj.mPolicy?.mCarPlateNumber

        val ownerNif = obj.mUser?.mNif
        val onwerName = obj.mUser?.mName

        view.findViewById<TextView>(card_body)?.text = "$carModel - $carPlateNumber\n$onwerName -  $ownerNif"

        return view
    }

    /**
     * Return the content model for landscape tablets in a double pane screen
     */
    override fun getContentModel(obj: CarAccidentBean): IMultiPaneModel {
        return AccidentContentModel(obj)
    }
}