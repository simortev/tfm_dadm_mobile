package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean

class DamagesListAction(parentId : Long  ) : GetListAction<CarDamageBean>( parentId , CarDamageBean::class.java)