package client.manager.accident.car.uoc.es.model.agenda.info



import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.DamagesInfoFragment
import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.DocumentsInfoFragment
import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.LocationInfoFragment
import client.manager.accident.car.uoc.es.activity.factory.PanelConstants.*
import client.manager.accident.car.uoc.es.activity.factory.PanelFactory.createPane
import client.manager.accident.car.uoc.es.activity.view.multipane.MultiPaneModel
import client.manager.accident.car.uoc.es.activity.view.multipane.MultiPaneViewHolder
import client.manager.accident.car.uoc.es.activity.view.multipane.PaneInfo
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.CAR_ACCIDENT
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.local.DataStorage
import client.manager.accident.car.uoc.es.data.local.DataVariable.PANEL_INFO

abstract class AgendaContentModel<T : IAgendaBean>(
        protected var mAgenda: T,
        protected var fragmentInfoFactory : () -> Fragment)
    : MultiPaneModel()  {

    private var mStorage : DataStorage? = null

    private var mPaneId : Int = -1

    private var mMultipane : MultiPaneViewHolder? = null

    override fun createContent(storage : DataStorage) : Fragment {

        mStorage = storage

        mPaneId = mStorage?.getData( PANEL_INFO ) as? Int? ?: AGENDA_ITEM_INFO.ordinal

        mMultipane = MultiPaneViewHolder()

        mMultipane?.setContentModel( this )

        mMultipane?.addPane(createPane( fragmentInfoFactory, mAgenda , AGENDA_ITEM_INFO ) )

        mMultipane?.addPane(createPane( ::LocationInfoFragment , mAgenda , AGENDA_LOCATION_INFO ) )

        mAgenda.getAgenda()?.mType
                .takeIf { it == CAR_ACCIDENT.ordinal }
                ?.let {  mMultipane?.addPane(createPane(::DamagesInfoFragment, mAgenda , AGENDA_DAMAGES_INFO ) ) }

        mMultipane?.addPane(createPane(::DocumentsInfoFragment, mAgenda ,AGENDA_DOCUMENTS_INFO) )

        return mMultipane!!
    }

    override fun getPaneBackgroundColor(selected: Boolean): Int {
        return when{
            selected -> R.color.cardBackgroundSelected
            else -> R.color.cardBackgroundNoSelected
        }
    }

    override fun onPaneSelection(pane: PaneInfo) {
        mPaneId = pane.mPaneId
        mStorage?.addData( PANEL_INFO , mPaneId )
    }

    override fun isPanelSelected(panel: Int): Boolean {
        return mPaneId == panel
    }

}