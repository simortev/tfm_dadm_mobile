package client.manager.accident.car.uoc.es.activity.view.multipane

import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IMultiPaneModel
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IPaneGenerator
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IPaneListener

abstract class MultiPaneModel() : IPaneListener, IPaneGenerator, IMultiPaneModel