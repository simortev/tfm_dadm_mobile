package client.manager.accident.car.uoc.es.data.mock.services

import client.manager.accident.car.uoc.es.data.beans.*
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.CAR_ACCIDENT
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role.CUSTOMER
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role.INSURANCE_STAFF
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.mock.data.MockData
import client.manager.accident.car.uoc.es.data.mock.tables.*


/**
 * The mock service
 */
object MockService{

    /** The user table */
    var mUserTable = UserMockTable()

    /** The agenda table */
    var mAgendaTable = AgendaMockTable()

    /** The damages table */
    var mDamagesTable = DamageMockTable()

    /** The documents table */
    var mDocumentsTable = DocumentMockTable()

    /** The policy table*/
    var mPolicyMockTable = PolicyMockTable()

    /** User car info */
    var mCarInfoBean : CarInfoBean? = null

    /** Tables by type of bean */
    private var mTablesByObject = mutableMapOf<Class<*>,MockTable<*>>()

    init {
        mTablesByObject[UserBean::class.java] = mUserTable
        mTablesByObject[AgendaBean::class.java] = mAgendaTable
        mTablesByObject[DamageBean::class.java] = mDamagesTable
        mTablesByObject[DocumentBean::class.java] = mDocumentsTable
        mTablesByObject[PolicyBean::class.java] = mPolicyMockTable
    }

    fun <T: IObjectBean >addObject( obj : T ): T ?{
        return (mTablesByObject[obj.javaClass] as? MockTable<T>?)?.addObject(obj)
    }

    fun <T: IObjectBean >removeObject( obj : T ) : T? {
        return (mTablesByObject[obj.javaClass] as? MockTable<T>?)?.    removeObject(obj)
    }

    fun <T: IObjectBean >removeObject(cl : Class<*>, objectId : Long) : T? {
        return (mTablesByObject[cl] as? MockTable<T>?)?.removeObject(objectId)
    }

    fun <T: IObjectBean >getObject( cl : Class<T>,objectId : Long ) : T? {
        return (mTablesByObject[cl] as? MockTable<T>?)?.getObjectById(objectId)
    }

    fun start(){
        MockData.start()
    }
}