package client.manager.accident.car.uoc.es.data.daos

import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_AGENDA_DOCUMENTS
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_DOCUMENT
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.DocumentListAction
import client.manager.accident.car.uoc.es.data.mock.actions.DownloadDocumentAction
import client.manager.accident.car.uoc.es.data.mock.actions.UploadDocumentAction
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.remote.rest.RestTaskDao
import client.manager.accident.car.uoc.es.data.remote.rest.actions.RestFileDownloadAction
import client.manager.accident.car.uoc.es.data.remote.rest.actions.RestFileUploadAction

import java.io.InputStream
import java.io.OutputStream

/**
 * Object used to execute basic operation on document beans, it extends ObjectDao
 *
 * Object used to upload and download files
 *
 * ADD, UPDATE, REMOVE, GET OBJECT , GET OBJECT BY ID or GET Document bean objects
 *
 * when a operation function is invoked a ITaskDao is received
 *
 *  A handler should be established, the response in ASYN mode is notified in a different thread form
 *  the invocation one
 *
 *  ADD, UPDATE, REMOVE, GET OBJECT return a bean or null if there was an error
 *
 *  GET OBJECT LIST, return a list, empty in case of error
 *
        DocumentDao()
        .updateObject( bean )
        .onResponse( ResponseBean<BeanType>::setResponse , response )
        .execute()

   It contains specific funcitons to upload and download files,
 */

class DocumentDao
    : ObjectDao<DocumentBean>(URL_DOCUMENT, DocumentBean::class.java, Array<DocumentBean>::class.java){

    /** Upload a document */
    fun uploadDocument( document : DocumentBean , input : InputStream ) : ITaskDao<DocumentBean> {
        return when( mMockMode ) {
            true -> MockTaskDao( UploadDocumentAction( document , input ) )
            false -> RestTaskDao(RestFileUploadAction(document, input))
        }
    }

    /** Download a document */
    fun downloadDocument( document : DocumentBean , output : OutputStream ) : ITaskDao<DocumentBean> {
        return when( mMockMode ) {
            true -> MockTaskDao( DownloadDocumentAction( document , output ) )
            false -> RestTaskDao(RestFileDownloadAction(document, output))
        }
    }

    /** Get documents of a specific agenda item */
    fun getDocuments( parentId : Long  ) : ITaskDao<List<DocumentBean>> {

        return when( mMockMode ) {
            true -> MockTaskDao( DocumentListAction( parentId ) )
            false -> getObjectList( String.format(URL_AGENDA_DOCUMENTS,parentId ) )
        }
    }
}