package client.manager.accident.car.uoc.es.data.remote.rest.actions

import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.UPLOAD_FILE
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.APPLICATION_OCTET_STREAM
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.APPLICATION_PLAIN_TEXT
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.BINARY_CONTENT_TYPE
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.CONTENT_DISPOSITION
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.CONTENT_TRANSFER_ENCODING_BASE64
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.CONTENT_TYPE
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.CONTENT_TYPE_HEADER
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.END_BOUNDARY
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.FILE_NAME
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.FORM_FILE_INFO_PARAMETER
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.FORM_FILE_PARAMETER
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.LINE_FEED
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.SET_BOUNDARY
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.START_BOUNDARY
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_DOCUMENT
import com.google.gson.Gson
import java.io.*
import java.net.HttpURLConnection
import java.util.*
import kotlin.text.Charsets.UTF_8

/**
 * This action is used to upload file
 *
 * The HTTP request is a multimime part with 2 blocks
 *
 * 1) The mb_block with the document info ( DocumentBean ) as an JSON object
 * 2) The file content codified in base64
 *
 * The response is the previous DocumentBean with the id set by backend
 * The response is processed by RestObjectResponseAction (this class extends it )
 *
 * The parameters are the document with the information about the file
 * The imputStream to get the content of the file
 */
class RestFileUploadAction constructor( private var mDocument : DocumentBean, private var mInputStream: InputStream)
    : RestObjectResponseAction<DocumentBean>(UPLOAD_FILE, URL_DOCUMENT , null, DocumentBean::class.java  ) {


    override fun sendRequest(httpConnection : HttpURLConnection) {

        /** The boundary used in form */
        val boundary: String = Date().time.toString()
        httpConnection.setRequestProperty(CONTENT_TYPE_HEADER, String.format(SET_BOUNDARY,boundary))
        httpConnection.doOutput = true

        val writer =
                PrintWriter(LogWriter(OutputStreamWriter(httpConnection.outputStream, UTF_8.name())), true)

        /*** SEND DOCUMENT CONTENT IN BASE64 */
        writer.append(String.format(START_BOUNDARY, boundary)).append(LINE_FEED)
        writer.append(String.format(CONTENT_DISPOSITION, FORM_FILE_PARAMETER))
        writer.append(String.format(FILE_NAME,mDocument.mFileName)).append(LINE_FEED)
        writer.append(String.format(BINARY_CONTENT_TYPE, APPLICATION_OCTET_STREAM )).append(LINE_FEED)
        writer.append(CONTENT_TRANSFER_ENCODING_BASE64).append(LINE_FEED)
        writer.append(LINE_FEED)
        writer.flush()

        var encodedContent : String ? =  "dXJsPWh0dHA6Ly9sb2NhbGhvc3Q6ODA4MS9jYXJfYWNjaWRlbnRfbWFuYWdlci0xLjAtU05BUFNIT1QKYmFkX3VzZXI9anVhbgpiYWRfcGFzc3dvcmQ9ZmFpbHVyZQphZ2VuZGEuY2FyLmFjY2lkZW50LnNob3J0X2Rlc2NyaXB0aW9uPVNob3J0IGRlc2NyaXB0aW9uCmFnZW5kYS5jYXIuYWNjaWRlbnQuY2l0eT1Mb25kb24KYWdlbmRhLmNhci5hY2NpZGVudC5jb3VudHJ5PUVuZ2xhbmQKYWdlbmRhLmNhci5hY2NpZGVudC5wb2xpY3kuaWQ9MQphZ2VuZGEuY2FyLmFjY2lkZW50LmRhbWFnZXMuZGVzY3JpcHRpb24xPVRoZSByaWdodCBzaWRlIHdpbmRvdyBoYXMgYmVlbiBicm9rZW4gMQphZ2VuZGEuY2FyLmFjY2lkZW50LmRhbWFnZXMuZGVzY3JpcHRpb24yPVRoZSByaWdodCBzaWRlIHdpbmRvdyBoYXMgYmVlbiBicm9rZW4gMgoKY3VzdG9tZXJfdXNlcj1sdWlzCmN1c3RvbWVyX3Bhc3N3b3JkPWx1aXMKaW5zdXJhbmNlX3N0YWZmX3VzZXI9dmljZW50ZQppbnN1cmFuY2Vfc3RhZmZfcGFzc3dvcmQ9dmljZW50ZQpwcm9mZXNzaW9uYWxfc3RhZmZfdXNlcl8xPWp1YW4KcHJvZmVzc2lvbmFsX3N0YWZmX3Bhc3N3b3JkXzE9anVhbgpwcm9mZXNzaW9uYWxfc3RhZmZfdXNlcl8yPXBlZHJvCnByb2Zlc3Npb25hbF9zdGFmZl9wYXNzd29yZF8yPXBlZHJv"

       /*if( GlobalConstants.mConnectionType == ASYNC) {
            encodedContent = Base64.encodeToString(mInputStream.readBytes(), Base64.DEFAULT)
        }
        else{
            var array : ByteArray = mInputStream.readBytes()
            encodedContent = array.encodeBase64ToString()
        }
*/


        mInputStream.close()
        writer.write(encodedContent!!, 0, encodedContent.length)
        writer.append(LINE_FEED)
        writer.flush()

        /** SEND DOCUMENT INFO AS A JSON STRING */
        val jsonDocument = Gson().toJson( mDocument )

        writer.append(String.format(START_BOUNDARY, boundary)).append(LINE_FEED)
        writer.append(String.format(CONTENT_DISPOSITION, FORM_FILE_INFO_PARAMETER)).append(LINE_FEED)
        writer.append(String.format(CONTENT_TYPE, APPLICATION_PLAIN_TEXT, UTF_8.name() )).append(LINE_FEED)
        writer.append(LINE_FEED)
        writer.flush()
        writer.append(jsonDocument).append(LINE_FEED)
        writer.flush()
        writer.append(LINE_FEED)
        writer.append(String.format(END_BOUNDARY, boundary)).append(LINE_FEED)
        writer.close()
    }
}