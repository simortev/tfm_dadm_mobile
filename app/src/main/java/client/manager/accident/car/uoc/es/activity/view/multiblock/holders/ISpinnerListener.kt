package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

/**
 * Interface to notify when an item has been selected
 */
interface ISpinnerListener{
    fun onSelectedIem( position: Int )
}