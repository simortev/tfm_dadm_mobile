package client.manager.accident.car.uoc.es.data.remote.rest.actions

import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.SYNC
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.APPLICATION_JSON
import client.manager.accident.car.uoc.es.data.remote.rest.constants.RestConstants.CONTENT_TYPE_HEADER

import com.google.gson.Gson
import com.google.gson.stream.JsonWriter
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.net.HttpURLConnection
import kotlin.text.Charsets.UTF_8

/**
 * This class is used to GET, ADD, UPDATE or REMOVE a bean of a BeanType
 *
 * ADD and UPDATE send the object as a JSON body
 * GET adn REMOVE don't send an object, they use an id in the URL
 *
 * As this class extends RestObjectResponseAction, the response is processed by RestObjectResponseAction class
 */

class RestObjectAction<BeanType> constructor(
        mOperation: Operation,
        mRelativeUrl: String,
        params : Map<String,String>?,
        private var mObj: BeanType?,
        mObjClass: Class<BeanType>? )
    : RestObjectResponseAction<BeanType>(mOperation, mRelativeUrl,params, mObjClass ){

    override fun sendRequest(httpConnection : HttpURLConnection) {
        if( mObj == null ){
            return
        }

        httpConnection.setRequestProperty(CONTENT_TYPE_HEADER, APPLICATION_JSON)
        httpConnection.doOutput = true

        val writer = when( GlobalConstants.mConnectionType ) {
            SYNC -> PrintWriter(LogWriter(OutputStreamWriter(httpConnection.outputStream, UTF_8.name())), true)
            else -> OutputStreamWriter(httpConnection.outputStream, UTF_8.name())
        }

        Gson().toJson( mObj ,mObjClass , JsonWriter( writer ) )
      //  httpConnection.outputStream.flush()
      //  httpConnection.outputStream.close()
        writer.flush()
        writer.close()
    }
}
