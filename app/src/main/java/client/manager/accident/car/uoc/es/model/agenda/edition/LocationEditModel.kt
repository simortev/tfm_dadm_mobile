package client.manager.accident.car.uoc.es.model.agenda.edition
import android.location.Address
import android.util.Log
import client.manager.accident.car.uoc.es.activity.EditBaseActivity


import client.manager.accident.car.uoc.es.activity.fragments.agenda.edition.AgendaBlockEditionFragment
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.IFabButtonHolder

import client.manager.accident.car.uoc.es.activity.R.drawable.ic_current_location
import client.manager.accident.car.uoc.es.application.GlobalConstants.LOG_TAG
import client.manager.accident.car.uoc.es.application.utils.location.ILocationListener
import client.manager.accident.car.uoc.es.application.utils.location.LocationExecutor


class LocationEditModel(private var mFragment: AgendaBlockEditionFragment)
    :IFabButtonHolder, ILocationListener
{
    private var mLocationExecutor : LocationExecutor? = null

    override fun getButtonId(): Int {
        return ic_current_location
    }

    override fun onClickButton() {
       if( mLocationExecutor != null ) {
           return
       }

       mLocationExecutor = LocationExecutor(mFragment.context!!,this)
               .apply { execute() }
    }

    override fun onLocation(address: Address?) {
        Log.i(LOG_TAG,"Address " + address)

        var agenda = mFragment.getAgenda()

        agenda?.mLongitude = address?.longitude
        agenda?.mLatitude = address?.latitude
        agenda?.mCountry = address?.countryName
        agenda?.mCity = address?.locality
        agenda?.mStreet = address?.thoroughfare

        mFragment.updateAgendaData( agenda!! )

        (mFragment.activity as? EditBaseActivity?)?.onUpdateFragment()

        mLocationExecutor = null
    }

}