package client.manager.accident.car.uoc.es.activity.java.utils

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.CUSTOMER_PASSWORD
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.CUSTOMER_USER
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.INSURANCE_STAFF_PASSWORD
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.INSURANCE_STAFF_USER
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.PROFESSIONAL_STAFF_PASSWORD_1
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.PROFESSIONAL_STAFF_PASSWORD_2
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.PROFESSIONAL_STAFF_USER_1
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.PROFESSIONAL_STAFF_USER_2
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.URL_PROPERTY_LABEL
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.daos.UserDao
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role.*
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.SYNC
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import org.junit.Assert
import java.net.CookieHandler

import java.net.CookieManager
import java.net.CookiePolicy.ACCEPT_ALL


/**
 * Class with utils used in login and logout process for testing
 */
object LoginUtils {

    /** Cookie manager with the customer session  */
    private var mCookieManagerCustomUser: CookieManager ? = null
    /** Cookie manager with the insurance staff worker session  */
    private var mCookieManagerInsuranceStaffUser: CookieManager ? = null
    /** Cookie manager with the professional staff session 1  */
    private var mCookieManagerProfessionalStaffUser1: CookieManager ? = null
    /** Cookie manager with the professional staff session 2  */
    private var mCookieManagerProfessionalStaffUser2: CookieManager ? = null

    /**
     * Login with user and password
     */
    fun login(user: String, password: String): CookieManager {

        val cookieManager = CookieManager(null, ACCEPT_ALL)
        CookieHandler.setDefault(cookieManager)

        /***
         * User dao is the object to get user data, login is the operation to do,
         * on response set one object and the handler to manage the response,
         * the object is passed as parameter to this handler
         */
        UserDao().login( user , password)
            .onResponse(LoginVerifier::verifyLoginOk , LoginVerifier() )
            .execute()

        return cookieManager
    }

    /**
     * Login user for beans tests ( add, update, remove and get beans )
     */
    fun login() {
        GlobalConstants.mUrl = Properties[URL_PROPERTY_LABEL]
        GlobalConstants.mConnectionType = SYNC
        login(Properties[INSURANCE_STAFF_USER], Properties[INSURANCE_STAFF_PASSWORD])
    }

    /**
     * Log all the users, used for authorization test
     * In these test there are 4 actors ( consumer, insurance staff and 2 professional workers )
     * All of them are in session , the cookiemanager contains the cookie ( session ) of each of them
     */
    fun loginUsers() {
        /** Set url and connection type to sync, connection in the same thread as invocation */
        GlobalConstants.mUrl = Properties[URL_PROPERTY_LABEL]
        GlobalConstants.mConnectionType = SYNC

        mCookieManagerCustomUser = login(
                Properties[CUSTOMER_USER],
                Properties[CUSTOMER_PASSWORD])

        mCookieManagerInsuranceStaffUser = login(
                Properties[INSURANCE_STAFF_USER],
                Properties[INSURANCE_STAFF_PASSWORD]
        )

        mCookieManagerProfessionalStaffUser1 = login(
                Properties[PROFESSIONAL_STAFF_USER_1],
                Properties[PROFESSIONAL_STAFF_PASSWORD_1]
        )

        mCookieManagerProfessionalStaffUser2 = login(
                Properties[PROFESSIONAL_STAFF_USER_2],
                Properties[PROFESSIONAL_STAFF_PASSWORD_2]
        )
    }

    /**
     * Previous to a request, a user is activated
     * This means the cookie used ( session ) is the one who was activated in this function
     * This is used in test for verifying authorization functionality
     */
    fun activeUser(role: Role) {

        when( role ) {
            CUSTOMER ->  mCookieManagerCustomUser
            INSURANCE_STAFF -> mCookieManagerInsuranceStaffUser
            CAR_MECHANIC -> mCookieManagerProfessionalStaffUser1
            else -> mCookieManagerProfessionalStaffUser2
        }.let ( CookieHandler::setDefault )
    }

    /**
     * Logout user, invalidate session
     */
    fun logout() {
        UserDao().logout()
            .onResponse(LoginVerifier::verifyLogout , LoginVerifier() )
            .execute()
    }

    /**
     * Logout all the users
     */
    fun logoutUsers() {

        CookieHandler.setDefault(mCookieManagerCustomUser)
        logout()

        CookieHandler.setDefault(mCookieManagerInsuranceStaffUser)
        logout()

        CookieHandler.setDefault(mCookieManagerProfessionalStaffUser1)
        logout()

        CookieHandler.setDefault(mCookieManagerProfessionalStaffUser2)
        logout()
    }

    /**
     * The functions of this class are invoked after the rest connection has ended
     * with the proper result
     * Depending on the test, a result or other is expected
     */
    class LoginVerifier
    {
        /** Verify user wass logged and a user bean received */
        fun verifyLoginOk( carInfoBean: CarInfoBean?)  {
            Assert.assertNotNull("User no authenticated", carInfoBean)
        }

        /** User doesn't exist or bad password, user bean was no received */
        fun verifyLoginNoOk( carInfoBean: CarInfoBean?) {
            Assert.assertNull("Bad user was logged", carInfoBean)
        }

        /** Verify logout was ok and user bean information was received */
        fun verifyLogout( carInfoBean: CarInfoBean?) {
            Assert.assertNotNull("User logout error", carInfoBean)
            CookieHandler.setDefault(CookieManager(null, ACCEPT_ALL))
        }
    }
}

