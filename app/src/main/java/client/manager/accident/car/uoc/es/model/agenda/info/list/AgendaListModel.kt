package client.manager.accident.car.uoc.es.model.agenda.info.list


import android.view.Gravity
import android.view.View
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.*
import client.manager.accident.car.uoc.es.activity.factory.ActivityFactory
import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.local.DataVariable.*
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.activity.view.list.GenericRecyclerViewAdapter
import client.manager.accident.car.uoc.es.activity.view.list.ListModel
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IMultiPaneModel
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.*
import kotlinx.android.synthetic.main.activity_list_double_pane.*

import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.application.GlobalConstants.DATE_TIME_FORMAT
import client.manager.accident.car.uoc.es.application.GlobalConstants.MAX_SHORT_DESCRIPTION_SIZE
import client.manager.accident.car.uoc.es.application.utils.DeviceHelper
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import java.io.Serializable
import java.text.SimpleDateFormat


/**
 * Base list model used to show accident, task or event list
 *
 * It received a agenda bean as parent, null in case of car accident list
 *
 * The acitity where the list is located
 *
 * The class extends from the list model
 *
 *  The list model allows
 *
 *      + Get data items from the repository
 *
 *      + All the cards for each item in the list
 *          + The left icon ( accident: car, task: icon task, event: icon event )
 *          + The right icon ( an arrow in case or accidents or tasks to go other level )
 *          + The content of the card
 *
 *      + Is notified about the folowing events
 *
 *          + Click on left icon ( in this case do nothing )
 *          + Click on right icon ( mavigage to the accident or task )
 *          + Click on content ->
 *                  + Show content in landscape tablet
 *                   + Show content in another activty in mobile or vertical tablet
 *           + Click of floating button ( the create button , accident, task or event )
 *              + Create a new bean and go to edition acitivit
 */
abstract class AgendaListModel<T : IAgendaBean>(
        protected var mParent : T?,
        activity: AgendaBaseActivity)
    : ListModel<T>( activity , activity.window.decorView.rootView )  {

    // Selected object
    private var mSelectedObject : T ? = null

    // The user and policy info
    private var mCarInfo : CarInfoBean? = null

    // This class is overriden for every specif model to get item data
    abstract fun getData() : ITaskDao<List<T>>

    // This function is overriden for each model to get content as a multi pane
    abstract fun getContentModel(obj : T): IMultiPaneModel

    /**
     * There are two different layouts for a list
     *   1) for mobile and vertical tablets
     *   2) for landscape tablets
     *
     *   The second one has the widget fragment_content_landscapte to show the content
     *   of the selected item
     */
    private fun isDoublePane() : Boolean {
        return mActivity.fragment_content_landscape != null
    }

    /**
     * Create list and get car info from the permanent storage
     */
    override fun createList( )  {

        if( verifyOrientation() ) {
            return
        }

        // If it is a car accident list then get this information
        if( mParent == null ) {
            mCarInfo = mActivity.getStorage().getAppObject(POLICY_INFO, CarInfoBean::class.java)
        }

        onRefreshList()
    }


    // If tablet landscape finish the activity
    private fun verifyOrientation() : Boolean {
        val storage = mActivity.getStorage()

        if( DeviceHelper.isMobile( mActivity )  ) {
            return false
        }

        if( DeviceHelper.isLandScape( mActivity ) ) {

            var agendaType = storage.getAppObject(AGENDA_ITEM_TYPE_RETURN,Int::class.java)

            val agenda : IAgendaBean? = when( agendaType ) {
                CAR_ACCIDENT.ordinal -> storage.getAppObject(AGENDA_ITEM_RETURN,CarAccidentBean::class.java)
                else -> storage.getAppObject(AGENDA_ITEM_RETURN,AgendaBean::class.java)
            }

            val panel = storage.getAppObject(PANEL_INFO_RETURN,Int::class.java)

            agenda?.let {
                storage.removeData(AGENDA_ITEM_RETURN)
                storage.addData( AGENDA_ITEM , agenda ) }

            panel?.let {
                storage.removeData( PANEL_INFO_RETURN )
                storage.addData( PANEL_INFO , panel ) }

            return false
        }

        val agenda = mActivity.getStorage().getData(AGENDA_ITEM)
        if( agenda == null ) {
            return false
        }

        val panel = storage?.getData(PANEL_INFO)
        if( panel == null ) {
            return false
        }

        panel?.let { storage?.addData( PANEL_INFO_RETURN , panel ) }
        storage.removeData(PANEL_INFO)

        (agenda as? T?)?.let { onContentClick( it )}

        return true
    }
    /**
     * Invoked when the list should be refreshed and created again, this get data from
     * repository in an asyn way
     */
    override fun onRefreshList() {

        getData()
            .onResponse( AgendaListModel<T>::createListView, this)
            .execute()
    }

    /**
     * On response to refresh list a lis is received and the recycler view is created
     */
    private fun createListView(hList : List<T> ? ) {

        finishRefresh()

        if( hList == null || hList.isEmpty() ) {
            mActivity.removeFragments()
            mActivity.getStorage().removeData(AGENDA_ITEM)
        }
        else if( mActivity.getStorage().getData(AGENDA_ITEM) == null )  {
            mActivity.getStorage().addData( AGENDA_ITEM, hList[0])
        }

        hList.let {
            mRecyclerView?.adapter = GenericRecyclerViewAdapter(this, hList ?: ArrayList() )
        }
    }

    // Icon right gravity
    override fun getCardRightIconGravity(obj: T): Int? {
        return Gravity.CENTER
    }

    // Icon left gravity
    override fun getCardLeftIconGravity(obj: T): Int? {
        return Gravity.TOP
    }

    /**
     * If the right icon is pressed then navigate to the next list
     *
     * accident -> task
     * task -> event
     * event doesn't have left icon to navigate
     *
     * Save the item selected to mantain it if a new activity is created and later on
     * should be recovered
     *
     * A new agenda list activity is created
     */
    override fun onRightIconClick(obj: T) {
        mActivity.getStorage().addData( AGENDA_ITEM , obj )
        mActivity.getStorage().removeData( PANEL_INFO )
        ActivityFactory.onShowActivity( mActivity, obj , AGENDA_OBJECT, AgendaListActivity::class.java )
    }

    override fun onLeftIconClick(obj: T) {

    }

    /**
     * The content of the item should be shown,
     *
     * In mobile and vertical tablets a new activity should be created
     * In landscape tablets the content is shown as a fragment
     *
     * IN this case there is always an selected item, when the list is cretted is the first one
     *
     * On content , save the agenda item, if a new activity should be created
     */
    override fun onContentClick(obj: T) {
        if( mSelectedObject?.getId() == obj.getId() ){
            return
        }

        val agenda = mActivity.getStorage().getData( AGENDA_ITEM) as? IAgendaBean?

        if( agenda?.getId() != obj.getId()  ){
            mActivity.getStorage().removeData( PANEL_INFO )
        }

        mSelectedObject = obj

        mActivity.getStorage().addData( AGENDA_ITEM, obj )

        when( isDoublePane() ){
            true -> onShowContentSecondPane( obj )
            false -> ActivityFactory.onShowActivity( mActivity, obj ,
                    AGENDA_OBJECT , AgendaInfoActivity::class.java )
        }
    }

    /**
     * If click on add button, then create a new bean and show a new edit activity
     */
    override fun onAddButtonClick() {
        val agenda = AgendaBean()
                    .apply { mParentId = mParent?.getId() }
                    .apply { mType = getTypeOfNewAgendaItem() }

        mCarInfo?.let { agenda.mPolicyId = it.mPolicy?.getId() }

        ActivityFactory.onShowActivity( mActivity, agenda , AGENDA_OBJECT , AgendaEditActivity::class.java)
    }

    private fun getTypeOfNewAgendaItem() : Int {
        return when( mParent?.getAgenda()?.mType ) {
            CAR_ACCIDENT.ordinal -> TASK.ordinal
            TASK.ordinal -> EVENT.ordinal
            else -> CAR_ACCIDENT.ordinal
        }
    }

    /**
     * Show content in a second pane as a fragment in the case of landscape tablet
     */
    private fun onShowContentSecondPane(obj : T ) {

        mActivity.supportFragmentManager
                .beginTransaction()
                .replace(fragment_content_landscape,
                        getContentModel( obj ).createContent( mActivity.getStorage() ))
                .commit()
    }


    /**
     * Get card background depending of if the item is selected or not
     */
    override fun getCardBackgroundColor(selected: Boolean): Int {

        return when{
            selected && isDoublePane() -> R.color.cardBackgroundSelected
            else -> R.color.cardBackgroundNoSelected
        }
    }

    /**
     * Inform to the list component if the item is selected or not, is used in
     * landscape tablets
     */
    override fun isCardSelected( obj : T): Boolean {

        return when( val agenda = mActivity.getStorage().getData(AGENDA_ITEM ) ){
            is IObjectBean  -> agenda.getId() == obj.getId() && isDoublePane()
            else -> false
        }
    }

    /**
     * The content of each card in list with date and description
     */
    override fun getCardContent(obj: T): View {
         val view = mActivity.layoutInflater.inflate(R.layout.card_agenda_list_item, null, false)

         val date = obj?.getAgenda()?.mStartDate?.let { SimpleDateFormat(DATE_TIME_FORMAT).format( it ) } ?: ""
         val description = obj.getAgenda()?.mShortDescription ?: ""

        view.findViewById<TextView>(card_date)?.text = date

        val content = when{
            description.length > MAX_SHORT_DESCRIPTION_SIZE ->
                description.substring(0 , MAX_SHORT_DESCRIPTION_SIZE - 1 ) + "..."
            else -> description
        }

        view.findViewById<TextView>(card_footer)?.text = content

        return view
    }
}