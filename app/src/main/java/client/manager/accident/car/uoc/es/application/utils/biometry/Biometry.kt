package client.manager.accident.car.uoc.es.application.utils.biometry

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.BIOMETRIC_SUCCESS
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.AuthenticationCallback
import client.manager.accident.car.uoc.es.activity.R.string.*
import client.manager.accident.car.uoc.es.application.GlobalConstants


/**
 * Class used by biometric authentication
 */
object Biometry{

    /**
     * Function used to authenticate, when the process finish the listener is invoked with the result
     */
    fun authenticate(context: AppCompatActivity,listener: BiometryListener) : Boolean {

        if( BiometricManager.from( context ).canAuthenticate() != BIOMETRIC_SUCCESS ) {
            return false
        }

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle( context.resources.getString( biometric_title ) )
                .setDescription(context.resources.getString( biometric_description ) )
                .setNegativeButtonText(context.resources.getString( biometry_cancel ) )
                .build();

        val callback = BiometryCallBack()

        BiometricPrompt(context, BiometryExecutor( listener , callback ) , callback ).authenticate( promptInfo )

        return true
    }

    /**
     * Callback used to get the result of biometry authentication
     */
    class BiometryCallBack() : AuthenticationCallback() {

        var mAuthenticate = false

        /**
         * When there was an erro with authentication
         */
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
            super.onAuthenticationError(errorCode, errString)
            Log.e(GlobalConstants.LOG_TAG, "On authentication error " + errString)
            mAuthenticate = false
        }

        /**
         * When biometry has been ok
         */
        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
            super.onAuthenticationSucceeded(result)
            mAuthenticate = true
        }

        /**
         * When authentication failed
         */
        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
            mAuthenticate = false
        }
    }

}