package client.manager.accident.car.uoc.es.activity.view.list.interfaces

interface IListRefreshListener {

    fun onRefreshList()
}