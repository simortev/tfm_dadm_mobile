package client.manager.accident.car.uoc.es.model.agenda.info.list

import android.view.View
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.model.agenda.info.EventContentModel
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IMultiPaneModel
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager
import java.util.*

/**
 * Create a list of events
 */
class EventListModel( parent : AgendaBean ? ,activity: AgendaBaseActivity  )
    : AgendaListModel<AgendaBean>( parent , activity ) {

    /**
     * Get events of a task
     */
    override fun getData() : ITaskDao<List<AgendaBean>>{
         return AgendaDao().getChildren( ( mParent )?.getId()!! )
    }

    /**
     * No right icon, no more navegation levels
     */
    override  fun getCardRightIconId(obj: AgendaBean): Int ? {
        return null
    }

    /**
     * Get left icon depending on the event type
     */
    override  fun getCardLeftIconId(obj: AgendaBean): Int ? {
       return AgendaDataBindingManager.getEventIConId( obj )
    }

    /**
     * Get card connte
     */
    override fun getCardContent(obj: AgendaBean ): View {
        val view = super.getCardContent(obj)

        view.findViewById<TextView>(R.id.card_body)?.text =
                AgendaDataBindingManager.getEventDescription( mActivity , obj.mSubtype!! )

        return view
    }

    // Get the content model for landscape tablets to show a specific event
    override fun getContentModel(obj: AgendaBean): IMultiPaneModel {
        return EventContentModel(obj)
    }
}