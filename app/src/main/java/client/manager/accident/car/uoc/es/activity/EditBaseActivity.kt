package client.manager.accident.car.uoc.es.activity

import android.view.MenuItem
import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.activity.R.layout.activity_content_editor
import client.manager.accident.car.uoc.es.activity.view.multipane.MultiPaneViewHolder
import client.manager.accident.car.uoc.es.model.agenda.AgendaModelManager


/**
 * This activity is the base to show the views to create or update accidents, tasks or events and
 * settings
 *
 * As in other activities the model is created and content is loaded as a fragment
 *
 * The model depends on the associated bean
 *
 * Accident -> AccidentEditModel
 * Task -> TaskEditModel
 * Event -> EventEditMdodel
 * Settings -> SettingFragment
 */
abstract class EditBaseActivity : AgendaBaseActivity(activity_content_editor) {


    /** The fragment for edtion */
     var mFragment: Fragment? = null

    /** Function invoked when the save button has been pushed */
    abstract fun onClickSaveData()

    /**
     * Create multipane as a fragment, depending if it is agenda edition or settings editons
     * there is different contents
     */
    override fun onResume() {
        super.onResume()

        mFragment = AgendaModelManager.getEditionContent(  this )

        mFragment?.let {
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_content_editor_portrait, it)
                    .commit()
        }
    }

    /**
     * When the icons of the tool bar are pushed
     *
     * If cancel (really the icon is changed from an arrow to a cancel symbol ),  finish activity
     * if accept changes then save the bean ( agenda bean ) and finis
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.icon_right -> onClickSaveData()
            else -> super.onOptionsItemSelected( item )
        }

        finish()

        return true
    }

    /**
     * Update fragment
     */
    fun onUpdateFragment() {
        (mFragment as? MultiPaneViewHolder?)?.onUpdateFragment()
    }

}