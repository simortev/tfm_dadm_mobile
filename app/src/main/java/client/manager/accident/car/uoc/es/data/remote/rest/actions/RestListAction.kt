package client.manager.accident.car.uoc.es.data.remote.rest.actions

import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.LIST
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import kotlin.text.Charsets.UTF_8

/**
 * This class is used for receive a list of beans as an ARRAY of JSON objects
 *
 * No object is send in the request, the HTTP method is always GET
 */
class RestListAction<ResponseBeanType>(mRelativeUrl: String, private var mClass : Class<Array<ResponseBeanType>>)
    : RestAction<List<ResponseBeanType>>(LIST, mRelativeUrl, null){

    override fun receiveResponse( httpConnection : HttpURLConnection ) {
        mResponseBean = (Gson().fromJson<ResponseBeanType>(JsonReader(
                InputStreamReader( httpConnection.inputStream , UTF_8.name() ) ) ,  mClass ) as Array<ResponseBeanType> )
                .asList()
    }
}