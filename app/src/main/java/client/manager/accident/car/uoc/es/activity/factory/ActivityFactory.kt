package client.manager.accident.car.uoc.es.activity.factory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity


import client.manager.accident.car.uoc.es.activity.AgendaEditActivity
import client.manager.accident.car.uoc.es.application.GlobalConstants.NAVIGATION
import client.manager.accident.car.uoc.es.application.GlobalConstants.Navigation.AGENDA_EDITION
import client.manager.accident.car.uoc.es.data.beans.IObjectBean

object ActivityFactory {

     fun <K : AppCompatActivity> onShowActivity(
             activity: FragmentActivity,
             obj : IObjectBean?,
             objId : String,
             cl : Class<K> ) {

        activity.startActivity( Intent( activity , cl  ).apply {
            putExtra(objId, obj )

            if( cl == AgendaEditActivity::class.java )
            { putExtra(NAVIGATION, AGENDA_EDITION)}
        })
    }

}