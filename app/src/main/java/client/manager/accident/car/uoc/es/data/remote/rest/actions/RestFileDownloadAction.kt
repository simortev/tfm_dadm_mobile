package client.manager.accident.car.uoc.es.data.remote.rest.actions


import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_SEPARATOR
import client.manager.accident.car.uoc.es.data.daos.OperationDao.Operation.DOWNLOAD_FILE
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_DOCUMENT
import java.io.OutputStream
import java.net.HttpURLConnection

/**
 * This class is used to received a document, the response is the document in the requset
 *
 * The parameters are the document and the outpusream used to dump the content of the file
 */
class RestFileDownloadAction constructor( private var mDocument : DocumentBean, private var mOutPutStream: OutputStream)
    : RestAction<DocumentBean>(DOWNLOAD_FILE, URL_DOCUMENT + URL_SEPARATOR + mDocument.mId, null ){

    override fun receiveResponse( httpConnection : HttpURLConnection ) {
        httpConnection.inputStream.copyTo( mOutPutStream )
        mResponseBean = mDocument
    }

}