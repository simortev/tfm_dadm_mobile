package client.manager.accident.car.uoc.es.activity.view.list

interface IToolBarListener {

     fun onRightIconClick()

     fun onLeftIconClick()
}