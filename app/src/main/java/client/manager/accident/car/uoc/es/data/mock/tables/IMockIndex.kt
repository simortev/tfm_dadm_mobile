package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.IObjectBean

/**
 * Interface of a index
 */
interface IMockIndex<ResponseBeanType : IObjectBean> {

    /** Add an object*/
    fun addObject( obj : ResponseBeanType)

    /** Remove and object */
    fun removeObject( obj : ResponseBeanType)

}