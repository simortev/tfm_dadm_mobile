package client.manager.accident.car.uoc.es.activity.view.multipane


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.id.fragment_multipane_content
import client.manager.accident.car.uoc.es.activity.R.layout.fragment_multipane
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IPanelHolderSelectedListener


class MultiPaneViewHolder(  ): Fragment() , IPanelHolderSelectedListener {

    private var mContentModel : MultiPaneModel? = null

    private val mPanes = mutableListOf<PaneInfo>()

    private var mSelectedPane : PaneViewHolder?  = null

    fun addPane( pane : PaneInfo) {
        mPanes.add( pane )
    }

    fun setContentModel(contentModel : MultiPaneModel) {
      mContentModel = contentModel
    }

    override fun  onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? {

        val multiPaneView = inflater.inflate(fragment_multipane, container, false)

        val multiPaneMenuView = MultiPaneMenuViewHolder( mContentModel!!,this )
        multiPaneMenuView.onCreateView( multiPaneView , mPanes )

        return multiPaneView
    }

    override fun onHolderSelected(holder: PaneViewHolder) {

        fragmentManager?.beginTransaction()?.
                replace( fragment_multipane_content ,holder.mPane.createFragment())
                ?.setTransition( TRANSIT_FRAGMENT_FADE)
                ?.commit()

        mSelectedPane?.mView?.setBackgroundResource( mContentModel?.getPaneBackgroundColor(false )!! )
        mSelectedPane = holder
        mSelectedPane?.mView?.setBackgroundResource( mContentModel?.getPaneBackgroundColor(true )!! )

        mContentModel?.onPaneSelection( holder.mPane )
    }

     fun onUpdateFragment() {



         fragmentManager?.beginTransaction()
                    ?.remove(getSelectedFragment()!!)
                    ?.commit()

        onHolderSelected( mSelectedPane!!)
    }

    fun getSelectedFragment() : Fragment? {
        return mSelectedPane?.mPane?.mFragment
    }
}