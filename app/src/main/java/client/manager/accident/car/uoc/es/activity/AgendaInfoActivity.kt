package client.manager.accident.car.uoc.es.activity

import client.manager.accident.car.uoc.es.activity.R.layout.activity_content_viewer
import client.manager.accident.car.uoc.es.activity.R.string.*
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.application.utils.DeviceHelper
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.*
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.local.DataVariable
import client.manager.accident.car.uoc.es.data.local.DataVariable.*
import client.manager.accident.car.uoc.es.model.agenda.AgendaModelManager

/**
 * This activity is used in mobiles and tablets in vertical layout to show the info
 * related with a bean ( accident , tasks or event )
 *
 * If bean is deleted then the activity finish and the list is shown and updated
 * If bean is updated then the activity is updated
 *
 */
class AgendaInfoActivity : AgendaBaseActivity(activity_content_viewer) {

    override fun onResume() {
        super.onResume()
        verifyOrientation()
        onUpDateBean()
    }

    // If bean is removed then finish the activity
    override fun onDeleteBean() {
        finish()
    }

    /**
     * Create the content model depending on the bean associated to the activity
     *
     * Accident -> AccidentContentModel
     * Task -> TaskContentModel
     * Event -> EventContentModel
     *
     * And create content as a fragment
     */
    override fun onUpDateBean() {

        val agenda = intent.getSerializableExtra(AGENDA_OBJECT) as? IAgendaBean?

        AgendaDao()
                .getObject( agenda?.getAgenda()?.getId()!!)
                .onResponse( AgendaInfoActivity::createContent , this )
                .execute()
    }

    /**
     * create content
     */
    private fun createContent( result : AgendaBean ?) {

        val agenda = when (result?.mType) {
            CAR_ACCIDENT.ordinal -> getCarInfoBean( result ) as? IAgendaBean?
            else -> result
        }

        mStorage?.addData( AGENDA_ITEM, agenda!!)

        AgendaModelManager.getContentModel(agenda!!).createContent(getStorage()).let {
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_content_viewer_portrait, it)
                    .commit()
        }
    }

    /**
     * Get new car info bean
     */
    private fun getCarInfoBean( result : AgendaBean ? ): CarInfoBean {

            val event = mStorage?.getData(AGENDA_ITEM) as? CarInfoBean?

            val accident = CarAccidentBean( )
            accident.mCarAccident = result
            accident.mPolicy = event?.mPolicy
            accident.mUser = event?.mUser

            return accident
    }

    // If tablet landscape the orientation has changed, finish the activity and return to list
    // the activity should be recreated again, return agenda and pannel info
    // to be selected in the new list-content pannel

    private fun verifyOrientation() {
        if( DeviceHelper.isMobile( this )  ) {
            return
        }

        if( DeviceHelper.isPortrait( this ) ) {
            val panel = mStorage?.getAppObject(PANEL_INFO_RETURN,Int::class.java)

            panel?.let {
                mStorage?.removeData( PANEL_INFO_RETURN )
                mStorage?.addData( PANEL_INFO , panel ) }

            return
        }

        val agenda = mStorage?.getData(AGENDA_ITEM) as? IAgendaBean?

        val panel = mStorage?.getData(PANEL_INFO)

        agenda?.let { mStorage?.addData(AGENDA_ITEM_TYPE_RETURN, agenda.getAgenda()?.mType!! ) }

        agenda?.let {  mStorage?.addData( AGENDA_ITEM_RETURN , agenda ) }

        panel?.let {  mStorage?.addData( PANEL_INFO_RETURN , panel ) }

        finish()
    }

    // Get subtitle
    override fun getSubTitleId( type: Int ) : Int? {
        return when( type ){
            CAR_ACCIDENT.ordinal -> accident_info_title
            TASK.ordinal  -> task_info_title
            else -> event_info_title
        }
    }
}
