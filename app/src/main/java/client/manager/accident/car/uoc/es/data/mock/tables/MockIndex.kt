package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames

/**
 * Mock index of objects
 */
class MockIndex<ResponseBeanType: IObjectBean>( var mFunction : ( ResponseBeanType ) -> Any? )
    : IMockIndex<ResponseBeanType>{

    /** Map of object by a key */
    private var mObjectIndex = mutableMapOf<Any,ResponseBeanType>()

    /**
     * Add object of type and index it by the value of function
     */
    override fun addObject( obj : ResponseBeanType) {
        mFunction.invoke(obj)?.let { mObjectIndex.put( it , obj ) }
    }

    /**
     * Remove the object
     */
    override fun removeObject( obj : ResponseBeanType ) {
        mFunction.invoke(obj)?.let { mObjectIndex.remove( it ) }
    }


    /**
     * Get object by key
     */
    fun getObject( key : Any ) : ResponseBeanType? {
        return mObjectIndex[key]
    }
}