package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.AgendaEditActivity
import client.manager.accident.car.uoc.es.activity.LocaleActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.factory.ActivityFactory
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.activity.view.multifab.MultiFabButtonView
import client.manager.accident.car.uoc.es.activity.view.multifab.interfaces.IMultiFabButtonModel
import client.manager.accident.car.uoc.es.application.GlobalConstants.AGENDA_OBJECT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.daos.AgendaDao
import client.manager.accident.car.uoc.es.data.local.DataVariable.AGENDA_ITEM
import client.manager.accident.car.uoc.es.data.local.DataVariable.PANEL_INFO

abstract class InfoFragment<T : IObjectBean>(private var mLayoutId: Int )
    : Fragment() , IMultiFabButtonModel {

    protected abstract fun createContent(  )

    protected var mRootView : ViewGroup ? = null

    private var mMultiFabButtonView: MultiFabButtonView? = null


    protected var mAgenda : IAgendaBean? = null

    override fun  onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? {

        mRootView = inflater?.inflate(mLayoutId, container, false) as ViewGroup

        mMultiFabButtonView = MultiFabButtonView( mRootView!! , this )
        mMultiFabButtonView?.createView()

        mAgenda = ( activity as AgendaBaseActivity).getStorage().getData(AGENDA_ITEM) as? IAgendaBean?

        if( mAgenda == null ) {
            mAgenda = (activity as LocaleActivity).intent.getSerializableExtra(AGENDA_OBJECT) as? IAgendaBean?
            ( activity as AgendaBaseActivity).getStorage().addData( AGENDA_ITEM,mAgenda!!)
        }

        createContent( )

        return mRootView
    }

    override fun getFabButtonsId(): List<Int> {
        return mutableListOf(R.id.edit_fab_button, R.id.delete_fab_button, R.id.refresh_fab_button)
    }

    override fun onFabButtonSelected(buttonId: Int) {
        when( buttonId) {
            R.id.edit_fab_button -> editContent()
            R.id.refresh_fab_button -> refreshContent()
            else -> deleteContent()
        }
    }

    private fun editContent() {
        ActivityFactory.onShowActivity((activity as LocaleActivity),mAgenda?.getAgenda(),
                AGENDA_OBJECT, AgendaEditActivity::class.java)
    }

    private fun refreshContent() {
        (activity as AgendaBaseActivity).onUpDateBean()
    }

    private fun deleteContent() {

        AgendaDao()
                .removeObject( mAgenda?.getAgenda() )
                .onResponse( InfoFragment<T>::onRemoveAgendaBean , this )
                .execute()
    }

    private fun onRemoveAgendaBean( agenda : AgendaBean?) {
        (activity as AgendaBaseActivity).getStorage().removeData( AGENDA_ITEM )
        (activity as AgendaBaseActivity).getStorage().removeData( PANEL_INFO )
        (activity as AgendaBaseActivity).onDeleteBean()
    }

}