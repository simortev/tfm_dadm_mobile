package client.manager.accident.car.uoc.es.data.mock.actions

/**
 * Interface used by all the other actions
 */
interface IObjectAction<ResponseBeanType>{

    /** Execute action */
    fun execute()

    /** Get response */
    fun getResponse(): ResponseBeanType?
}