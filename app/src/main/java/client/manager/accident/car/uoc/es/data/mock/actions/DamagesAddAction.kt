package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.mock.services.DamageService

class DamagesAddAction(
        private var mDamage : DamageBean? ,
        private var mPolicyNumber : String ? ) : IObjectAction<DamageBean> {

    private var mDamages : DamageBean? = null

    override fun execute() {
        mDamages = DamageService.addDamageByPolicyNumber(mDamage , mPolicyNumber )
    }

    override fun getResponse(): DamageBean? {
        return mDamages
    }

}