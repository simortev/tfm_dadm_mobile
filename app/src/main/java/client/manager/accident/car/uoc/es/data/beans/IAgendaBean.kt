package client.manager.accident.car.uoc.es.data.beans

import java.io.Serializable

interface IAgendaBean : Serializable, IObjectBean {
    fun getAgenda() : AgendaBean?
}