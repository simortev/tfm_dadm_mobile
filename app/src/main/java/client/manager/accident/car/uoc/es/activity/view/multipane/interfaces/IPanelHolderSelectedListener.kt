package client.manager.accident.car.uoc.es.activity.view.multipane.interfaces

import client.manager.accident.car.uoc.es.activity.view.multipane.PaneViewHolder


interface IPanelHolderSelectedListener{
    fun onHolderSelected( holder : PaneViewHolder)
}