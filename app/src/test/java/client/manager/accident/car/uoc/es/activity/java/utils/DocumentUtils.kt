package client.manager.accident.car.uoc.es.activity.java.utils

import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils.ResponseBean
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.daos.DocumentDao
import java.io.InputStream
import java.io.OutputStream

object DocumentUtils {

    fun createDocuments(number: Int, lParentId: Long , name: String ): List<DocumentBean> {
        return (1..number)
                .map { id -> createDocument( lParentId , name , id ) }
                .toList()
    }

    private fun createDocument( lParentId: Long , name: String , index: Int ) : DocumentBean {

        val document = DocumentBean()
        document.mAgendaId = lParentId
        document.mFileName = name + index

        return document
    }

    fun uploadDocument(document : DocumentBean, input : InputStream) : DocumentBean? {
        val response = ResponseBean<DocumentBean>()

        DocumentDao()
                .uploadDocument( document , input )
                .onResponse( ResponseBean<DocumentBean>::setResponse , response )
                .execute()

        return response.mBean
    }

    fun downloadDocument(document : DocumentBean, output : OutputStream) : DocumentBean? {
        val response = ResponseBean<DocumentBean>()

        DocumentDao()
                .downloadDocument( document , output )
                .onResponse( ResponseBean<DocumentBean>::setResponse , response )
                .execute()

        return response.mBean
    }

    fun getDocuments(parentId: Long ): List<DocumentBean>  {
        val response = ResponseBean<List<DocumentBean>>()

        DocumentDao()
                .getDocuments( parentId )
                .onResponse( ResponseBean<List<DocumentBean>>::setResponse , response )
                .execute()

        return response.mBean ?: ArrayList()
    }

    class RandomInputStream() : InputStream(){

        var mBuffer: List<Int> = ArrayList()

        var mIterator : Iterator<Int> ? = null

        var mSize = 0

        override fun read(): Int {
            var value = mIterator?.takeIf { iterator -> iterator.hasNext() }?.next() ?: -1

            mSize--

            return value
        }

        init {
            mBuffer = (1..2000).map { id -> id / 255 }

            mIterator = mBuffer.iterator()

            mSize = mBuffer.size
        }

        override fun available(): Int {
           return mSize
        }

    }
}