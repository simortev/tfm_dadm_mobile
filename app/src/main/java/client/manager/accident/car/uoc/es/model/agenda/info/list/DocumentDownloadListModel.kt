package client.manager.accident.car.uoc.es.model.agenda.info.list

import android.app.DownloadManager
import android.view.View
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.beans.IAgendaBean
import client.manager.accident.car.uoc.es.data.daos.DocumentDao

import client.manager.accident.car.uoc.es.activity.view.list.ListModel
import client.manager.accident.car.uoc.es.activity.view.list.GenericRecyclerViewAdapter
import android.content.Context.DOWNLOAD_SERVICE
import android.os.Environment
import android.view.Gravity
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.local.DataVariable
import java.io.File
import java.io.FileOutputStream

/**
 * Create a list with all the documents of a accident, task or event
 * With a file icon on the left and a download file icon on the right
 */

class DocumentDownloadListModel(rootView : View, activity: AgendaBaseActivity)
    : ListModel<DocumentBean>( activity, rootView ) {

    /** All the pending download files */
    private var mPendingDownloads = mutableMapOf<Long,FileOutputStream>()

    // Create list and disable the add button in list layout
    override fun createList() {
       disableAddButton()
       onRefreshList()
    }

    // Get documents of the item , accident, task or event
    override fun onRefreshList() {
        val agenda = mActivity.getStorage().getData(DataVariable.AGENDA_ITEM) as? IAgendaBean?

        DocumentDao()
                .getDocuments( agenda?.getAgenda()?.getId()!! )
                .onResponse( DocumentDownloadListModel::createListView , this )
                .execute()
    }

    // create list view on response
    private fun createListView(hList : List<DocumentBean> ? ) {
        finishRefresh()

        hList?.let { mRecyclerView?.adapter = GenericRecyclerViewAdapter(this, hList ) }
    }

    // Set icons
    override fun getCardLeftIconId(obj: DocumentBean): Int? {
        return R.drawable.ic_file
    }

    override fun getCardRightIconId(obj: DocumentBean): Int? {
        return R.drawable.ic_file_download
    }

    override fun getCardRightIconGravity(obj: DocumentBean): Int? {
        return Gravity.CENTER
    }

    override fun getCardLeftIconGravity(obj: DocumentBean): Int? {
        return Gravity.CENTER
    }

    // Set content with file name and description
    override fun getCardContent(obj: DocumentBean): View {
        val view = mActivity.layoutInflater.inflate(R.layout.card_info_document, null, false)
        view.findViewById<TextView>(R.id.file_name)?.text = obj.mFileName?:""
        view.findViewById<TextView>(R.id.file_description)?.text = obj.mShortDescription?:""
        return view

    }

    /**
     * Function to download a file form the repository to download directory of the mobile
     */
    private fun onDownload(obj: DocumentBean?) {

        mPendingDownloads.remove( obj?.mId!!)?.let { it.close() }

        val downloadManager = mActivity.getSystemService(DOWNLOAD_SERVICE) as DownloadManager

        var file = getDownloadFile(obj)

        downloadManager.addCompletedDownload(obj.mFileName, obj.mFileName,
                true, "text/plain", file?.getAbsolutePath(), file?.length()!!, true)
    }

    /**
     * Download the file, create a file output stream and use the docuent dao to get it
     */
    override fun onRightIconClick(obj: DocumentBean) {

        if( mPendingDownloads[obj.mId] != null ){
            return
        }

        getDownloadFile( obj )?.let { FileOutputStream( it ) }?.let { mPendingDownloads[obj.mId!!]  = it }

        DocumentDao()
                .downloadDocument( obj , mPendingDownloads[obj.mId!!]!! )
                .onResponse( DocumentDownloadListModel::onDownload, this )
                .execute()
    }

    override fun onLeftIconClick(obj: DocumentBean) {

    }

    /**
     * Open a file for write the content received
     */
    private fun getDownloadFile(obj: DocumentBean?) : File ?{
        val downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        return obj.let { File("${downloads.absolutePath}//${obj?.mFileName}" ) }
    }

    override fun onContentClick(obj: DocumentBean) {

    }

    override fun onAddButtonClick() {

    }

    override fun getCardBackgroundColor(selected: Boolean): Int {
        return R.color.cardBackgroundNoSelected
    }

    override fun isCardSelected(obj: DocumentBean): Boolean {
        return false
    }
}