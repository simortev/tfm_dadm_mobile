package client.manager.accident.car.uoc.es.activity.view.multipane


import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.dimen.pane_menu_mobile_text_size
import client.manager.accident.car.uoc.es.activity.R.id.menu_pane_text
import client.manager.accident.car.uoc.es.activity.R.layout.fragment_multipane_menu
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IPaneGenerator
import client.manager.accident.car.uoc.es.activity.view.multipane.interfaces.IPanelHolderSelectedListener
import client.manager.accident.car.uoc.es.application.utils.DeviceHelper
import client.manager.accident.car.uoc.es.application.utils.DeviceHelper.isMobileLandScape

class MultiPaneMenuViewHolder(
        private var mGenerator : IPaneGenerator,
        private var mPanelSelectionListener: IPanelHolderSelectedListener){

    private var mMultiPaneMenuLayout : LinearLayout ? = null

    fun onCreateView( multiPaneView : View? = null ,  hPanes : List<PaneInfo> ) : MultiPaneMenuViewHolder {
        mMultiPaneMenuLayout = multiPaneView?.findViewById(R.id.fragment_multipane_menu)

        // Set weight mobile and landscape
        if( isMobileLandScape( mMultiPaneMenuLayout?.context!!)) {
            (mMultiPaneMenuLayout?.layoutParams as? LayoutParams?)?.weight = 0.8F
        }

       hPanes.map {  createMenuItem( it )}

        return this
    }

    private fun createMenuItem( pane: PaneInfo) {

        mMultiPaneMenuLayout?.context
                .let { LayoutInflater.from( it )?.inflate( fragment_multipane_menu, mMultiPaneMenuLayout , false ) }
                ?.let { createPaneView( pane , it ) }
    }

    private fun createPaneView( pane: PaneInfo, view : View ){
        val menuText = mMultiPaneMenuLayout?.context?.resources?.getString( pane.mMenuTextId )?:""
        val paneView = PaneViewHolder( pane , view  )
        mMultiPaneMenuLayout?.addView( view )
        val textView = view.findViewById<TextView>( menu_pane_text )

        textView?.text = menuText.toUpperCase()

        if( DeviceHelper.isMobile( textView?.context!! )) {
            textView?.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 7F )
        }

        view.findViewById<ImageView>( R.id.menu_pane_icon)?.setImageResource( pane.mIconId )
        view.setOnClickListener { mPanelSelectionListener.onHolderSelected( paneView ) }
        if( mGenerator.isPanelSelected( pane.mPaneId ) ) { mPanelSelectionListener.onHolderSelected( paneView) }
    }
}