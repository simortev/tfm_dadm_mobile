package client.manager.accident.car.uoc.es.activity.fragments.agenda.edition


import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.LocaleActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.string.settings_url
import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.EditionFragment
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.BlockViewHolder
import client.manager.accident.car.uoc.es.data.local.DataVariable.SERVER_URL
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager

/**
 * Fragment for editing the setting
 */
class SettingsFragment: EditionFragment(R.layout.edit_settings) {

    private var mBlockViewHolder : BlockViewHolder? = null

    override fun createContent() {
        val block = AgendaDataBindingManager.getBlockForSettings( (activity as? AgendaBaseActivity?)!! )
        mBlockViewHolder = BlockViewHolder( block )
        mBlockViewHolder?.createView( mRootView , (activity as LocaleActivity).layoutInflater )
    }

    fun saveSettings() {
        (activity as? AgendaBaseActivity?)!!.getStorage()
                .addData(SERVER_URL,mBlockViewHolder?.getValue(settings_url)!!)
    }
}