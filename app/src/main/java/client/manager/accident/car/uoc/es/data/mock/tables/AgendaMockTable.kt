package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.AGENDA_CHILDREN_BY_PARENT_ID_INDEX
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.AGENDA_CHILDREN_BY_TYPE

class AgendaMockTable : MockTable<AgendaBean>(  ) {

    init {
        addMultipleIndex(AGENDA_CHILDREN_BY_PARENT_ID_INDEX, AgendaBean::mParentId )
        addMultipleIndex(AGENDA_CHILDREN_BY_TYPE,AgendaBean::mType)
    }

    /**
     * Remove the object by object id
     */
    override fun removeObject( obj : AgendaBean ) : AgendaBean {

        getList(AGENDA_CHILDREN_BY_PARENT_ID_INDEX, obj.mId!! )
                ?.let { HashSet( it ) }
                ?.map { removeObject( it.mId!! ) }

        return super.removeObject( obj )
    }

}