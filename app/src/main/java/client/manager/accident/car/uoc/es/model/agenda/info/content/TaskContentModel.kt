package client.manager.accident.car.uoc.es.model.agenda.info

import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.TaskInfoFragment
import client.manager.accident.car.uoc.es.data.beans.AgendaBean

class TaskContentModel( agenda : AgendaBean )
    : AgendaContentModel<AgendaBean>( agenda , ::TaskInfoFragment )