package client.manager.accident.car.uoc.es.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.*
import android.widget.TextView.BufferType.EDITABLE
import client.manager.accident.car.uoc.es.activity.R.id.fingerprint_fab_button
import client.manager.accident.car.uoc.es.activity.R.string.*
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.DEFAULT_URL
import client.manager.accident.car.uoc.es.application.GlobalConstants.LOG_TAG
import client.manager.accident.car.uoc.es.application.GlobalConstants.PERMISSION_SERVICE
import client.manager.accident.car.uoc.es.application.GlobalConstants.mPermissions
import client.manager.accident.car.uoc.es.application.utils.PermissionHelper
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.local.DataVariable.*
import client.manager.accident.car.uoc.es.model.autentication.AuthenticationModel
import client.manager.accident.car.uoc.es.model.autentication.IAuthenticationListener
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy


/**
 * The activy for authentication
 * It is responsible to select the language and for request permissions if it is necessary
 */
class AuthenticationActivity : LocaleActivity() , IAuthenticationListener {

    // Used for request application permissinos
    private var mPermissionHelper: PermissionHelper? = null

    // The authentication model with the login to access data
    private var mAuthenticationModel : AuthenticationModel? = null

    // The text field with user login
    private var mLoginText : EditText? = null

    // The text field with password level
    private var mPasswordText : EditText? = null

    // The list box with language selection
    private var mSpinner : Spinner? = null


    /**
     * Set global constants
     */


    // Fucntion used to create the view of the activity
    override fun onCreate(savedInstanceState: Bundle?) {

        /** Se activity view */
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        GlobalConstants.mUrl = mStorage?.getData( SERVER_URL ) as? String?

        if(GlobalConstants.mUrl == null ) {
            GlobalConstants.mUrl = DEFAULT_URL
            mStorage?.addData(SERVER_URL, DEFAULT_URL )
        }

        GlobalConstants.mConnectionType = GlobalConstants.RestConnectionType.ASYNC
        CookieHandler.setDefault(CookieManager(null, CookiePolicy.ACCEPT_ALL))

        // Create model
        mAuthenticationModel = AuthenticationModel( this )



        // Set title
       // setTitle( title )

        // Get views
        mLoginText = findViewById( R.id.login_text)

        mPasswordText = findViewById(R.id.password_text )

        mSpinner = findViewById( R.id.language_spinner)

        // Set login button action
        findViewById<View>(R.id.login_button).setOnClickListener{ login() }

        // Load spinner with languages and set model as listener
        mSpinner?.adapter = mAuthenticationModel?.getLanguageAdapter( )
        mSpinner?.onItemSelectedListener = mAuthenticationModel
        mSpinner?.setSelection( mAuthenticationModel?.mLanguageId!!)

        // Set image depending of language
        findViewById<ImageView>(R.id.authentication_image).setImageResource(mAuthenticationModel?.getImageId()!!)

        /** Permission for application */
        mPermissionHelper = PermissionHelper(this, mPermissions, PERMISSION_SERVICE)
        mPermissionHelper?.requestAll {  }
        mPermissionHelper?.denied { mPermissionHelper?.openAppDetailsActivity() }

        mAuthenticationModel?.authenticate( this )
    }

    /**
     * The login function
     */
    private fun login() {
        mAuthenticationModel?.login( mLoginText?.text.toString(), mPasswordText?.text.toString(), this )
    }

    /**
     * When user login is known
     */
    override fun onUserValue( login : String ){
        mLoginText?.setText( login , EDITABLE)
        mPasswordText?.requestFocus()
    }

    /**
     * The login response, if the user was authenticated a CarInfoBean is received with
     * information of the user and his insurance policy
     */
    override fun onLoginResponse(carInfoBean: CarInfoBean?) {
        when( carInfoBean != null ) {
            true -> onUserInSession( carInfoBean )
            else -> onBadUser()
        }
    }

    /**
     * User is in session
     */
    private fun onUserInSession( carInfoBean : CarInfoBean  ){
        Log.i( LOG_TAG, "User logged ${carInfoBean.mUser?.mName}" )
        mStorage?.addData( POLICY_INFO , carInfoBean  )
        startActivity( Intent( this, AgendaListActivity::class.java))
    }

    /**
     * User is not in session, show error
     */
    private fun onBadUser() {
        val message = resources.getString( bad_login_message)
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        Log.i( LOG_TAG, "User no logged"  )
    }

    /**
     * Call back when the permission is requested
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPermissionHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    // Disable finter print button
    override fun onActiveFingerPrint() {
        setFingerPrintButtonVisibility(GONE)
    }

    // On deactive finger print
    override fun onDeactiveFingerPrint() {
        setFingerPrintButtonVisibility(VISIBLE)
    }

    // Set fingerprint visibility
    private fun setFingerPrintButtonVisibility(visibility : Int  ) {

        val button = findViewById<View>(fingerprint_fab_button)
        button?.visibility = visibility
        if( visibility == VISIBLE ) {
            button?.setOnClickListener( { _ ->
                mStorage?.removeData(BIOMETRY)
                mAuthenticationModel?.authenticate( this )} )
        }
    }

    // Get subtitle
    override fun getSubTitle() : String? {
        return resources.getString(authentication_title)
    }
}
