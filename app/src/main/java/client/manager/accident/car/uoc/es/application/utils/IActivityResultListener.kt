package client.manager.accident.car.uoc.es.application.utils

import android.content.Intent

interface IActivityResultListener{
    fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?)
}