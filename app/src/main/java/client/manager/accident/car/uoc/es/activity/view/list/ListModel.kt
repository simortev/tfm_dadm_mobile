package client.manager.accident.car.uoc.es.activity.view.list


import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.id.activity_list
import client.manager.accident.car.uoc.es.activity.R.id.refresh_layaout
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListItemGenerator
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListItemListener
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListModel
import client.manager.accident.car.uoc.es.activity.view.list.interfaces.IListRefreshListener
import com.google.android.material.floatingactionbutton.FloatingActionButton


abstract class ListModel<T>(
        protected var mActivity : AgendaBaseActivity,
        protected var mParentView: View )
    : IListItemGenerator<T>, IListItemListener<T>, IListRefreshListener, IListModel {

    protected var mRecyclerView: RecyclerView? = null

    protected var mRefreshLayout: SwipeRefreshLayout? = null

    init {
        mRecyclerView =  mParentView.findViewById(activity_list)

        mRefreshLayout = mParentView.findViewById(refresh_layaout)

        mRefreshLayout?.setOnRefreshListener { onRefreshList() }

        mRecyclerView?.layoutManager = LinearLayoutManager( mActivity )

        mParentView.findViewById<View>(R.id.list_fab_button)?.setOnClickListener{ _ -> onAddButtonClick()  }
    }

    fun disableAddButton() {
        setFabButtonVisibility( GONE )
    }

    fun enableAddButton() {
       setFabButtonVisibility( VISIBLE )
    }

    private fun setFabButtonVisibility( visibility : Int  ) {
        mParentView.findViewById<View>(R.id.list_fab_button)?.visibility = visibility
    }


    fun setFabButton( iconId : Int ) {
        (mParentView.findViewById<View>(R.id.list_fab_button) as? FloatingActionButton?)
                ?.setImageResource( iconId)
    }

    protected fun finishRefresh() {
        mRefreshLayout?.isRefreshing = false
    }
}