package client.manager.accident.car.uoc.es.data.remote.rest.constants

object RestConstants {
    /** HTTP response status ok*/
    const val STATUS_OK = 200

    /** HTTP type of data in request and response */
    const val APPLICATION_JSON = "application/json"

    /** The accept header */
    const val ACCEPT_HEADER = "Accept"

    /** The content type hedader */
    const val CONTENT_TYPE_HEADER = "Content-Type"

    /** The form file parameter for upload files  */
    const val FORM_FILE_PARAMETER = "file"

    /** The form file info parameter for upload file */
    const val FORM_FILE_INFO_PARAMETER = "info"

    /** The content encoding is base64*/
    const val CONTENT_TRANSFER_ENCODING_BASE64 = "Content-Transfer-Encoding: base64"

    /** Define the content type and charest */
    const val CONTENT_TYPE = "Content-Type: %s; charset=%s"

    /** Define the content type and charest */
    const val BINARY_CONTENT_TYPE = "Content-Type: %s"

    /** Establish the boundary in a multimime part form */
    const val SET_BOUNDARY = "multipart/form-data; boundary=%s"

    /** Start a part of the multimime form */
    const val START_BOUNDARY = "--%s"

    /** End the multimime form */
    const val END_BOUNDARY = "--%s--"

    /** Content disposition of a part in the multimime part form */
    const val CONTENT_DISPOSITION = "Content-Disposition: form-data; name=\"%s\""

    /** File name if the part is a content file */
    const val FILE_NAME = "; filename=\"%s\""

    /** The application plain text */
    const  val APPLICATION_PLAIN_TEXT = "text/plain"

    const  val APPLICATION_OCTET_STREAM= "application/octet-stream"

    /** The line feed */
    const val LINE_FEED = "\r\n"
}