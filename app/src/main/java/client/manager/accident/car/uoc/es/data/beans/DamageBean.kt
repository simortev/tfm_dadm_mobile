package client.manager.accident.car.uoc.es.data.beans



import com.google.gson.annotations.SerializedName

/**
 * The damage bean
 */
class DamageBean : IObjectBean {

    /** ID of the damage */
    @SerializedName("id")
    var mId: Long? = null

    /** Agenda id of the damage  */
    @SerializedName( "agenda_id")
    var mAgendaId: Long? = null

    /** The policy ID related with the damage, the car model, ...  */
    @SerializedName( "policy_id")
    var mPolicyId: Long? = null

    /** Description of damages  */
    @SerializedName( "damages")
    var mDamages: String? = null

    override fun getId(): Long? {
        return mId

    }

    override fun setId( objId : Long ){
        mId = objId
    }

    override fun toString(): String {
        val builder = StringBuilder()

        mId?.let { id -> builder.append("ID: ").append(id).append("\n") }
        mAgendaId?.let{ agendaId -> builder.append("AGENDA ID: ").append(agendaId).append("\n") }
        mPolicyId?.let{ policyId -> builder.append("POLICY: ").append(policyId).append("\n") }
        mDamages?.let{ damages -> builder.append("DAMAGES: ").append(damages).append("\n") }

        return builder.toString()
    }
}
