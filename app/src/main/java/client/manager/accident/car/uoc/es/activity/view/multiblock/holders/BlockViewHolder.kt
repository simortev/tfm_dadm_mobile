package client.manager.accident.car.uoc.es.activity.view.multiblock.holders


import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.R.id.*
import client.manager.accident.car.uoc.es.activity.R.layout.*
import client.manager.accident.car.uoc.es.activity.view.multiblock.*
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * This is the view of a block of content
 *
 * If load the layout, set icon, set the title of the block and create each of the block items
 */
open class BlockViewHolder(private var mBlock : Block)
    :View.OnClickListener{

    // All the block items
    private var mBlockItemHolders = mutableMapOf<Int,BlockItemViewHolder>()

    // Each block really is a card layout
    var mCardView : View? = null

    // For every block create a block holder
    init {
        mBlock.mBlockItems
                .map { getBlockItemViewHolder(  it ) }
                .map { mBlockItemHolders.put( it.mLabelId , it ) }
    }

    // Create the view of the block
    fun createView(parentView: ViewGroup?, layoutInflater: LayoutInflater) {

        if( mBlock.mDisableScroll ) {
            mCardView = layoutInflater.inflate(mb_block_no_scrolled, parentView, false)
        }else{
            mCardView = layoutInflater.inflate(mb_block_scrolled, parentView, false)
        }

        parentView?.addView(mCardView)

        val fabButtonHolder : IFabButtonHolder? = mBlock?.mFabButtonHolder

        val fabButton : FloatingActionButton? = mCardView?.findViewById(block_fab_button)

        if( fabButtonHolder == null ) {
            (fabButton as? View?)?.visibility = GONE
        }else {
            fabButton?.setImageResource( fabButtonHolder?.getButtonId() )
            fabButton?.setOnClickListener( this )
        }

        mBlock.mIconId.let{mCardView?.findViewById<ImageView>(block_image)?.setImageResource(it)}

        mBlock.mTitleId.let { mCardView?.findViewById<TextView>(block_name)?.setText(it) }

        val mContentView = mCardView?.findViewById<LinearLayout>(block_content)

        mBlockItemHolders.values.map { it.createView( mContentView!! , layoutInflater ) }
    }

    /**
     * When fab button if exist has been pushed
     */
    override fun onClick(v: View?) {
        mBlock?.mFabButtonHolder?.onClickButton()
    }


    /* Get value in case of a BlockEditHolder */
    fun getValue( layoutId : Int  ) : String {
        return (mBlockItemHolders[layoutId] as? BlockEditHolder?)?.getValue() ?: ""
    }

    /** Get selected positio in caes of a BlockArray , a spinner */
    fun getPosition( layoutId : Int  ) : Int {
        return (mBlockItemHolders[layoutId] as? BlockArrayEditHolder?)?.getPosition()?: -1
    }

    fun setSpinnerListener(layoutId : Int , listener : ISpinnerListener ) {
        (mBlockItemHolders[layoutId] as? BlockArrayEditHolder?)?.mListener = listener
    }

    /**
     * Get the block view holder depending of their type
     */
    private fun getBlockItemViewHolder( blockItem : BlockItem) : BlockItemViewHolder{
        return when {
            blockItem is BlockText -> BlockTextViewHolder( blockItem )
            blockItem is BlockEditText -> BlockEditHolder( blockItem )
            else -> BlockArrayEditHolder( blockItem as BlockArray )
        }
    }

    fun setIcon( iconId : Int ) {
        mBlock.mIconId = iconId
        mBlock.mIconId.let{mCardView?.findViewById<ImageView>(block_image)?.setImageResource(it)}
    }
}