package client.manager.accident.car.uoc.es.application.utils

import android.content.Context
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import client.manager.accident.car.uoc.es.activity.R

object DeviceHelper{

    // Return if device is tablet or mobile
    fun isTablet( context : Context) : Boolean {
        return context.resources.getBoolean( R.bool.isTablet )
    }

    // Return if device is a mobile
    fun isMobile( context: Context ) : Boolean{
        return !isTablet( context )
    }

    // Return if orientation is landscape
    fun isLandScape( context: Context ) : Boolean {
        return context.resources.configuration.orientation == ORIENTATION_LANDSCAPE
    }

    // Return if it is or not a portrait orientation
    fun isPortrait( context: Context ) : Boolean {
        return !isLandScape(context )
    }

    // Return if it is or not mobile land scape
    fun isMobileLandScape(context: Context): Boolean {
        return isMobile(context) && isLandScape(context);
    }
}