package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.DamageBean
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.DAMAGES_BY_AGENDA_ID_INDEX

class DamageMockTable: MockTable<DamageBean>(  ) {

    init {
        addMultipleIndex(DAMAGES_BY_AGENDA_ID_INDEX, DamageBean::mAgendaId )
    }
}