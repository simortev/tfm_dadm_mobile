package client.manager.accident.car.uoc.es.data.daos

import client.manager.accident.car.uoc.es.data.daos.OperationDao.ConnectionMethod.*

/**
 * Object with all the HTTP methods used by RestFul services
 * GET -> get information
 * POST -> add a bean
 * PUT -> update a bean
 * DELETE -> remove a bean
 *
 * It also contains information abut operations and the relation of them with this methods
 */
object OperationDao {

    /** All the method for connect the rest service to add, update, remove or get data */
    enum class ConnectionMethod {
        GET,
        POST,
        PUT,
        DELETE
    }

    /** The operation executed and the relation with the HTTP method */
    enum class Operation( val mConnectionMethod: ConnectionMethod) {
        LOGIN(GET),
        LOGOUT(GET),
        ADD(POST),
        UPDATE(PUT),
        LIST(GET),
        OBJECT(GET),
        REMOVE(DELETE),
        UPLOAD_FILE(POST),
        DOWNLOAD_FILE(GET)
    }
}