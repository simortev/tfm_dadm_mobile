package client.manager.accident.car.uoc.es.activity.view.multiblock.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import client.manager.accident.car.uoc.es.activity.R.id.block_label

/**
 * This is the base class for view item holders ( text, edition or spinner ) basically it
 * load the layout, there is one different layout for each type of holder
 */
open class BlockItemViewHolder(var mLayoutId : Int,  var mLabelId : Int) {

    // Create the view, load the proper layout and set the label
    open fun createView(parentView: ViewGroup, layoutInflater: LayoutInflater): ViewGroup? {

        val blockView : ViewGroup ? =
                layoutInflater.inflate(mLayoutId, parentView, false) as ViewGroup
        parentView.addView(blockView)

        blockView?.findViewById<TextView>(block_label)?.setText(mLabelId)

        return blockView
    }
}