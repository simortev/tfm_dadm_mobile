package client.manager.accident.car.uoc.es.data.mock.tables

object MockConstants {

    val PARENT_ID_FOR_ACCIDENTS = -1L

    enum class MockNames{
        USER_BY_NAME_INDEX,
        POLICY_BY_USER_ID_INDEX,
        POLICY_BY_NUMBER_INDEX,
        AGENDA_CHILDREN_BY_PARENT_ID_INDEX,
        AGENDA_CHILDREN_BY_TYPE,
        DAMAGES_BY_AGENDA_ID_INDEX,
        DOCUMENTS_BY_AGENDA_ID_INDEX,
        OBJECT_BY_ID_INDEX,
    }
}