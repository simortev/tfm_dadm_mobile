package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.BAD_PASSWORD_PROPERTY_LABEL
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.BAD_USER_PROPERTY_LABEL
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.INSURANCE_STAFF_PASSWORD
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.INSURANCE_STAFF_USER
import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants.URL_PROPERTY_LABEL
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils.LoginVerifier
import client.manager.accident.car.uoc.es.activity.java.utils.properties.Properties
import client.manager.accident.car.uoc.es.data.daos.UserDao
import client.manager.accident.car.uoc.es.application.GlobalConstants
import client.manager.accident.car.uoc.es.application.GlobalConstants.RestConnectionType.SYNC

import org.junit.BeforeClass
import org.junit.Test
import java.net.CookieHandler

import java.net.CookieManager
import java.net.CookiePolicy



/**
 * This class is used to verify login and logout servlets
 *
 * + Verify login ok
 * + Verify login user error
 * + Verify login password error
 * + Verify logout
 *
 * UserDao is the object to get user data, login is the operation to do,
 * on response set one object and the handler to manage the response,
 * the object is passed as parameter to this handler
 *
 *     UserDao()
 *       .login( Properties[INSURANCE_STAFF_USER] , Properties[INSURANCE_STAFF_PASSWORD] )
 *        .onResponse( LoginVerifier::verifyLoginOk , LoginVerifier())
 *        .execute()
 *
 */
class TestLogin {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setURL() {
            /** Set url, sync connection and cookie manager for sessions */
            GlobalConstants.mUrl = Properties[URL_PROPERTY_LABEL]
            CookieHandler.setDefault(CookieManager(null, CookiePolicy.ACCEPT_ALL))
            GlobalConstants.mConnectionType = SYNC
        }
    }

    /** Test login and logout ok */
    @Test
    fun testLoginOk() {
        /***
         * User dao is the object to get user data, login is the operation to do,
         * on response set one object and the handler to manage the response,
         * the object is passed as parameter to this handler
         */
        UserDao().login( Properties[INSURANCE_STAFF_USER] , Properties[INSURANCE_STAFF_PASSWORD] )
            .onResponse( LoginVerifier::verifyLoginOk , LoginVerifier())
            .execute()

        UserDao().logout()
            .onResponse( LoginVerifier::verifyLogout , LoginVerifier())
            .execute()
    }

    /** Test login user doesn't exist */
    @Test
    fun testLoginUserError() {
        UserDao().login( Properties[BAD_USER_PROPERTY_LABEL] , Properties[INSURANCE_STAFF_PASSWORD] )
            .onResponse( LoginVerifier::verifyLoginNoOk , LoginVerifier()  )
            .execute()
    }

    /** Test password it is not correct */
    @Test
    fun testPasswordError() {
        UserDao().login( Properties[INSURANCE_STAFF_USER] , Properties[BAD_PASSWORD_PROPERTY_LABEL] )
            .onResponse( LoginVerifier::verifyLoginNoOk , LoginVerifier() )
            .execute()
    }
}
