package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarDamageBean
import client.manager.accident.car.uoc.es.data.mock.services.AgendaService
import client.manager.accident.car.uoc.es.data.mock.services.DamageService
import client.manager.accident.car.uoc.es.data.mock.services.DocumentService

open class GetListAction<ResponseBeanType: IObjectBean>(
        private var mParentId : Long,
        private var mCl : Class<ResponseBeanType> )
    :IObjectAction<List<ResponseBeanType>>{

    private var mList : List<ResponseBeanType> ? = null

   override fun execute() {
        mList = when(mCl) {
            AgendaBean::class.java -> AgendaService.getChildren( mParentId ) as? List<ResponseBeanType>?
            DocumentBean::class.java -> DocumentService.getDocuments(mParentId) as? List<ResponseBeanType>?
            CarDamageBean::class.java -> DamageService.getAccidentDamages(mParentId) as? List<ResponseBeanType>?
            else -> null
        }
    }

    override fun getResponse(): List<ResponseBeanType>? {
        return mList
    }
}