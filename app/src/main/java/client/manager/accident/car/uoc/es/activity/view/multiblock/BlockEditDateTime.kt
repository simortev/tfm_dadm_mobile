package client.manager.accident.car.uoc.es.activity.view.multiblock

import java.util.*

// It contains a label and a edit text
open class BlockEditDateTime (labelId : Int  = 0, var mDate : Date)  :BlockItem( labelId )