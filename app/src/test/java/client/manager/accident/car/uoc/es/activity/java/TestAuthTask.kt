package client.manager.accident.car.uoc.es.activity.java

import client.manager.accident.car.uoc.es.activity.java.constants.TestConstants
import client.manager.accident.car.uoc.es.activity.java.utils.AgendaUtils
import client.manager.accident.car.uoc.es.activity.java.utils.LoginUtils
import client.manager.accident.car.uoc.es.activity.java.utils.TestUtils
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.UserBean
import client.manager.accident.car.uoc.es.data.beans.UserBean.Role.*
import client.manager.accident.car.uoc.es.data.daos.AgendaDao

import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test


class TestAuthTask {

    /** The agenda bean added  */
    private var mCarAccident: AgendaBean? = null


    /** Login user , and maintain cookie */
    companion object {
        @BeforeClass
        @JvmStatic
        fun login() {
            if(TestConstants.MODE_MOCK) {
                return
            }
            LoginUtils.loginUsers()
        }

        /** Logout user, invalidate session */
        @AfterClass
        @JvmStatic
        fun logout() {
            if(TestConstants.MODE_MOCK) {
                return
            }
            LoginUtils.logoutUsers()
        }
    }

    @Test
    fun testTask() {
        if(TestConstants.MODE_MOCK) {
            return
        }

        LoginUtils.activeUser(CUSTOMER)
        mCarAccident = TestUtils.addBeanItem( AgendaUtils.createCarAccident() , ::AgendaDao)
        TestUtils.assertBeanItemMatchesDataBaseItem( mCarAccident ,  CUSTOMER.name + "  add accident " , ::AgendaDao)

        /** Customers and professional staff can't add a task  */
        addTaskFailure(CUSTOMER)
        addTaskFailure(CAR_MECHANIC)

        addTask()

        LoginUtils.activeUser(INSURANCE_STAFF)
        TestUtils.removeBeanItem(  mCarAccident!!, ::AgendaDao)
    }

    /**
     * Add a tasak by customer or car mechanic , they can't do it, therefore an error is received
     * @param role customer or professional staff
     */
    fun addTaskFailure(role: UserBean.Role) {

        /** Role setContentModel task  */
        LoginUtils.activeUser(role)

        val task = TestUtils.addBeanItem( AgendaUtils.createRepairingCarTask(mCarAccident!!.getId()) , ::AgendaDao)
        Assert.assertNull(role.name +  ", there was an error add task accident with  no grants", task )
    }

    fun addTask() {
        /** Role setContentModel car accident  */
        LoginUtils.activeUser(INSURANCE_STAFF)

        val task = TestUtils.addBeanItem( AgendaUtils.createRepairingCarTask(mCarAccident!!.getId()) , ::AgendaDao)
        TestUtils.assertBeanItemMatchesDataBaseItem( task ,  INSURANCE_STAFF.name + " add task " , ::AgendaDao)

        /** Verify every has grants on tasks but also car driver  */
        AgendaUtils.verifyHasAuthorization(task, INSURANCE_STAFF)
        AgendaUtils.verifyHasAuthorization(task, CUSTOMER)
        AgendaUtils.verifyHasAuthorization(task, CAR_MECHANIC)
        // No authorization
        AgendaUtils.verifyNoAuthorization(task!!, CAR_DRIVER)

        /** Verify everybody has grants on car accident but also car driver  */
        AgendaUtils.verifyHasAuthorization(task?.mParentId!!, INSURANCE_STAFF)
        AgendaUtils.verifyHasAuthorization(task?.mParentId!!, CUSTOMER)
        AgendaUtils.verifyHasAuthorization(task?.mParentId!!, CAR_MECHANIC)

        // No authorization
        AgendaUtils.verifyNoAuthorization(task?.mParentId!!, CAR_DRIVER)
    }
}
