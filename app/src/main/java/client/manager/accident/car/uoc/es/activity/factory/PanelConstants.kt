package client.manager.accident.car.uoc.es.activity.factory

import client.manager.accident.car.uoc.es.activity.R


enum class PanelConstants( val mIconId : Int , val mTitleId: Int  ) {
     AGENDA_ITEM_INFO(R.drawable.ic_info,R.string.information),
     AGENDA_DAMAGES_INFO(R.drawable.ic_car,R.string.damages),
     AGENDA_LOCATION_INFO(R.drawable.ic_location,R.string.location),
     AGENDA_DOCUMENTS_INFO(R.drawable.ic_documents,R.string.documents),
     AGENDA_ITEM_EDIT(R.drawable.ic_info,R.string.information),
     AGENDA_DAMAGES_EDIT(R.drawable.ic_car,R.string.damages),
     AGENDA_LOCATION_EDIT(R.drawable.ic_location,R.string.location),
     AGENDA_DOCUMENTS_EDIT(R.drawable.ic_documents,R.string.documents),

}