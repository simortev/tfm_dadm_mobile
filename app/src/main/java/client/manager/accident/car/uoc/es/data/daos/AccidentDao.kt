package client.manager.accident.car.uoc.es.data.daos


import client.manager.accident.car.uoc.es.application.GlobalConstants.mMockMode
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_ACCIDENTS
import client.manager.accident.car.uoc.es.data.remote.rest.constants.UrlConstants.URL_AGENDA
import client.manager.accident.car.uoc.es.data.tasks.ITaskDao
import client.manager.accident.car.uoc.es.data.beans.extended.CarAccidentBean
import client.manager.accident.car.uoc.es.data.mock.MockTaskDao
import client.manager.accident.car.uoc.es.data.mock.actions.CarAccidentsAction

class AccidentDao
    : ObjectDao<CarAccidentBean>(URL_AGENDA, CarAccidentBean::class.java, Array<CarAccidentBean>::class.java){

    fun getAccidents(  ) : ITaskDao<List<CarAccidentBean>> {
        return when(mMockMode){
            true -> MockTaskDao( CarAccidentsAction() )
            false ->  getObjectList( URL_ACCIDENTS )
        }
    }
}