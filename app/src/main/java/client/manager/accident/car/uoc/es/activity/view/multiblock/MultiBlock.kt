package client.manager.accident.car.uoc.es.activity.view.multiblock

/**
 * If represents several blocks of information or edition
 *
 * It contains block and each block contains block items
 *
 * There are different block items , for information, edition or selection ( spinner )
 */
open class MultiBlock{

    // List of blocks
    var mBlocks = mutableListOf<Block>()

    // Add a block
    fun addBlock( block : Block ) {
        mBlocks.add( block )
    }
}