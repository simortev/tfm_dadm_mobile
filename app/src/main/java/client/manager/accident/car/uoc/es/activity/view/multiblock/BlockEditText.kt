package client.manager.accident.car.uoc.es.activity.view.multiblock

// It contains a label and a edit text
open class BlockEditText (labelId : Int  = 0, var mValueText : String  = "")  :BlockItem( labelId )