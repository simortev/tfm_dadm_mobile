package client.manager.accident.car.uoc.es.data.mock.services

import client.manager.accident.car.uoc.es.data.beans.UserBean
import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mCarInfoBean
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mPolicyMockTable
import client.manager.accident.car.uoc.es.data.mock.services.MockService.mUserTable
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.POLICY_BY_USER_ID_INDEX
import client.manager.accident.car.uoc.es.data.mock.tables.MockConstants.MockNames.USER_BY_NAME_INDEX

object UserService {

    /**
     * Login user and return policy info and user info
     */
    fun login( name : String , pasword : String ) : CarInfoBean? {

        val user = mUserTable.getObject(USER_BY_NAME_INDEX,name)
        if( user == null || user.mPassword?.equals( pasword ) == false  ){
            return null
        }

        if( user.mPassword.equals( pasword) == false ) {
            return null
        }

        var policy = mPolicyMockTable.getObject(POLICY_BY_USER_ID_INDEX, user?.mId!!)
        if(policy == null ) {
            return null
        }

        return CarInfoBean().apply {
            mPolicy = policy
            mUser = user.apply { mPassword == null }

            // This works as session , if mCarInfoBean is null then user is not in session
            mCarInfoBean = this
        }
    }

    /**
     * Logout
     */
    fun logout(  ) : CarInfoBean? {
        val user = mCarInfoBean
        mCarInfoBean = null
        return user
    }

    /**
     * Get user by id
     */
    fun getUserById( userId: Long ) : UserBean? {
        return mUserTable.getObjectById( userId )
    }
}