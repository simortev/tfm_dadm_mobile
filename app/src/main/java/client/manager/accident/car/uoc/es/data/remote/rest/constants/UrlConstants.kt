package client.manager.accident.car.uoc.es.data.remote.rest.constants

object UrlConstants {

    /** The login url  */
    const val URL_LOGIN = "/login"

    /** The logout url  */
    const val URL_LOGOUT = "/logout"

    /** The agenda car accidents url  */
    const val URL_ACCIDENTS = "/rest/agenda/accidents"

    /** The agenda event url  */
    const val URL_AGENDA = "/rest/agenda/element"

    /** The document url  */
    const val URL_DOCUMENT = "/rest/document/element"

    /** The damages url  */
    const val URL_DAMAGES = "/rest/damages/element"

    /** The damages url  */
    const val URL_DAMAGES_POLICY = "/rest/damages/policy/%s"

    /** The agenda documents url  */
    const val URL_AGENDA_DOCUMENTS = "/rest/document/agenda/%d"

    const val URL_AGENDA_CHILDREN = "/rest/agenda/agenda/%d"

    /** Get damages by car accident */
    const val URL_AGENDA_DAMAGES = "/rest/damages/agenda/%d"


    /** The login parameter of the url  */
    const val URL_LOGIN_PARAMETER = "login"

    /** The password parameter of the url  */
    const val URL_PASSWORD_PARAMETER = "password"

    const val URL_SEPARATOR = "/"

    const val URL_QUERY_PREFIX = "?"

    const val URL_QUERY_SEPARATOR = "&"

    const val URL_QUERY_PARAMETER_SEPARATOR = "="
}