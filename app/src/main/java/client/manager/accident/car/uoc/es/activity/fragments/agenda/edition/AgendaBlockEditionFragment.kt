package client.manager.accident.car.uoc.es.activity.fragments.agenda.edition

import client.manager.accident.car.uoc.es.activity.AgendaBaseActivity
import client.manager.accident.car.uoc.es.activity.R
import client.manager.accident.car.uoc.es.activity.R.layout.edition_content
import client.manager.accident.car.uoc.es.activity.R.string.agenda_event_type
import client.manager.accident.car.uoc.es.activity.R.string.agenda_task_type
import client.manager.accident.car.uoc.es.activity.fragments.agenda.info.EditionFragment
import client.manager.accident.car.uoc.es.activity.view.multiblock.Block
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.BlockViewHolder
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.ISpinnerListener
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.EVENT
import client.manager.accident.car.uoc.es.data.beans.AgendaBean.AgendaItemType.TASK
import client.manager.accident.car.uoc.es.data.local.DataVariable.AGENDA_ITEM
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager


abstract class AgendaBlockEditionFragment : EditionFragment(edition_content) , ISpinnerListener {

    protected var mBlockViewHolder : BlockViewHolder? = null

    protected var mUpdageAgendaFromBlock = true

    abstract fun getBlock() : Block

    override fun createContent(  ){
        mBlockViewHolder = BlockViewHolder( getBlock()!! ).apply { createView( mRootView!! , layoutInflater ) }

        when( mAgenda?.mType) {
            TASK.ordinal -> mBlockViewHolder?.setSpinnerListener( agenda_task_type , this )
            EVENT.ordinal -> mBlockViewHolder?.setSpinnerListener( agenda_event_type , this )
        }
    }

    fun getAgenda() : AgendaBean? {
        return mAgenda
    }

    fun updateAgenda( agenda: AgendaBean ) {
        ( activity as? AgendaBaseActivity?)?.getStorage()?.addData( AGENDA_ITEM , agenda )
         mAgenda = agenda
    }

    fun updateAgendaData( agenda: AgendaBean ) {
        ( activity as? AgendaBaseActivity?)?.getStorage()?.addData( AGENDA_ITEM , agenda )
        mAgenda = agenda
        mUpdageAgendaFromBlock = false
    }

    override fun onDestroyView() {
        super.onDestroyView()

        if( mUpdageAgendaFromBlock)   updateAgendaBlock()
    }

    open fun updateAgendaBlock() {
        AgendaDataBindingManager.updateAgendaFromAgendaBlock(mBlockViewHolder!!,mAgenda!!)
                .let { updateAgenda( it ) }
    }

    /**
     * If task or event has been selected then change the icon of task or event
     */
    override fun onSelectedIem(position: Int) {
        when( mAgenda?.mType) {
            TASK.ordinal -> mBlockViewHolder?.setIcon( AgendaDataBindingManager.getTaskIconIdFromPosition(position) )
            EVENT.ordinal -> mBlockViewHolder?.setIcon( AgendaDataBindingManager.getEventIconIdFromPosition( position))
        }
    }
}