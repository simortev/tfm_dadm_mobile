package client.manager.accident.car.uoc.es.activity.java.utils

import client.manager.accident.car.uoc.es.data.beans.IObjectBean
import client.manager.accident.car.uoc.es.data.daos.ObjectDao
import org.junit.Assert

object TestUtils {

    class ResponseBean<BeanType>(var mBean : BeanType? = null )
    {
        fun setResponse( bean: BeanType? ) {
            mBean = bean
        }

    }

    fun <BeanType : IObjectBean> addBeanItem(bean: BeanType, factory : () -> ObjectDao<BeanType>) : BeanType?
    {
        val response = ResponseBean<BeanType>()

        factory.invoke()
            .addObject( bean )
            .onResponse( ResponseBean<BeanType>::setResponse , response )
            .execute()

        return response.mBean
    }

    fun <BeanType : IObjectBean> updateBeanItem(bean: BeanType, factory : () -> ObjectDao<BeanType>) : BeanType?
    {
        val response = ResponseBean<BeanType>()

        factory.invoke()
            .updateObject( bean )
            .onResponse( ResponseBean<BeanType>::setResponse , response )
            .execute()

        return response.mBean
    }

    fun <BeanType : IObjectBean> removeBeanItem(bean: BeanType, factory : () -> ObjectDao<BeanType>) : BeanType?
    {
        val response = ResponseBean<BeanType>()

        factory.invoke()
            .removeObject( bean )
            .onResponse( ResponseBean<BeanType>::setResponse, response )
            .execute()

        return response.mBean
    }

    private fun <BeanType : IObjectBean> getBeanItem(beanId : Long, factory : () -> ObjectDao<BeanType>) : BeanType?
    {
        val response = ResponseBean<BeanType>()

        factory.invoke()
            .getObject( beanId )
            .onResponse( ResponseBean<BeanType>::setResponse , response )
            .execute()

        return response.mBean
    }



    fun <BeanType : IObjectBean>  assertBeanItemInDataBase(beanId : Long , factory : () -> ObjectDao<BeanType>){
        Assert.assertNotNull( "Bean not exsist " , getBeanItem( beanId , factory ) )
    }

    fun <BeanType : IObjectBean>  assertNotBeanItemInDataBase(beanId : Long, factory : ( ) -> ObjectDao<BeanType>){
        Assert.assertNull( "Bean exists " , getBeanItem( beanId , factory ) )
    }

    fun <BeanType : IObjectBean> assertBeanItemMatchesDataBaseItem(bean : BeanType? , op : String , factory : ( ) -> ObjectDao<BeanType>) {
        Assert.assertNotNull("There was an error $op, bean null" , bean )
        assertBeanItemMatches( bean!! , getBeanItem( bean.getId()!! , factory ), op )
    }

    fun <BeanType : IObjectBean> assertBeanItemMatches(bean1 : BeanType, bean2 : BeanType?, op: String  ){
        Assert.assertNotNull("There was an error $op, bean2 null" , bean2 )
        Assert.assertTrue("There was an error $op, beans don't match",
                bean1.toString().equals(bean2.toString()) == true)
    }
}

