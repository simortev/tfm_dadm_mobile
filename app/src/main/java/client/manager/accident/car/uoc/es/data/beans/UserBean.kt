package client.manager.accident.car.uoc.es.data.beans

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserBean : IObjectBean {

    /** The user ID */
    @SerializedName("id")
    var mId: Long? = null

    /** The login , it can be null when user doesn't have a policy in this company */
    @SerializedName("login")
    var mLogin: String? = null

    /** It is going to be null */
    @Expose(serialize = false)
    var mPassword: String? = null

    /** The user name */
    @SerializedName("name")
    var mName: String? = null

    /** The user role id */
    @SerializedName("role")
    var mRole: Int? = null

    /** The driver license number of user */
    @SerializedName("driver_license_number")
    var mDriverLicenseNumber: String? = null

    /** The user nif  */
    @SerializedName("nif")
    var mNif: String? = null

    /** The user mail  */
    @SerializedName("mail")
    var mMail: String? = null

    /** The user phone  */
    @SerializedName("phone")
    var mPhone: String? = null

    /** The enum with roles */
    enum class Role constructor( var mId : Long ) {
        CUSTOMER(0),
        INSURANCE_STAFF(1),
        CAR_DRIVER(2),
        TOW_TRUCK_DRIVER(3),
        LOSS_ADJUSTER(4),
        CAR_MECHANIC(5),
        LAWYER(6),
        MEDICAL_PRACTITIONER(7),
        PHYSIOTHERAPIST(8);
    }

    override fun getId() : Long? {
        return mId
    }

    override fun setId( objId : Long ){
        mId = objId
    }

    override fun toString(): String {
        val builder = StringBuilder()

        mId?.let{ id -> builder.append("ID: ").append(id).append("\n") }
        mLogin?.let { login -> builder.append("LOGIN: ").append(login ).append("\n") }
        mName?.let { name -> builder.append("NAME: ").append(name).append("\n") }
        mNif?.let{ nif -> builder.append("NIF: ").append(nif).append("\n") }
        mRole?.let{ role -> builder.append("ROLE: ").append(Role.values()[role].name).append("\n") }
        mDriverLicenseNumber?.let { driverLicenseNumber -> builder.append("DRIVER LICENSE: ").append(driverLicenseNumber).append("\n") }
        mMail?.let{ mail -> builder.append("MAIL: ").append(mail).append("\n") }
        mPhone?.let{ phone -> builder.append("PHONE: ").append(phone).append("\n") }

        return builder.toString()
    }
}
