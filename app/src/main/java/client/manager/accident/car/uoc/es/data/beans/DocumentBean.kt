package client.manager.accident.car.uoc.es.data.beans

import com.google.gson.annotations.SerializedName


/**
 * The document data, the id, the car accident, taks or event where is located and the file name
 */

class DocumentBean : IObjectBean {

    /** ID of the event in agenda table  */
    @SerializedName("id")
    var mId: Long? = null

    /** Agenda Id  */
    @SerializedName("agenda_id")
    var mAgendaId: Long? = null

    /** File name */
    @SerializedName("file_name")
    var mFileName: String? = null

    /** Short description of file */
    @SerializedName("short_description")
    var mShortDescription: String? = null


    override fun getId(): Long? {
        return mId
    }

    override fun setId( objId : Long ){
        mId = objId
    }

    override fun toString(): String {
        val builder = StringBuilder()

        mId?.let { id -> builder.append("ID: ").append(id).append("\n") }
        mAgendaId?.let{ agendaId -> builder.append("AGENDA: ").append(agendaId).append("\n") }
        mFileName?.let{ fileName -> builder.append("FILE NAME: ").append(fileName).append("\n") }
        mFileName?.let{ description -> builder.append("SHORT DESCRIPTION: ").append(description).append("\n") }

        return builder.toString()
    }


}
