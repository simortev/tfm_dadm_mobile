package client.manager.accident.car.uoc.es.data.mock.tables

import client.manager.accident.car.uoc.es.data.beans.IObjectBean

/**
 * Mock index of objects
 */
class MockMultiIndex<T: IObjectBean>(var mFunction : (T ) -> Any? )
    : IMockIndex<T>{

    /** Map of object by a key */
    private var mObjectIndex = mutableMapOf<Any,MutableMap<Long,T>>()

    /**
     * Add object of type and index it by the value of function
     */
    override fun addObject( obj : T) {
         getMap( obj ).let{ it[obj.getId()!!] = obj }
    }

    /**
     * Remove the object
     */
    override fun removeObject( obj : T ) {
       getMap(obj)?.apply{
            remove(obj.getId()!!)
            if( isEmpty()) mObjectIndex.remove( mFunction.invoke(obj)!! )
       }
    }


    /**
     * Get list of objects
     */
    fun getList( key : Any ) : MutableCollection<T>? {
        return mObjectIndex[key]?.values
    }

    /**
     * Get set of key
     */
    private fun getMap( obj: T) : MutableMap<Long,T> {

        val key : Any = mFunction.invoke(obj)!!

        if(mObjectIndex[key] == null ) {
            mObjectIndex[key] = mutableMapOf()
        }

        return mObjectIndex[key]!!
    }
}