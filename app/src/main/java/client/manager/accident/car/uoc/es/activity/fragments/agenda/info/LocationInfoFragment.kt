package client.manager.accident.car.uoc.es.activity.fragments.agenda.info

import android.widget.LinearLayout
import client.manager.accident.car.uoc.es.activity.LocaleActivity
import client.manager.accident.car.uoc.es.activity.R.id.location_content
import client.manager.accident.car.uoc.es.activity.R.id.map
import client.manager.accident.car.uoc.es.activity.R.layout.detail_location
import client.manager.accident.car.uoc.es.activity.view.multiblock.Block
import client.manager.accident.car.uoc.es.activity.view.multiblock.MultiBlock
import client.manager.accident.car.uoc.es.activity.view.multiblock.holders.MultiblockViewHolder
import client.manager.accident.car.uoc.es.application.utils.LocaleHelper
import client.manager.accident.car.uoc.es.data.beans.AgendaBean
import client.manager.accident.car.uoc.es.model.agenda.AgendaDataBindingManager
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.view.View
import android.widget.RelativeLayout







class LocationInfoFragment : InfoFragment<AgendaBean>(detail_location), OnMapReadyCallback {

    // Google map
    private lateinit var mMap: GoogleMap

    // Multi block
    private var mMultiBlock : MultiblockViewHolder? = null

    override fun createContent(  ){

        val multiBlock = MultiBlock()

        multiBlock.addBlock(AgendaDataBindingManager.getBlockDataBindingForLocation(
                LocaleHelper.getLanguage((activity as LocaleActivity)),mAgenda?.getAgenda()!!,
                Block::createBlockText,null ))


        mMultiBlock = MultiblockViewHolder( multiBlock )
                .apply { mRootView?.findViewById<LinearLayout>(location_content)
                        ?.let{ createView(it, layoutInflater ) } }


        (childFragmentManager?.findFragmentById(map) as SupportMapFragment).getMapAsync( this )
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if( mAgenda?.getAgenda()?.mLatitude !=null && mAgenda?.getAgenda()?.mLongitude != null ) {
            // Add a marker in Sydney and move the camera
            val place = LatLng(mAgenda?.getAgenda()?.mLatitude!!,mAgenda?.getAgenda()?.mLongitude!!)
            mMap.addMarker(MarkerOptions().position(place))
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,15F))
        }

        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setMaxZoomPreference(20F)
        mMap.setMinZoomPreference(5F)

        val zoomControls = (childFragmentManager?.findFragmentById(map) as SupportMapFragment)
                .getView()?.findViewById<View>( Integer.parseInt("1"))

        if (zoomControls != null && zoomControls.getLayoutParams() is RelativeLayout.LayoutParams) {
            // ZoomControl is inside of RelativeLayout
            val params_zoom = zoomControls.getLayoutParams() as RelativeLayout.LayoutParams

            params_zoom.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        }
    }
}