package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.DocumentBean
import client.manager.accident.car.uoc.es.data.mock.services.DocumentService
import java.io.OutputStream

class DownloadDocumentAction(
        private var mDocument : DocumentBean,
        private var mOutput : OutputStream) : IObjectAction<DocumentBean>{

    private var mResponse : DocumentBean? = null

    override fun execute() {
        mResponse = DocumentService.downloadDocument( mDocument , mOutput )
    }

    override fun getResponse(): DocumentBean? {
        return mResponse
    }
}
