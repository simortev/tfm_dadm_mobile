package client.manager.accident.car.uoc.es.data.mock.actions

import client.manager.accident.car.uoc.es.data.beans.extended.CarInfoBean
import client.manager.accident.car.uoc.es.data.mock.services.UserService

class LogoutAction( ) : IObjectAction<CarInfoBean> {

    var mResponse : CarInfoBean? = null

    override fun execute(){
        mResponse = UserService.logout(  )
    }

    override fun getResponse(): CarInfoBean? {
        return mResponse
    }
}